typedef int (*CallbackType)();

class CallBackTestInClass{
public:
	int prCb();
	void callMe(CallbackType);

	CallbackType getNativeCallback();
};
