#include "CallbackTestInClass.h"
#include <stdio.h>

void callMe(CallbackType c) {
    printf("Java code returned %d\n",c());
    fflush(stdout);
}

int prCb() {
    printf("Native function called\n");
    fflush(stdout);
    return 56;
}

CallbackType getNativeCallback() {
    return &prCb;
}
