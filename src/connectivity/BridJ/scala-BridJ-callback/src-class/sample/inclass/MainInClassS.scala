package sample.inclass

import org.bridj.Pointer

object MainInClass extends App {

  val sample = new CallBackTestInClass

  sample.getNativeCallback().get().apply();

  val callback : CallBackWrapper = new CallBackWrapper()
  println("new callback created (but not passed)")
  
  // pass a callback function to the library
  sample.callMe(Pointer.pointerTo(callback));

  println("done")
}