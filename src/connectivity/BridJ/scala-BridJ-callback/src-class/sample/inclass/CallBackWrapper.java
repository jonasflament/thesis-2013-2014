package sample.inclass;

/**
 * Wraps a callback in a Java object (to exclude that this is a Scala-specific problem) 
 *
 */
public class CallBackWrapper extends SampleLibrary.CallbackType {

	@Override
	public int apply() {
		System.out.println("Java callback called");
		return 175;
	}

}
