package sample.methods

import org.scalatest.Matchers
import org.scalatest.FlatSpec
import org.bridj.Pointer
import sample.inclass.CallBackTestInClass
import sample.inclass.SampleLibrary



// TODO: figure out why this cannot be run as a ScalaTest
class BridJCallbackTestInClass extends FlatSpec with Matchers {

  behavior of "A BridJ callback application"

  it should "be able to call a callback when there is indeed a C++ class involved" in {
    val sample = new CallBackTestInClass()

    // calling the callback method
    sample.getNativeCallback().get().apply() should equal(56);

    // create the callback
    val callback: SampleLibrary.CallbackType = new SampleLibrary.CallbackType() {
      def apply(): Int = {
        System.out.println("Java callback called");
        return 175;
      }
    };

    // pass a callback function to the library
    sample.callMe(Pointer.pointerTo(callback));
  }

}