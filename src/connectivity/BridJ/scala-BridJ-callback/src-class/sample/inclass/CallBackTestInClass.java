package sample.inclass;
import org.bridj.Pointer;
import org.bridj.ann.Library;
import org.bridj.cpp.CPPObject;

import sample.inclass.SampleLibrary.CallbackType;
/**
 * <i>native declaration : line 4</i><br>
 * This file was autogenerated by <a href="http://jnaerator.googlecode.com/">JNAerator</a>,<br>
 * a tool written by <a href="http://ochafik.com/">Olivier Chafik</a> that <a href="http://code.google.com/p/jnaerator/wiki/CreditsAndLicense">uses a few opensource projects.</a>.<br>
 * For help, please visit <a href="http://nativelibs4java.googlecode.com/">NativeLibs4Java</a> or <a href="http://bridj.googlecode.com/">BridJ</a> .
 */
@Library("CallbackTestInClass") 
public class CallBackTestInClass extends CPPObject {
	/**
	 * Original signature : <code>void callMe(CallbackType)</code><br>
	 * <i>native declaration : line 6</i>
	 */
	native public void callMe(Pointer<CallbackType > CallbackType1);
	/**
	 * Original signature : <code>CallbackType getNativeCallback()</code><br>
	 * <i>native declaration : line 8</i>
	 */
	native public Pointer<CallbackType > getNativeCallback();
	public CallBackTestInClass() {
		super();
	}
	public CallBackTestInClass(Pointer pointer) {
		super(pointer);
	}
}
