package sample.inclass;

import org.bridj.Pointer;

public class MainInClassJ {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		CallBackTestInClass sample = new CallBackTestInClass();

		sample.getNativeCallback().get().apply();

		SampleLibrary.CallbackType callback = new CallBackWrapper();

		System.out.println("new callback created (but not passed)");

		// pass a callback function to the library
		sample.callMe(Pointer.pointerTo(callback));
		
		System.out.println("done");
	}

}
