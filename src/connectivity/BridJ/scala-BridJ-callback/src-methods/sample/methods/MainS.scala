package sample.methods

import org.bridj.Pointer

object MainS extends App{
		CallbackTestLibrary.getNativeCallback().get().apply();

		val callback: CallbackTestLibrary.CallbackType = new CallbackTestLibrary.CallbackType() {
			def apply(): Int = {
				System.out.println("Java callback called");
				return 175;
			}
		};
		
		// pass a callback function to the library
	    CallbackTestLibrary.callMe(Pointer.pointerTo(callback));
}