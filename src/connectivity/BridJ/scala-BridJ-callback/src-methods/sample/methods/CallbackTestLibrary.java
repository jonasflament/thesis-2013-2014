package sample.methods;
import org.bridj.BridJ;
import org.bridj.CRuntime;
import org.bridj.Callback;
import org.bridj.Pointer;
import org.bridj.ann.Library;
import org.bridj.ann.Runtime;

@Library("CallbackTest")
@Runtime(CRuntime.class)
public class CallbackTestLibrary {
  static {
    BridJ.register();
  }
  public static abstract class CallbackType extends Callback<CallbackType > {
    public abstract int apply();
  };
  
  public static native void callMe(Pointer<CallbackTestLibrary.CallbackType > CallbackType1);
  public static native Pointer<CallbackTestLibrary.CallbackType > getNativeCallback();
}