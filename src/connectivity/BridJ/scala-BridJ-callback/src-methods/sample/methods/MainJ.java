package sample.methods;



import org.bridj.Pointer;



public class MainJ {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		CallbackTestLibrary.getNativeCallback().get().apply();

		CallbackTestLibrary.CallbackType callback = new CallbackTestLibrary.CallbackType() {
			@Override
			public int apply() {
				System.out.println("Java callback called");
				return 175;
			}
		};
		
		// pass a callback function to the library
	    CallbackTestLibrary.callMe(Pointer.pointerTo(callback));
	}

}
