package sample.methods

import org.scalatest.Matchers
import org.scalatest.FlatSpec
import org.bridj.Pointer

class BridJCallbackTestMethods extends FlatSpec with Matchers {

  behavior of "A BridJ callback application"

  it should "be able to call a callback when there is no C++ class involved" in {

    // call the C++ callback, returned by the getNativeCallback function
	CallbackTestLibrary.getNativeCallback().get.apply() should equal(56)

    
    // create a callback for the callMe function to call
    val callback: CallbackTestLibrary.CallbackType = new CallbackTestLibrary.CallbackType() {
      def apply(): Int = {
        System.out.println("Java callback called");
        return 175;
      }
    };

    // pass a callback function to the library
    CallbackTestLibrary.callMe(Pointer.pointerTo(callback));

  }

}