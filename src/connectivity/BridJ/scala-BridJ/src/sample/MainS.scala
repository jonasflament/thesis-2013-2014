package sample

import org.bridj.Pointer

object MainS extends App {
  val sample = new Sample
  val callback: SampleLibrary.CallbackType = new SampleLibrary.CallbackType() {
    def apply(): Int = {
      System.out.println("Java callback called");
      return 175;
    }
  };

  // pass a callback function to the library
  sample.callMe(Pointer.pointerTo(callback));
}