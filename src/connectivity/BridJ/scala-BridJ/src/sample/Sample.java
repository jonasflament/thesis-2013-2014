package sample;
import org.bridj.Callback;
import org.bridj.Pointer;
import org.bridj.ann.CLong;
import org.bridj.ann.Library;
import org.bridj.cpp.CPPObject;
import sample.SampleLibrary.CallbackType;
import sample.SampleLibrary.FPReturningFloat;
/**
 * <i>native declaration : line 5</i><br>
 * This file was autogenerated by <a href="http://jnaerator.googlecode.com/">JNAerator</a>,<br>
 * a tool written by <a href="http://ochafik.com/">Olivier Chafik</a> that <a href="http://code.google.com/p/jnaerator/wiki/CreditsAndLicense">uses a few opensource projects.</a>.<br>
 * For help, please visit <a href="http://nativelibs4java.googlecode.com/">NativeLibs4Java</a> or <a href="http://bridj.googlecode.com/">BridJ</a> .
 */
@Library("cpp-BridJ") 
public class Sample extends CPPObject {
	/**
	 * Original signature : <code>void voidMethod()</code><br>
	 * <i>native declaration : line 9</i>
	 */
	native public void voidMethod();
	/**
	 * Original signature : <code>int add5ToInt(int)</code><br>
	 * <i>native declaration : line 11</i>
	 */
	native public int add5ToInt(int i);
	/**
	 * Original signature : <code>int add6ToLong(long)</code><br>
	 * <i>native declaration : line 13</i>
	 */
	native public int add6ToLong(@CLong long l);
	/**
	 * Original signature : <code>int add5WPointerToPi(int*)</code><br>
	 * <i>native declaration : line 15</i>
	 */
	native public int add5WPointerToPi(Pointer<Integer > i);
	/**
	 * Original signature : <code>double addFivePointTwoToDouble(double)</code><br>
	 * <i>native declaration : line 17</i>
	 */
	native public double addFivePointTwoToDouble(double d);
	/**
	 * Original signature : <code>float addOnePointThreeToFloat(float)</code><br>
	 * <i>native declaration : line 19</i>
	 */
	native public float addOnePointThreeToFloat(float f);
	/**
	 * Original signature : <code>bool negateBoolean(bool)</code><br>
	 * <i>native declaration : line 21</i>
	 */
	native public boolean negateBoolean(boolean b);
	/**
	 * Original signature : <code>void callMe(CallbackType)</code><br>
	 * <i>native declaration : line 28</i>
	 */
	native public void callMe(Pointer<CallbackType > CallbackType1);
	/**
	 * Original signature : <code>CallbackType getNativeCallback()</code><br>
	 * <i>native declaration : line 29</i>
	 */
	native public Pointer<CallbackType > getNativeCallback();
	/**
	 * Original signature : <code>int prCb()</code><br>
	 * <i>native declaration : line 31</i>
	 */
	native public int prCb();
	/** <i>native declaration : line 23</i> */
	public static abstract class returnValueOfCallBackReturningFloat_callBack_callback extends Callback<returnValueOfCallBackReturningFloat_callBack_callback > {
		abstract public float apply();
	};
	
	public Sample() {
		super();
	}
	public Sample(Pointer pointer) {
		super(pointer);
	}
}
