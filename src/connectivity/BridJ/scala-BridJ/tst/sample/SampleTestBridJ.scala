package sample

import org.scalatest.FlatSpec
import org.scalatest.Matchers
import com.sun.jna.Native
import com.sun.jna.Library
import com.sun.jna.win32.StdCallFunctionMapper
import com.sun.jna.NativeLibrary
import java.util.Map
import com.sun.jna.FunctionMapper
import java.lang.reflect.Method
import scala.collection.mutable.HashMap
import com.sun.jna.Function
import org.bridj.Pointer

class SampleTestBridJ extends FlatSpec with Matchers {

  behavior of "A JNA application"
  val sample = new Sample

  it should "handle a void method" in {
    sample.voidMethod()
  }

  it should "handle an integer method" in {
    var i = sample.add5ToInt(1)
    i should equal(1 + 5)
  }
  
  it should "handle an integer method hen passing a pointer" in {
    var p = Pointer.allocateInt()
    p.set(1)
    var i = sample.add5WPointerToPi(p)
    i should equal(1 + 5)
  }

  it should "handle a long method" in {
    var l = 1.toLong
    var i = sample.add6ToLong(l)
    i should equal(1 + 6)
  }

  it should "handle a double method" in {
    var d = sample.addFivePointTwoToDouble(1.0)
    d should equal(1.0 + 5.2)
  }

  it should "handle a float method" in {
    var f = sample.addOnePointThreeToFloat(1.0f)
    f should equal(1.0f + 1.3f)
  }

  //   UNSATISFIED LINKERROR
  it should "handle a boolean method when passing true" in {
    var b1 = sample.negateBoolean(true)
    b1 should be(false)
  }
  
  it should "handle a boolean method when passing false" in {
    var b2 = sample.negateBoolean(false)
    b2 should be(true)
  }

  ignore should "be able to call a function --which returs a float-- returned by another function" in {
    //    val f: Pointer[SampleLibrary.FPReturningFloat] = sample.getFunctionPointer();
    //    val func : Function = Function.getFunction(f);
    //    val func = f.get()
    //    
    //    val res: Float = func.apply();
    //    
    //    print(s"QSFDFQDFSDF: $res")
    //    res should equal(1.5f)

    //    sample.getNativeCallback().get.apply() should equal(56)

    sample.getNativeCallback().get().apply();

    val callback: SampleLibrary.CallbackType = new SampleLibrary.CallbackType() {
      def apply(): Int = {
        System.out.println("Java callback called");
        return 175;
      }
    };

    // pass a callback function to the library
    sample.callMe(Pointer.pointerTo(callback));
  }

  ////    "handle an int list" in {
  ////      var iArr = sample.mapAddOne(List(1, 2, 3, 4, 5).toArray)
  ////      iArr === List(2, 3, 4, 5, 6).toArray
  ////    }
  //  }
}
