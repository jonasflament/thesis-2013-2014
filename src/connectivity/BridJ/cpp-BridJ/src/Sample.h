typedef int (*CallbackType)();

class Sample{
public:
	Sample();

	void voidMethod();

	int add5ToInt(int i);

	int add6ToLong(long l);

	int add5WPointerToPi(int *i);

	double addFivePointTwoToDouble(double d);

	float addOnePointThreeToFloat(float f);

	bool negateBoolean(bool b);

	void callMe(CallbackType);
	CallbackType getNativeCallback();

	int prCb();
};
