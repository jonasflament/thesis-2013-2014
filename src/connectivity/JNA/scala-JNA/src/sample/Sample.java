package sample;

import com.sun.jna.Function;
import com.sun.jna.Library;
import com.sun.jna.Pointer;

public interface Sample extends Library{
	
	public void voidMethod();
	
	public int add5ToInt(int i);

	public int add6ToLong(long l);

	public int add5WPointerToPi(Pointer i);

	public double addFivePointTwoToDouble(double d);

	public float addOnePointThreeToFloat(float f);

	public boolean negateBoolean(boolean b);
	
//	public float returnValueOfCallBackReturningFloat(Function f);
	
//	public interface SignalFunction extends Callback {
//        void invoke(int signal);
//    }
	public Pointer getFunctionPointer();
}
