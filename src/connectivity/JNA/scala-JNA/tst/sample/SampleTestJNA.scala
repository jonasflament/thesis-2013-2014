package sample

import org.scalatest.FlatSpec
import org.scalatest.Matchers
import com.sun.jna.Native
import com.sun.jna.Library
import com.sun.jna.win32.StdCallFunctionMapper
import com.sun.jna.NativeLibrary
import java.util.Map
import com.sun.jna.FunctionMapper
import java.lang.reflect.Method
import scala.collection.mutable.HashMap
import com.sun.jna.Function
import com.sun.jna.Pointer

class SampleTest extends FlatSpec with Matchers {

  val LIBRARY_NAME = "cpp-JNA"

  // Eclipse CDT prefixed every method with _Z\d{2}, and postfixed it with {v|b|i|l|d} to denote the return method
  // the HashMap methodnames is a mapping between the names in the interface, and the compiled names
  val methodNames = new HashMap[String, String] {
    put("voidMethod", "_Z10voidMethodv")
    put("add5ToInt", "_Z9add5ToInti")
    put("add6ToLong", "_Z10add6ToLongl")
    put("negateBoolean", "_Z12negateBoleanb")
    put("addOnePointThreeToFloat", "_Z23addOnePointThreeToFloatf")
    put("addFivePointTwoToDouble", "_Z23addFivePointTwoToDoubled")
    put("add5WPointerToPi", "_Z16add5WPointerToPiPi")
    
    put("getFunctionPointer", "_Z18getFunctionPointerv")
  }

  val options = new java.util.HashMap[String, FunctionMapper]
  options.put(Library.OPTION_FUNCTION_MAPPER,
    new FunctionMapper {
      def getFunctionName(arg0: NativeLibrary, arg1: Method): String = {
        val name = methodNames.get(arg1.getName());
        if (name.isEmpty)
          return "method_name_missing_in_map"
        else
          return name.get;
      }
    })
  //  options.put(
  //    Library.OPTION_FUNCTION_MAPPER,
  //    new StdCallFunctionMapper() {
  //      public String getFunctionName(NativeLibrary library, Method method) {
  //        if (method.getName().equals("voidMethod"))
  //          method.setName("_Z10voidMethodv")
  //        return super.getFunctionName(library, method)
  //      }
  //    });

  behavior of "A JNA application"
  val sample = Native.loadLibrary(LIBRARY_NAME, classOf[Sample], options).asInstanceOf[Sample]

  it should "handle a void method" in {
    sample.voidMethod()
  }

  it should "handle an integer method" in {
    var i = sample.add5ToInt(1)
    i should equal(1 + 5)
  }

  it should "handle a long method" in {
    var l = 1.toLong
    var i = sample.add6ToLong(l)
    i should equal(1 + 6)
  }

  it should "handle a double method" in {
    var d = sample.addFivePointTwoToDouble(1.0)
    d should equal(1.0 + 5.2)
  }

  it should "handle a float method" in {
    var f = sample.addOnePointThreeToFloat(1.0f)
    f should equal(1.0f + 1.3f)
  }

  it should "handle a boolean method" in {
    var b1 = sample.negateBoolean(true)
    b1 should be(false)

    var b2 = sample.negateBoolean(false)
    b2 should be(true)
  }
  
  it should "be able to call a function --which returs a float-- returned by another function" in {
    val f: Pointer = sample.getFunctionPointer();
    
    val func : Function = Function.getFunction(f);
    
    val res: Float = func.invokeFloat(null);
    
    print(s"QSFDFQDFSDF: $res")
    res should equal(1.5f)
  }
  
  it should "let the native code call a function " in {
    
  }

  ////
  ////    "handle an int list" in {
  ////      var iArr = sample.mapAddOne(List(1, 2, 3, 4, 5).toArray)
  ////      iArr === List(2, 3, 4, 5, 6).toArray
  ////    }
  //  }
}
