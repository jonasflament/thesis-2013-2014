//#include <vector>
#include "Sample.h"
#include <stdio.h>

void Sample() {}
//void ~Sample() {}

void voidMethod() {
	printf("Nothing to do.");
}

int add5WPointerToPi(int *i) {
	printf("Int: %d -> %d\n", (*i), (*i+5));
	return ((*i) + 5);
}

int add5ToInt(int i) {
	printf("Int: %d -> %d\n", (i), (i+5));
	return ((i) + 5);
}

int add6ToLong(long l){
	printf("Long: %d -> %d\n", l, l+6l);
	return (6l+l);
}

double addFivePointTwoToDouble(double d) {
	printf("Double: %f -> %f\n", d, (d+5.2));
	return (d + 5.2);
}

float addOnePointThreeToFloat(float f) {
	printf("Int: %f -> %f\n", f, (f+1.3));
	return (f + 1.3);
}

bool negateBolean(bool b) {
	bool b2 = !b;
	printf("Boolean: %s -> %s\n", b ? "yes": "no", b2 ? "yes": "no");
	return b2;
}

float returnValueOfCallBackReturningFloat(float (*callBack)()){
	float f = (*callBack)();
	printf("Float from callback: %f", f);
	return f;
}

float callMeUsingFunctionPointer() {
	float f = 1.5f;
	printf("Float returned in function called as callback: %f\n", f);
	return f;
}

fpReturnFloat getFunctionPointer(){
	printf("Returned function pointer to callMeUsingFunctionPointer(void)\n");
	return &callMeUsingFunctionPointer;
}

//	std::vector<int> mapAddOne(int a[]){
//		std::vector<int> v(a, a + sizeof a / sizeof a[0]);
//		for(int i = 0; i < v.size(); i++){
//			v[i] = (a[i] + 1);
//		}
//		return v;
//	};
