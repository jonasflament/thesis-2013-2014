#if defined(MODULENAME_IMPORT)
#define EXPORTED __declspec(dllimport)
#elif defined(MODULENAME_EXPORT)
#define EXPORTED __declspec(dllexport)
#endif

typedef float(*fpReturnFloat)();

class Sample{
public:
	Sample();

	void voidMethod();

	int add5ToInt(int i);

	int add6ToLong(long l);

	int add5WPointerToPi(int *i);

	double addFivePointTwoToDouble(double d);

	float addOnePointThreeToFloat(float f);

	bool negateBoolean(bool b);

	float returnValueOfCallBackReturningFloat( float (*callBack)());

	float callMeUsingFunctionPointer();
	fpReturnFloat getFunctionPointer();

};
