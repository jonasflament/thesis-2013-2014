package be.kuleuven.IDP.experiment.packing

import be.kuleuven.IDP.IDPContext
import be.kuleuven.IDP.Types._
import be.kuleuven.IDP.representation.AST.Structure

import be.kuleuven.IDP.mx._

object Draw {

  val c = new IDPContext
  import c._

  import Voc.Draw._
  
  val data = structure(
    ColorValue as (0 to 255)
  )

  def visualize(s: Structure) = {
    var out = ""
    def squareCTs: List[(String, Int)] = asTupleTable(s, square).cts.map(x => (x(0).asString, x(1).asInt))

    for ((s, i) <- squareCTs) {
      out += s"idpd_polygon(4,$s,0,0,0,$i,$i,$i,$i,0).\n"
    }

    def xposCTs = asTupleTable(s, xpos).cts.map(x => (x(0).asInt, x(1).asInt))
    for ((i, j) <- xposCTs) {
      out += s"idpd_xpos($i,$j).\n"
    }

    def yposCTs = asTupleTable(s, ypos).cts.map(x => (x(0).asInt, x(1).asInt))
    for ((i, j) <- yposCTs) {
      out += s"idpd_ypos($i,$j).\n"
    }

    def colorCTs = asTupleTable(s, color).cts.map(x =>
      (x(0).asString, x(1).asInt, x(2).asInt, x(3).asInt))
    for ((s, colorValue1, colorValue2, colorValue3) <- colorCTs) {
      out += s"idpd_color($s,$colorValue1,$colorValue2,$colorValue3).\n"
    }

    def textCTs = asTupleTable(s, ypos).cts.map(x => (x(0).asString, x(1).asString))
    for ((s1, s2) <- textCTs) {
      out += s"idpd_text($s1,$s2).\n"
    }

    IDPDraw(out)
  }
}