package be.kuleuven.IDP.experiment.packing

import be.kuleuven.IDP.mx._
import be.kuleuven.IDP.representation.AST.Structure
import be.kuleuven.IDP.representation.AST.Theory
import be.kuleuven.IDP.IDPContext
import be.kuleuven.IDP.Types._

object PackingExperiment {
  val c = new IDPContext
  import c._

  import Voc.Draw._
  import Voc.Packing._

  def main(args: Array[String]) {
    //////////////////////////////////////////////
    // Setup the mock object for the experiment///
    setup()
    //////////////////////////////////////////////

    prettySolve(data15)
  }

  def prettySolve(data: Structure) {
    stdoptions.cpsupport = true
    println("Searching for solution...")

    val result = solve(data)
    result match {
      case Some(res) =>
        println("[DEBUG] printing AST on one line due to current lack of pretty printing")
        println(res.prettyprint)
        println("Done. Calculating visualization...")
        drawsol(res)
        println	("Done.")
      case None =>
        println("No solution found.")
    }
  }

  def test() {
    stdoptions.cpsupport = true
    stdoptions.groundwithbounds = false

    val result = optSolve(data15)
    if (result.isDefined)
      drawsol(result.get)
    else
      println("No solution found.")
  }

  /**
   * Given a rectangular area of a known dimension and a set of squares, each of which has a known dimension, the problem is to pack all the squares into the rectangular area such that no two squares overlap each other. There can be wasted spaces in the rectangular area.
   * The rectangular area forms a coordinate plane where the left upper corner of the area is the origin, the top edge is the x-axis, and the left edge is the y-axis.
   */

  val theo = theory(
    /*** Constraints ***/
    /* All squares should have a valid position */
    !('id -> Square)(ValidPos('id)),

    definition(
      ValidPos('id) <-- ((XPos('id) + Size('id) =< Width()) & (YPos('id) + Size('id) =< Height()))
    ),
    /* Two squares should not overlap */
    !('id1 -> Square, 'id2 -> Square)(NoOverlap('id1, 'id2)),
    definition(
      Left('id1, 'id2) <-- (XPos('id1) + Size('id1) =< XPos('id2)),
      Up('id1, 'id2) <-- (YPos('id1) + Size('id1) =< YPos('id2)),
      NoOverlap('id, 'id),
      NoOverlap('id1, 'id2) <-- (Left('id1, 'id2) | Left('id2, 'id1) | Up('id1, 'id2) | Up('id2, 'id1))
    ),

    /*** Symmetry breaking ***/
    /* Put largest square in a corner */
    !('id -> X)('id === Largest() ==> (XPos('id) === 0) & (YPos('id) === 0)),

    Largest() === min('id1 -> Square)(
      Size('id1) === max('id2 -> Square)(
        Square('id2), Size('id2)), 'id1)
  )

  val data9 = structure(
    Square as (1 to 9),
    X as (0 to 13),
    Y as (0 to 7),
    Width as 13,
    Height as 7,
    Size as (1 -> 5, 2 -> 5, 3 -> 2, 4 -> 2, 5 -> 2, 6 -> 2, 7 -> 2, 8 -> 2, 9 -> 2)
  )

  val data3 = structure(
    Square as (1 to 3),
    X as (0 to 6),
    Y as (0 to 4),
    Width as 6,
    Height as 4,
    //SizeT as (0 to 6)
    Size as (1 -> 4, 2 -> 2, 3 -> 2)
  )

  val data4 = structure(
    // UNSAT
    Square as (1 to 4),
    X as (0 to 13),
    Y as (0 to 7),
    Width as 13,
    Height as 7,
    //SizeT as (0 to 5)
    Size as (1 -> 4, 2 -> 4, 3 -> 4, 4 -> 4)
  )

  val data15 = structure(
    Square as (1 to 15),
    X as (0 to 200),
    Y as (0 to 100),
    Width as 200,
    Height as 100,
    //SizeT as (0 to 100)
    Size as (1 -> 43, 2 -> 87, 3 -> 42, 4 -> 58, 5 -> 10, 6 -> 15, 7 -> 17, 8 -> 29, 9 -> 17, 10 -> 12, 11 -> 10, 12 -> 15, 13 -> 4, 14 -> 8, 15 -> 7)
  )

  val data20 = structure(
    Square as (1 to 20),
    X as (0 to 400),
    Y as (0 to 200),
    Width as 400,
    Height as 200,
    //SizeT as (0 to 100)
    Size as (1 -> 43, 2 -> 87, 3 -> 42, 4 -> 58, 5 -> 10, 6 -> 15, 7 -> 17, 8 -> 29, 9 -> 17, 10 -> 12, 11 -> 10, 12 -> 15, 13 -> 4, 14 -> 8, 15 -> 7, 16 -> 40, 17 -> 30, 18 -> 25, 19 -> 20, 20 -> 15)
  )

  /**
   * Return the first solution for a given dataset.
   */
  def solve(data: Structure): Option[Structure] = {
    val s = modelexpand(theo, data)
    if (s.isEmpty)
      None
    else
      Some(s.head)
  }

  val visTheory = theory(
    definition(
      square('id, 's) <-- ('s === Size('id)),
      xpos('id, 'x) <-- ('x === XPos('id)),
      ypos('id, 'y) <-- ('y === YPos('id)),
      color('id, 154, 205, 50),
      text('id, 'id)
    )
  )

  /**
   * Draw the first solution for a given dataset.
   */
  def drawsol(model: Structure) = {
    if (model != null) {
      val m = model update (ColorValue as (154, 205, 50))
      val vis = modelexpand(visTheory, m)
      if (!vis.isEmpty)
        Draw.visualize(vis.head)
    }
  }

  // 
  val XPosSum = Func()(int)

  val xPosSumTheo = theory(
    XPosSum() === sum('id -> nat)(Square('id), XPos('id) + YPos('id) + 2 * Size('id))
  )

  //  term xPosSum : voc {
  //    sum{ id : Square(id) : XPos(id) + YPos(id) + 2 * Size(id) }
  //  }
  //  
  def optSolve(data: Structure): Option[Structure] = {
    val results = minimize(theo + xPosSumTheo, data, XPosSum)
    if (results == null)
      None
    else
      Some(results)
  }

  def setup() = new MockConfigurer().configure
}
