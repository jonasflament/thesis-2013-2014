package be.kuleuven.IDP.experiment.sudoku

import be.kuleuven.IDP.IDPConnect
import be.kuleuven.IDP.IDPMock
import be.kuleuven.IDP.representation.AST.Structure
import be.kuleuven.IDP.IDPContext

class MockConfigurer {

  def configure() {
    val a: IDPMock = IDPConnect
    import a._

    a.reset

    // suppress the output of the IDPDraw calls
    shouldOutput = false

    // In "create()"
    for(i <- (1 to 10)){
    	// modelexpand to giving two solutions, so 'madetrue' is used once to create a better solution
    	modelExpandQueue.enqueue(twoSolutionsToFillGridUntilOneSolution)
    }
    // modelexpand to give one solution, which will be used to begin trying to remove elements
    modelExpandQueue.enqueue(oneSolutionToFillGridUntilOneSolution)

    // add enough solutions, to show that there are no other elements that can be removed
    for (i <- (1 to 9 * 9)) {
      if (i < 6) {
        // add a single solution so one square is removed
        modelExpandQueue.enqueue(oneSolutionToFillGridUntilOneSolution)
      } else {
        modelExpandQueue.enqueue(solutionsToPreventElementRemoval)
      }
    }

    // In "solve(...)"
    // use onemodel to generate the solution of the sudoku based on the previous modelexpansion
    oneModelQueue.enqueue(oneModelSolution)
  }

  val c = new IDPContext()
  import c._

  import Voc.SudokuVoc._
  import Voc.SimpleGridVoc._

  def calcBlocks: List[(Int, Int, Int)] = {
    def calc(row: Int, col: Int): Int = {
      val colRes = ((col - 1) / 3) + 1
      val rowRes = ((row - 1) / 3) + 1
      (rowRes - 1) * 3 + colRes
    }

    (for {
      c <- (1 to 9);
      r <- (1 to 9)
    } yield (calc(r, c), r, c)).toList
  }

  val table = List(
    List(1, 2, 3, 7, 8, 9, 4, 5, 6),
    List(4, 5, 6, 1, 2, 3, 7, 8, 9),
    List(7, 8, 9, 4, 5, 6, 1, 2, 3),
    List(2, 1, 4, 3, 6, 5, 8, 9, 7),
    List(3, 6, 5, 8, 9, 7, 2, 1, 4),
    List(8, 9, 7, 2, 1, 4, 3, 6, 5),
    List(5, 3, 1, 6, 4, 2, 9, 7, 8),
    List(6, 4, 2, 9, 7, 8, 5, 3, 1),
    List(9, 7, 8, 5, 3, 1, 6, 4, 2)
  )

  val SudokuSolutions: Traversable[((Int, Int), Int)] = {
    // account for zero-based indexing and 1..x row and col numbering
    for { c <- (1 to table.size); r <- (1 to table.size) } yield ((r, c) -> (table(r - 1)(c - 1)))
  }

  val SudokuMirroredSolutions = {
    // flip the table vertically
    val table = this.table.mapConserve(x => x.reverse)
    // account for zero-based indexing and 1..x row and col numbering
    for { c <- (1 to table.size); r <- (1 to table.size) } yield ((r, c) -> (table(r - 1)(c - 1)))
  }

  val base: Structure = structure(
    Num as (1 to 9),
    Row as (1 to 9),
    Col as (1 to 9),
    Block as (1 to 9),
    InBlock as (calcBlocks)
  )

  val solved = base + structure(
    Sudoku as (SudokuSolutions)
  )

  val solvedMirrored = base + structure(
    Sudoku as (SudokuMirroredSolutions)
  )

  val twoSolutionsToFillGridUntilOneSolution: List[Structure] = List(solved, solvedMirrored)
  val oneSolutionToFillGridUntilOneSolution: List[Structure] = List(solved)
  val oneSolutionToRemoveASingleElement: List[Structure] = List(solved)
  val solutionsToPreventElementRemoval: List[Structure] = List(solved, solvedMirrored)
  val oneModelSolution: Structure = solved
}