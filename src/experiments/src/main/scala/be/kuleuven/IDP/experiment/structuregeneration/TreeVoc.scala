package be.kuleuven.IDP.experiment.structuregeneration

import be.kuleuven.IDP.IDPContext
import be.kuleuven.IDP.Types._

object TreeVoc {
  val context = new IDPContext
  import context._

  val TreeT = Type(char)
  val Left = Pred(TreeT, TreeT)
  val Right = Pred(TreeT, TreeT)
  val Val = Func(TreeT)(char)
  
}