package be.kuleuven.IDP.experiment.userdefinedtheory

import be.kuleuven.IDP.IDPContext
import scala.swing.SimpleSwingApplication
import scala.swing.TextArea
import scala.swing.MainFrame
import scala.swing.BorderPanel
import scala.swing.event.Key
import scala.swing.event.KeyPressed
import java.awt.Dimension
import scala.swing.ScrollPane
import javax.swing.ScrollPaneConstants
import scala.swing.Button
import scala.swing.event.ButtonClicked

object Main extends SimpleSwingApplication {
  val c = new IDPContext
  import c._

  val logArea = new TextArea {
    editable = false
  }

  def log(s: String) = {
    logArea.append(">" + s + "\n")
    logArea.caret.position = logArea.text.length
  }

  class Qs {
    def poseQuestion(question: String, answerWith: String => Any) = {
      Main.log(question + "\n")
      Main.answerCurrentQuestion = answerWith
    }

    def log(line: String) = {
      Main.logArea.append(line + "\n")
    }
  }

  val theoryConstructor = new TheoryConstructor(new Qs)

  var answerCurrentQuestion: String => Any = (x) => x

  val txt: TextArea = new TextArea {
    lineWrap = true

    listenTo(keys)

    reactions += {
      case KeyPressed(_, Key.Enter, _, _) =>
        autoButton.enabled = false
        if (txt.text != "") {
          answerCurrentQuestion(txt.text.trim)
        } else {
          // print theory
        }
        txt.text = ""
    }
  }

  val autoButton: Button = new Button {
    text = "Auto"
    reactions += {
      case ButtonClicked(_) =>
        txt.editable = false

        // set range
        answerCurrentQuestion("c")
        answerCurrentQuestion("a + b = c")
        answerCurrentQuestion("a < 1")
        answerCurrentQuestion("b >= 1")
        
        answerCurrentQuestion("show")

        txt.editable = true
    }
  }

  val scroller = new ScrollPane(logArea) {
    verticalScrollBarPolicy = ScrollPane.BarPolicy.Always
  }

  val txtPanel = new BorderPanel {
    layout(txt) = BorderPanel.Position.Center
    layout(autoButton) = BorderPanel.Position.East
  }

  val frame = new MainFrame {
    minimumSize = new Dimension(500, 400)

    contents = new BorderPanel {
      layout(scroller) = BorderPanel.Position.Center
      layout(txtPanel) = BorderPanel.Position.South
    }
  }
  def top = frame

  theoryConstructor.startQuestioning
}

class Main