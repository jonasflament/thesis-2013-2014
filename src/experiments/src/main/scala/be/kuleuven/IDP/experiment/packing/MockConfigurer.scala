package be.kuleuven.IDP.experiment.packing

import be.kuleuven.IDP.IDPConnect
import be.kuleuven.IDP.IDPMock
import be.kuleuven.IDP.representation.AST.Structure
import be.kuleuven.IDP.IDPContext
import be.kuleuven.IDP.experiment.sudoku.SudokuExperiment

class MockConfigurer {

  def configure() {
    val a: IDPMock = IDPConnect
    import a._

    a.reset
    
    // add the solution to the modelexpand queue
    modelExpandQueue.enqueue(List(solution))
    
    // add an empty structure to the modelexpand queue as visualization result
    modelExpandQueue.enqueue(Nil)

    // suppress the output of the IDPDraw calls
    shouldOutput = false

    // TODO: configure mock for the packing example
  }

  val c = new IDPContext()
  import c._

  import Voc.Packing._

  val solution = structure(
    SizeT as (4, 7, 8, 10, 12, 15, 17, 29, 42, 43, 58, 87),
    Square as (1 to 15),
    X as (0 to 200),
    Y as (0 to 100),
    Left as (1, 4, 1, 7, 1, 8, 1, 9, 1, 13, 1, 14, 1, 15, 2, 1, 2, 3, 2, 4, 2, 6, 2, 7, 2, 8, 2, 9, 2, 10, 2, 11, 2, 12, 2, 13, 2, 14, 2, 15, 3, 4, 3, 6, 3, 7, 3, 8, 3, 9, 3, 10, 3, 11, 3, 12, 3, 13, 3, 14, 3, 15, 5, 1, 5, 3, 5, 4, 5, 6, 5, 7, 5, 8, 5, 9, 5, 10, 5, 11, 5, 12, 5, 13, 5, 14, 5, 15, 6, 8, 6, 9, 6, 13, 6, 14, 6, 15, 7, 8, 7, 14, 7, 15, 9, 8, 9, 14, 9, 15, 10, 4, 10, 7, 10, 8, 10, 9, 10, 13, 10, 14, 10, 15, 11, 7, 11, 8, 11, 9, 11, 13, 11, 14, 11, 15, 12, 7, 12, 8, 12, 9, 12, 13, 12, 14, 12, 15, 13, 14, 13, 15, 15, 14),
    NoOverlap cf () as (),
    NoOverlap ct () as (),
    Up as (1, 3, 1, 5, 1, 6, 1, 7, 1, 8, 1, 9, 1, 10, 1, 11, 1, 12, 1, 13, 1, 14, 1, 15, 2, 5, 2, 13, 2, 14, 2, 15, 4, 5, 4, 6, 4, 7, 4, 8, 4, 9, 4, 11, 4, 12, 4, 13, 4, 14, 4, 15, 6, 5, 6, 7, 6, 11, 6, 12, 6, 13, 6, 14, 6, 15, 7, 13, 8, 13, 8, 14, 8, 15, 9, 5, 9, 7, 9, 12, 9, 13, 9, 14, 9, 15, 10, 5, 10, 6, 10, 7, 10, 11, 10, 12, 10, 13, 10, 14, 10, 15, 11, 5, 11, 12, 11, 13, 11, 14, 11, 15),
    ValidPos as (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15),
    Height as 100,
    Largest as 2,
    Size as (1 -> 43, 2 -> 87, 3 -> 42, 4 -> 58, 5 -> 10, 6 -> 15, 7 -> 17, 8 -> 29, 9 -> 17, 10 -> 12, 11 -> 10, 12 -> 15, 13 -> 4, 14 -> 8, 15 -> 7),
    Width as 200,
    XPos as (1 -> 99, 2 -> 0, 3 -> 87, 4 -> 142, 5 -> 77, 6 -> 132, 7 -> 146, 8 -> 164, 9 -> 147, 10 -> 130, 11 -> 136, 12 -> 129, 13 -> 161, 14 -> 172, 15 -> 165),
    YPos as (1 -> 5, 2 -> 0, 3 -> 58, 4 -> 1, 5 -> 87, 6 -> 60, 7 -> 76, 8 -> 59, 9 -> 59, 10 -> 48, 11 -> 75, 12 -> 85, 13 -> 93, 14 -> 88, 15 -> 88)
  )

}