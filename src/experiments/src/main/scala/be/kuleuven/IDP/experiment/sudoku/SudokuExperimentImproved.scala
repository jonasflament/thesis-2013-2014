package be.kuleuven.IDP.experiment.sudoku

import be.kuleuven.IDP.IDPContext
import be.kuleuven.IDP.Types._
import be.kuleuven.IDP.representation.AST.Theory
import be.kuleuven.IDP.representation.AST.Structure
import be.kuleuven.IDP.representation.AST.Structure
import be.kuleuven.IDP.mx._
import be.kuleuven.IDP.representation.AST.Structure
import be.kuleuven.IDP.representation.AST.PFDecl
import be.kuleuven.IDP.representation.AST.ElemList
import scala.util.Random
import java.io.File
import be.kuleuven.IDP.representation.AST.StructureInterpretation
import be.kuleuven.IDP.representation.AST.StructureStatement
import be.kuleuven.IDP.helper.IDPTypeWrapper

object SudokuExperimentImproved {

  def main(args: Array[String]) {
    //////////////////////////////////////////////
    // Setup the mock object for the experiment///
    setup()
    //////////////////////////////////////////////

    val puzzle = create()
    show(puzzle)
    val solution = solve(puzzle)
    println("[Debug] show solution from solve(...)")
    showtext(solution)
    show(solution)
  }

  import Voc.SimpleGridVoc._
  import Voc.SudokuVoc._

  val context = Grid.context
  import context._

  import Voc._

  val t = theory(
    !('r -> Row, 'n -> Num)(
      ?=(1, 'c -> Col)(Sudoku('r, 'c) === 'n)
    ),

    !('c -> Col, 'n -> Num)(
      ?=(1, 'r -> Row)(Sudoku('r, 'c) === 'n)
    ),

    !('b -> Block, 'n -> Num)(
      ?=(1, 'r -> Row, 'c -> Col)(
        InBlock('b, 'r, 'c) & Sudoku('r, 'c) === 'n
      )
    ),

    definition(
      !('c -> Col, 'r -> Row, 'b -> Block)(
        InBlock('b, 'r, 'c) <-- (
          'b === (('r - 1) - (('r - 1) % 3)) + (('c - 1) - (('c - 1) % 3)) / 3 + 1)
      )
    )
  )

  /**
   * Solve a sudoku, represented by a partial structure 'input'
   */
  def solve(input: Structure) = onemodel(t, input)

  /**
   * Create an empty grid
   */
  def creategrid(n: Int): Structure = {
    val grid = Grid.makesquaregrid(n)
    return grid + structure(
      Num as (1 to n),
      Block as (1 to n)
    )
  }

  /**
   * Create a new sudoku puzzle
   */
  def create(): Structure = {
    val size = 9

    println("Creating an empty grid")
    val newGrid = creategrid(size)
    println("[Debug] Showing empty grid")
    showtext(newGrid)

    println("Filling the grid until one solution is left...")
    stdoptions.nbmodels = 2

    val uniqueGrid = createUniquePuzzle(newGrid, size)

    println("Trying to remove elements ")
    val removedGrid = removeRedundantElements(uniqueGrid)

    println("[Debug] showing final grid in create()")
    showtext(removedGrid)

    return removedGrid
  }

  /**
   * Returns the number which is the result of Sudoku(row, col)
   */
  def getNum(struct: Structure, row: Int, col: Int): Option[Int] = {
    val table = asTupleTable(struct, Sudoku)
    val res = table.cts.find(x => x(0).asInt == row && x(1).asInt == col)
    if (res.isDefined)
      Some(res.get(2).asInt)
    else
      None
  }

  /**
   * Returns a random number between 'start' and 'end', inclusively.
   */
  def randomIn(start: Int, end: Int): Int = Random.shuffle((start to end).toList).head

  /**
   * Returns a square whose value differs in the first two solutions
   */
  def findDifferentiatingSquare(sols: List[Structure], size: Int): (Int, Int, Int) = {
    val row = randomIn(1, size)
    val col = randomIn(1, size)
    val num = getNum(sols.head, row, col).get
    val otherNum = getNum(sols.tail.head, row, col).get

    if (num == otherNum) {
      findDifferentiatingSquare(sols, size)
    } else {
      (row, col, num)
    }
  }

  /**
   * Iteratively creates a puzzle which has a unique solution
   */
  def createUniquePuzzle(grid: Structure, size: Int): Structure = {
    val sols = modelexpand(t, grid)
    if (sols.size == 1) {
      sols.head
    } else {
      val (row, col, num) = findDifferentiatingSquare(sols, size)
      val newGrid = maketrue(grid,
        Sudoku as ((row, col) -> num)
      )
      println("[Debug] showing grid when Sudoku(col, row, num) differ in the two solution, and is made true in the grid")
      showtext(grid)
      createUniquePuzzle(newGrid, size)
    }
  }

  /**
   * Collects everything that is known to be true in the structure for the the given predicate or function declaration
   */
  def cts(s: Structure, pf: PFDecl): List[List[IDPTypeWrapper]] = asTupleTable(s, pf).cts

  /**
   * Remove redundant elements, i.e. elements which could be omitted and still generate a unique solution
   */
  def removeRedundantElements(grid: Structure): Structure = {
    for (v <- Random.shuffle(cts(grid, Sudoku))) yield {
      // extract row, col, and number from value
      val List(r, c, n) = v
      // keep the old grid in case of failure
      val gridU = makeunknown(grid, Sudoku as ((r.asInt, c.asInt) -> n.asInt))

      val currsols = modelexpand(t, gridU)
      if (currsols.size == 1) {
        println("[Debug] showing the grid when an element is removed")
        showtext(grid)
        return removeRedundantElements(gridU)
      }
    }
    grid
  }

  /**
   * ***********
   * Output
   * ***********
   */

  /**
   * Print an ascii version of a (partial solution to a) sudoku, given as a predicate table or structure
   */
  def showtext(s: Structure) = {
    println()
    for (row <- rows(s)) {
      if ((row - 1) % 3 == 0) {
        println("-" * (rows(s).size * 2 + 2))
      }
      for (col <- cols(s)) {
        if (col > 1) {
          print(" ")
          if ((col - 1) % 3 == 0) {
            print("|")
          }
        }

        print(getNum(s, row, col).getOrElse("x"))
      }
      println // end the current line
    }
    println()
  }

  def rows(s: Structure) = asIntTable(s, Col).cts
  def cols(s: Structure) = asIntTable(s, Row).cts

  /**
   * Show a (partial solution to a) sudoku on the screen using IDPDraw
   * FIXME: This is a quick and dirty solution...
   * TODO: Use a definition to calculate the visualization!
   */
  def show(s: Structure) = {
    val scale = 20
    var config = ""

    for (r <- rows(s)) {
      for (c <- cols(s)) {
        config += s"idpd_polygon(4,$r,$c,0,0,0,$scale, $scale, $scale, $scale, 0).\n"
        config += s"idpd_xpos($r,$c,${r * scale}).\n"
        config += s"idpd_ypos($r,$c,${c * scale}).\n"
      }
    }

    def getSudokuCTs = asTupleTable(s, Sudoku).cts

    for (t <- getSudokuCTs) yield {
      val List(colW, rowW, numW) = t
      config += s"idpd_text(${colW.asInt}, ${rowW.asInt}, ${numW.asInt}).\n"
    }

    for (i <- 4 to 7 by 3) {
      config += s"idpd_polygon(4,h,$i,0,0,0,${9 * scale},2,${9 * scale},2,0).\n"
      config += s"idpd_xpos(h,$i,${i * scale - 2}).\n"
      config += s"idpd_ypos(h,$i,scale).\n"
      config += s"idpd_color(h,$i,0,0,0).\n"
      config += s"idpd_polygon(4,v,$i,0,0,${9 * scale},0,${9 * scale},2,0,2).\n"
      config += s"idpd_xpos(v,$i,$scale).\n"
      config += s"idpd_ypos(v,$i,${i * scale - 2}).\n"
      config += s"idpd_color(v,$i,0,0,0).\n"
    }

    IDPDraw(config)
  }

  def setup() = new MockConfigurer().configure
}