package be.kuleuven.IDP.experiment.sudoku

import be.kuleuven.IDP.IDPContext
import be.kuleuven.IDP.representation.AST.Structure
import be.kuleuven.IDP.Types._
import be.kuleuven.IDP.representation.AST.Structure

object Grid {
  val context = new IDPContext
  import context._

  import Voc.SimpleGridVoc._

  def makegrid(nbRows: Int, nbColums: Int): Structure = {
    return structure(
      Row as (1 to nbRows),
      Col as (1 to nbColums)
    )
  }

  def makesquaregrid(dim: Int): Structure = makegrid(dim, dim)
}