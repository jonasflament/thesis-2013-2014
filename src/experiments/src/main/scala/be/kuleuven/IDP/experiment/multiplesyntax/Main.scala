package be.kuleuven.IDP.experiment.multiplesyntax

import be.kuleuven.IDP.IDPContext
import be.kuleuven.IDP.representation.AST.Formula
import be.kuleuven.IDP.representation.AST.Predicate

object Main {

  def main(args: Array[String]) {
    val c = new IDPContext
    import c._

    val p1 = Pred()
    val p2 = Pred()

    val normal = makeWithNamedSyntax(p1, p2)
    val named = makeWithNamedSyntax(p1, p2)

    println("Checking all formulas for equivalency")
    normal.zip(named).foreach {
      case (a, b) =>
        assert(a == b, s"Given formula's did not match: $a did not equal $b")
    }
    
    println("Each formula is equivalent.")
    println("Done.")
  }

  /**
   * Creates a list of formula's, using the syntax already present in IDPContext
   */
  def makeWithNormalSyntax(pred1: Predicate, pred2: Predicate): List[Formula] = {
    val c = new IDPContext
    import c._

    List(
      pred1() & pred2(),
      pred1() | pred2(),
      pred1() ==> pred2(),
      pred1() <=> pred2()
    )
  }

  /**
   * Creates a list of formula's, using the syntax NOT present in IDPContext, but added with the NamedLogicalConnectives-trait
   */
  def makeWithNamedSyntax(pred1: Predicate, pred2: Predicate): List[Formula] = {
    val c = new IDPContext with ExtendedDSL
    import c._

    List(
      pred1() and pred2(),
      pred1() or pred2(),
      pred1() implies pred2(),
      pred1() equiv pred2()
    )
  }
}