package be.kuleuven.IDP.experiment.sudoku

import be.kuleuven.IDP.IDPContext
import be.kuleuven.IDP.Types._

object Voc {
  val context = new IDPContext
  import context._

  object SimpleGridVoc {
    val Row = Type(int)
    val Col = Type(int)
  }

  object SudokuVoc {
    val Num = Type(int)
    val Block = Type(int)
    val Sudoku = Func(SimpleGridVoc.Row, SimpleGridVoc.Col)(Num)
    val InBlock = Pred(Block, SimpleGridVoc.Row, SimpleGridVoc.Col)
  }
}
