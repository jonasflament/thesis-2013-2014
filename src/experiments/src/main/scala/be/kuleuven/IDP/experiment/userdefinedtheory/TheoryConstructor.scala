package be.kuleuven.IDP.experiment.userdefinedtheory

import scala.collection.mutable.ArrayBuffer
import be.kuleuven.IDP.representation.AST.Function
import be.kuleuven.IDP.IDPContext
import be.kuleuven.IDP.Types._
import be.kuleuven.IDP.representation.AST.Formula
import be.kuleuven.IDP.representation.AST.Theory
import be.kuleuven.IDP.representation.AST.TrueForm
import be.kuleuven.IDP.representation.AST.Term
import be.kuleuven.IDP.representation.AST.IntE
import be.kuleuven.IDP.representation.AST.IntegerTerm
import Main.Qs

class TheoryConstructor(q: Qs) {
  val c = new IDPContext
  import c._

  val fs = new ArrayBuffer[Function]
  var theo = theory()

  def poseAdditionalConstraintQuestion: Unit = {
    q.poseQuestion(
      "Enter another constraint, or enter 'show' to print the current theory",
      (s: String) =>
        if (s == "show") {
          q.log("====================")
          q.log("= Resulting theory =")
          q.log("====================")

          q.log(theo.prettyprint)
        } else {
          addConstraint(parseConstraint(s))
          q.log(s"Added constraint for '$s'")
          poseAdditionalConstraintQuestion
        }
    )
  }

  def parseConstraint(s: String): Formula = {
    val fOps = (List(
      (">", (t1, t2) => t1 > t2),
      (">=", (t1, t2) => t1 >= t2),
      ("<", (t1, t2) => t1 < t2),
      ("=<", (t1, t2) => t1 =< t2),
      ("=", (t1, t2) => t1 === t2),
      ("~=", (t1, t2) => t1 ~= t2)
    ): List[(String, (Term, Term) => Formula)]).toMap

    val tOps = (List(
      ("+", (t1, t2) => t1 + t2),
      ("-", (t1, t2) => t1 - t2),
      ("%", (t1, t2) => t1 % t2),
      ("*", (t1, t2) => t1 * t2),
      ("/", (t1, t2) => t1 / t2)
    ): List[(String, (Term, Term) => Term)]).toMap

    def toTerm(s: String): Term = {
      try {
        IntegerTerm(s.toInt)
      } catch {
        case _: NumberFormatException =>
          fs.find(_.name == s).get()
      }
    }

    val a = s.split(" ")

    if (a.size == 3) {
      // [term] [fOp] [term]
      val t1 = toTerm(a(0))
      val t2 = toTerm(a(2))
      val f = fOps(a(1))

      f(t1, t2)
    } else if (a.size == 5) {
      // [term] [tOp] [term] [fOp] [term]
      val t1 = toTerm(a(0))
      val op1 = tOps(a(1))
      val t2 = toTerm(a(2))
      val op2 = fOps(a(3))
      val t3 = toTerm(a(4))

      op2(op1(t1, t2), t3)
    } else {
      throw new NotImplementedError(s"Cannot parse string '$s' (yet)")
    }
  }

  def startQuestioning {
    q.poseQuestion("Multiple integer constant functions are availablle, starting from 'a'.\n\nWhat is the end of the range?",
      { (s: String) =>
        setRangeOfFunctions(s.toCharArray.head)
        q.log(s"End of range set to '${s.toCharArray.head}' \n")
        poseAdditionalConstraintQuestion
      }
    )
  }

  def setRangeOfFunctions(end: Char): Unit = {
    fs.clear
    for (c <- ('a' to end.toLower)) {
      fs += Func(c + "")(int)
    }
  }

  def addConstraint(f: Formula): Unit = {
    theo = theo + theory(f)
  }

  def collect: Theory = theo

}