package be.kuleuven.IDP.experiment.structuregeneration

import be.kuleuven.IDP.IDPContext
import be.kuleuven.IDP.representation.AST.IDP
import be.kuleuven.IDP.representation.AST.Structure
import scala.collection.mutable.ArrayBuffer
import be.kuleuven.IDP.helper.IDPTable

object Main {

  /**
   * Creates two structure representations of the tree defined in the DemoStructure-trait
   * and asserts if these structures are equivalent
   */
  def main(args: Array[String]) {
    // creates two structures (one manually created, one automatically created)
    val manual = new ManualStructure().struct
    val auto = new AutoStructure().struct

    // assert its equivalence

    println("Checking equivalence of both structures")
    val failures = checkEquivalent(manual.breakdown, auto.breakdown)
    if(failures.isEmpty){
      println("Done: no failures detected")
    }else{
      failures.foreach(println)
    }
  }

  def checkEquivalent(a: Structure, b: Structure): List[String] = {
    import be.kuleuven.IDP.experiment.structuregeneration.TreeVoc._
    import be.kuleuven.IDP.mx._

    val failures = new ArrayBuffer[String]
    def isTableEqual[T](t1: IDPTable[T], t2: IDPTable[T]) = {
      if(t1.cts.toSet != t2.cts.toSet){
        failures += s"[CT] $t1 did not equal $t2"
      }
      if(t1.cfs.toSet != t2.cfs.toSet){
        failures += s"[CF] $t1 did not equal $t2"
      }
    }
    
    isTableEqual(asCharTable(a, TreeT), asCharTable(b, TreeT))

    for (t <- List(Left, Right, Val)) {
      val t1 = asTupleTable(a, t)
      val t2 = asTupleTable(b, t)
      isTableEqual(t1, t2)
    }
    
    failures.toList
  }
}
