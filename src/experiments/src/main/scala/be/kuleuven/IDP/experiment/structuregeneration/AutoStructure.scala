package be.kuleuven.IDP.experiment.structuregeneration

import TreeVoc._
import be.kuleuven.IDP.representation.AST.Structure

/**
 * Automatically creates a structure of the Trees described
 * in the DemoStructure-trait
 */
class AutoStructure extends StructureTrait {
  val A =
    Node('A',
      Leaf('B'),
      Leaf('C')
    )

  val B =
    Node('O',
      Node('P',
        Leaf('Q'),
        Leaf('R')),
      Leaf('S')
    )

  val structurer = new TreeStructurer
  def struct = structurer.structureForTrees(List(A, B))
}

abstract class ATree
case class Node(value: Char, left: ATree, right: ATree) extends ATree
case class Leaf(value: Char) extends ATree

/**
 * Creates structures for any given trees
 */
class TreeStructurer {
  import TreeVoc.context._

  def TreeTI(a: ATree): Char = a match {
    case Node(v, _, _) => v
    case Leaf(v) => v
  }

  def LeftI(a: ATree): (Char, Char) = a match {
    case Node(_, l, _) => (TreeTI(a), TreeTI(l))
    case l: Leaf => null
  }

  def RightI(a: ATree): (Char, Char) = a match {
    case Node(_, _, r) => (TreeTI(a), TreeTI(r))
    case l: Leaf => null
  }

  def ValI(a: ATree): (Char, Char) = (TreeTI(a), TreeTI(a))

  def collect[T](a: ATree, f: ATree => T): List[T] = {
    val r = a match {
      case n @ Node(v, l, r) => List(f(n)) ++ collect(l, f) ++ collect(r, f)
      case n: Leaf => List(f(n))
    }
    r.filter(_ != null)
  }

  def collect[T](as: List[ATree], f: ATree => T): List[T] = {
    as.map(collect(_, f)).flatten
  }

  def structureForTrees(a: List[ATree]): Structure = {
    structure(
      TreeT as (collect(a, TreeTI _): _*),
      Left as (collect(a, LeftI _): _*),
      Right as (collect(a, RightI _): _*)
    )
  }
}