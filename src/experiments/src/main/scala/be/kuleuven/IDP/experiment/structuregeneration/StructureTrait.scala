package be.kuleuven.IDP.experiment.structuregeneration

import be.kuleuven.IDP.representation.AST.Structure

/**
 * A trait which defines a method creating a structure representing
 * a certain tree to be used in the "Structure generation" demo
 */
trait StructureTrait {

  /**
   * Creates a structure which represents the given tree
   * A
   * -- B (left)
   * -- C (right)
   *
   * O
   * -- P
   * ---- Q
   * ---- R
   * -- S
   */
  def struct : Structure
  
}