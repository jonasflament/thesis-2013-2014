package be.kuleuven.IDP.experiment.sudoku

import be.kuleuven.IDP.IDPContext
import be.kuleuven.IDP.Types._
import be.kuleuven.IDP.representation.AST.Theory
import be.kuleuven.IDP.representation.AST.Structure
import be.kuleuven.IDP.representation.AST.Structure
import be.kuleuven.IDP.mx._
import be.kuleuven.IDP.representation.AST.Structure
import be.kuleuven.IDP.representation.AST.PFDecl
import be.kuleuven.IDP.representation.AST.ElemList
import scala.util.Random
import java.io.File
import be.kuleuven.IDP.representation.AST.StructureInterpretation
import be.kuleuven.IDP.representation.AST.StructureStatement
import be.kuleuven.IDP.helper.IDPTypeWrapper

object SudokuExperiment {

  def main(args: Array[String]) {
    //////////////////////////////////////////////
    // Setup the mock object for the experiment///
    setup()
    //////////////////////////////////////////////

    val puzzle = create()
    show(puzzle)
    val solution = solve(puzzle)
    println("[Debug] show solution from solve(...)")
    showtext(solution)
    show(solution)
  }

  import Voc.SimpleGridVoc._
  import Voc.SudokuVoc._

  val context = Grid.context
  import context._

  val t = theory(
    !('r -> Row, 'n -> Num)(
      ?=(1, 'c -> Col)(Sudoku('r, 'c) === 'n)
    ),

    !('c -> Col, 'n -> Num)(
      ?=(1, 'r -> Row)(Sudoku('r, 'c) === 'n)
    ),

    !('b -> Block, 'n -> Num)(
      ?=(1, 'r -> Row, 'c -> Col)(
        InBlock('b, 'r, 'c) & Sudoku('r, 'c) === 'n
      )
    ),

    definition(
      !('c -> Col, 'r -> Row, 'b -> Block)(
        InBlock('b, 'r, 'c) <-- (
          'b === (('r - 1) - (('r - 1) % 3)) + (('c - 1) - (('c - 1) % 3)) / 3 + 1)
      )
    )
  )

  /**
   * Solve a sudoku, represented by a partial structure 'input'
   */
  def solve(input: Structure) = onemodel(t, input)

  /**
   * Create an empty grid
   */
  def creategrid(n: Int): Structure = {
    val grid = Grid.makesquaregrid(n)
    return grid + structure(
      Num as (1 to n),
      Block as (1 to n)
    )
  }

  /**
   * Returns the number which is the result of Sudoku(row, col)
   */
  def getNum(struct: Structure, row: Int, col: Int): Option[Int] = {
    val table = asTupleTable(struct, Sudoku)
    val res = table.cts.find(x => x(0).asInt == row && x(1).asInt == col)
    if (res.isDefined)
      Some(res.get(2).asInt)
    else
      None
  }

  /**
   * Create a new sudoku puzzle
   */
  def create(): Structure = {
    val size = 9

    val r = Random
    def nextInt(start: Int, end: Int): Int = r.nextInt(end - start) + start

    println("Creating an empty grid")
    var grid = creategrid(size)
    println("[Debug] Showing empty grid")
    showtext(grid)

    println("Filling the grid until one solution is left...")
    stdoptions.nbmodels = 2

    var currsols = modelexpand(t, grid)
    var col: Int = 1
    var row: Int = 1
    var num: Int = 1

    while (currsols.size > 1) {
      do {
        col = nextInt(1, size)
        row = nextInt(1, size)
        num = getNum(currsols(0), row, col).get
      } while (num == getNum(currsols(1), row, col).get)

      grid = maketrue(grid,
        Sudoku as ((row, col) -> num)
      )

      println("[Debug] showing grid when Sudoku(col, row, num) differ in the two solution, and is made true in the grid")
      showtext(grid)
      currsols = modelexpand(t, grid)
    }
    // XXX: hack to create a full grid, because mock only returns a certain amount of models
    // when the IDP system would be used, a complete solution would have been created when the while loop terminates
    grid = currsols.head

    def cts(s: Structure, pf: PFDecl): List[List[IDPTypeWrapper]] = asTupleTable(s, pf).cts

    println("Trying to remove elements ")

    var change = true
    while (change) {
      change = false
      val cttab = Random.shuffle(cts(grid, Sudoku))
      for (v <- cttab) {
        if (!change) {
          // extract row, col, and number from value
          val List(r, c, n) = v
          // keep the old grid in case of failure
          val oldGrid = grid

          grid = makeunknown(grid, Sudoku as ((r.asInt, c.asInt) -> n.asInt))

          currsols = modelexpand(t, grid)
          if (currsols.size == 1) {
            change = true
            println("[Debug] showing the grid when an element is removed")
            showtext(grid)
          } else {
            // reset the grid back to its own grid
            grid = oldGrid
          }
        }
      }
    }

    println("[Debug] showing final grid in create()")
    showtext(grid)

    return grid
  }

  /**
   * ***********
   * Output
   * ***********
   */

  /**
   * Print an ascii version of a (partial solution to a) sudoku, given as a predicate table or structure
   */
  def showtext(s: Structure) = {
    println()
    for (row <- rows(s)) {
      if ((row - 1) % 3 == 0) {
        println("-" * (rows(s).size * 2 + 2))
      }
      for (col <- cols(s)) {
        if (col > 1) {
          print(" ")
          if ((col - 1) % 3 == 0) {
            print("|")
          }
        }

        print(getNum(s, row, col).getOrElse("x"))
      }
      println // end the current line
    }
    println()
  }

  def rows(s: Structure) = asIntTable(s, Col).cts
  def cols(s: Structure) = asIntTable(s, Row).cts

  /**
   * Show a (partial solution to a) sudoku on the screen using IDPDraw
   * FIXME: This is a quick and dirty solution...
   * TODO: Use a definition to calculate the visualization!
   */
  def show(s: Structure) = {
    val scale = 20
    var config = ""

    for (r <- rows(s)) {
      for (c <- cols(s)) {
        config += s"idpd_polygon(4,$r,$c,0,0,0,$scale, $scale, $scale, $scale, 0).\n"
        config += s"idpd_xpos($r,$c,${r * scale}).\n"
        config += s"idpd_ypos($r,$c,${c * scale}).\n"
      }
    }

    def getSudokuCTs = asTupleTable(s, Sudoku).cts

    for (t <- getSudokuCTs) yield {
      val List(colW, rowW, numW) = t
      config += s"idpd_text(${colW.asInt}, ${rowW.asInt}, ${numW.asInt}).\n"
    }

    for (i <- 4 to 7 by 3) {
      config += s"idpd_polygon(4,h,$i,0,0,0,${9 * scale},2,${9 * scale},2,0).\n"
      config += s"idpd_xpos(h,$i,${i * scale - 2}).\n"
      config += s"idpd_ypos(h,$i,scale).\n"
      config += s"idpd_color(h,$i,0,0,0).\n"
      config += s"idpd_polygon(4,v,$i,0,0,${9 * scale},0,${9 * scale},2,0,2).\n"
      config += s"idpd_xpos(v,$i,$scale).\n"
      config += s"idpd_ypos(v,$i,${i * scale - 2}).\n"
      config += s"idpd_color(v,$i,0,0,0).\n"
    }

    IDPDraw(config)
  }

  def setup() = new MockConfigurer().configure
}