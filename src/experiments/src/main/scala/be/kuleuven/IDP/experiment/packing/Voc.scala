package be.kuleuven.IDP.experiment.packing

import be.kuleuven.IDP.IDPContext
import be.kuleuven.IDP.Types.int
import be.kuleuven.IDP.Types.nat
import be.kuleuven.IDP.Types.string

object Voc {
  val c = new IDPContext
  import c._

  object Draw {
    val ColorValue = Type(nat)
    val square = Pred(string, nat)
    val xpos = Pred(string, nat)
    val ypos = Pred(string, nat)
    val color = Pred(string, ColorValue, ColorValue, ColorValue)
    val text = Pred(string, string)
  }
  
  object Packing {
    val Square = Type(nat)
    val SizeT = Type(nat)
    val X = Type(int)
    val Y = Type(int)

    /* Properties of the area */
    val Width = Func()(X)
    val Height = Func()(Y)

    /* Properties of squares */
    val Size = Func(Square)(SizeT)
    val XPos = Func(Square)(X)
    val YPos = Func(Square)(Y)
    val ValidPos = Pred(Square)
    val Left = Pred(Square, Square)
    val Up = Pred(Square, Square)
    val NoOverlap = Pred(Square, Square)
    val Largest = Func()(Square)
  }

}