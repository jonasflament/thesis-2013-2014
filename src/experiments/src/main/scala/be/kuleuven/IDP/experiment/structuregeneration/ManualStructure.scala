package be.kuleuven.IDP.experiment.structuregeneration

/**
 * Provides a manual created representation
 */
class ManualStructure extends StructureTrait {
  import TreeVoc._
  import TreeVoc.context._

  def struct = structure(
    TreeT as ('A' to 'C', 'O' to 'S'),

    Left as (
      ('A', 'B'),
      ('O', 'P'),
      ('P', 'Q')
    ),

    Right as (
      ('A', 'C'),
      ('P', 'R'),
      ('O', 'S')
    )
  )
}