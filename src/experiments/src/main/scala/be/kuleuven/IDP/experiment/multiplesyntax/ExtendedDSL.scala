package be.kuleuven.IDP.experiment.multiplesyntax

import be.kuleuven.IDP.IDPContext
import be.kuleuven.IDP.representation.AST.Formula
import be.kuleuven.IDP.representation.AST.TypeDecl

/**
 * Traits which adds named logical connectives
 */
trait NamedLogicalConnectives extends IDPContext {
  implicit class FWrapper(_wrappedF: Formula) {
    def and(f: Formula): Formula = _wrappedF & f

    def or(f: Formula): Formula = _wrappedF | f

    def equiv(f: Formula): Formula = _wrappedF <=> f

    def implies(f: Formula): Formula = _wrappedF ==> f
  }
}

/**
 * Traits which add named quantifiers
 */
trait NamedQuantifiers extends IDPContext {
  def exists(a: (Symbol, TypeDecl)*)(f: Formula): Formula = ?(a: _*)(f)
}

/**
 * An extended DSL with named logical connectives, and named quantifiers
 */
trait ExtendedDSL extends NamedLogicalConnectives with NamedQuantifiers