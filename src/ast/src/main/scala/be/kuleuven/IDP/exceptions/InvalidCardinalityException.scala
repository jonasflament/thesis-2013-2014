package be.kuleuven.IDP.exceptions

class InvalidCardinalityException(name: String, nbRequired: Int, nbReceived: Int) extends RuntimeException {
	
  override def getMessage() : String = s"$name requires $nbRequired arguments, received $nbReceived instead"
}