package be.kuleuven.IDP.representation.AST

//data ThreeValued = ThreeValued Variable (Maybe String)
case class ThreeValued(v: PFDecl, s: Option[ThreeValuedIdentifier]) extends IDP{// deriving (Eq,Ord,Show,Typeable,Data)
	override def prettyprint = {
	  val tagS = s match {
	    case Some(id) => s"<${id.prettyprint}>"
	    case None => ""
	  }
	  v.prettyprint + tagS
	} 
}

abstract class ThreeValuedIdentifier extends IDP

/**
 * Certainly true
 */
case object CT extends ThreeValuedIdentifier{
  override def prettyprint = "ct"
}

/**
 * Certainly false
 */
case object CF extends ThreeValuedIdentifier{
  override def prettyprint = "cf"
}

/**
 * Unknown
 */
case object U extends ThreeValuedIdentifier {
  override def prettyprint = "u"
}
