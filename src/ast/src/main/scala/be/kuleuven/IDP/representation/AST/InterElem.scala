package be.kuleuven.IDP.representation.AST

//data InterElem =
abstract class InterElem extends IDP { // deriving (Eq,Ord,Show,Typeable,Data)
  def breakdown: List[InterElem] = List(this)
}

case class FloatE(f: Float) extends InterElem {
  override def prettyprint = f + ""
}

// CharRange Char Char
case class CharRange(c: Char, c2: Char) extends InterElem {
  override def prettyprint = s"{$c..$c2}"
  override def breakdown = (c to c2).toList.map(CharE(_))
}
//				|IntRange Integer Integer
case class IntRange(i: Int, i2: Int) extends InterElem {
  override def prettyprint = s"{$i..$i2}"
  override def breakdown = (i.toInt to i2.toInt).toList.map(IntE(_))
}
//				|StringE String
case class StringE(s: String) extends InterElem {
  override def prettyprint = s
}
//				|CharE Char
case class CharE(c: Char) extends InterElem {
  override def prettyprint = c + ""
}
//				|IntE Integer
case class IntE(i: Int) extends InterElem {
  override def prettyprint = i + ""
}
//				|InterList [InterElem]
case class InterList(elems: List[InterElem]) extends InterElem {
  override def prettyprint = "(" + elems.map(_.prettyprint).mkString(", ") + ")"
}