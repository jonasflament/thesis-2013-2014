package be.kuleuven.IDP.representation.AST

//data StructureStatement =
abstract class StructureStatement extends IDP { // deriving (Eq,Ord,Show,Typeable,Data
  def pfdecl: PFDecl

  def isCT: Boolean
  def isCF: Boolean
  def isOther: Boolean = !(isCT | isCF)

  def asCT: StructureStatement
  def asCF: StructureStatement

  /**
   * Breaks the structure statement down to its smallest elements
   */
  def breakdown : StructureStatement
  
  def pf : PFDecl
  
}
// InterDecl ThreeValued StructureInterpretation
case class InterDecl(threeV: ThreeValued, structI: StructureInterpretation) extends StructureStatement {
  override def prettyprint = threeV.prettyprint + " = " + structI.prettyprint
  override def pfdecl = threeV.v

  override def isCT = threeV.s == Some(CT)
  override def isCF = threeV.s == Some(CF)

  override def asCT = copy(threeV = threeV.copy(s = Some(CT)))
  override def asCF = copy(threeV = threeV.copy(s = Some(CF)))
  
  override def breakdown = copy(structI = structI.breakdown)
  
  override def pf = threeV.v
}