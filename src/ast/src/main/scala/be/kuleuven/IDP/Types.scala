package be.kuleuven.IDP

import be.kuleuven.IDP.representation.AST.TypeDecl

object Types {
  val nat = TypeDecl("Nat", None, None, None)
  val int = TypeDecl("Int", None, None, Some(nat))
  
  val float = TypeDecl("float", None, None, None)
  
  val char = TypeDecl("Char", None, None, None)
  val string = TypeDecl("String", None, None, Some(char))
  
}