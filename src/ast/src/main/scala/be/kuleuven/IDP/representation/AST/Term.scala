package be.kuleuven.IDP.representation.AST

import scala.language.implicitConversions

import be.kuleuven.IDP.Types
import be.kuleuven.IDP.representation.AST.ASTTypes.Name

//data Term = 
abstract class Term extends IDP // deriving (Eq,Ord,Show,Typeable,Data)
{
  def +(other: Term) = ConnectedT(Plus, this, other)
  def -(other: Term) = ConnectedT(Minus, this, other)

  def *(other: Term) = ConnectedT(Mult, this, other)
  def /(other: Term) = ConnectedT(Div, this, other)

  def %(other: Term) = ConnectedT(Mod, this, other)

  def ^(other: Term) = ConnectedT(Pow, this, other)

  def ===(tOther: Term): EqChainForm = {
    return EqChainForm(this, List((EQ, tOther)))
  }

  def ~=(tOther: Term): EqChainForm = {
    return EqChainForm(this, List((NEQ, tOther)))
  }

  def >(tOther: Term): EqChainForm = {
    return EqChainForm(this, List((GT, tOther)))
  }

  def <(tOther: Term): EqChainForm = {
    return EqChainForm(this, List((LT, tOther)))
  }

  def =<(tOther: Term): EqChainForm = {
    return EqChainForm(this, List((LEQ, tOther)))
  }

  def >=(tOther: Term): EqChainForm = {
    return EqChainForm(this, List((GEQ, tOther)))
  }

  def ===(chain: EqChainForm): EqChainForm = {
    val EqChainForm(t, acc) = chain
    return EqChainForm(this, List((EQ, t)) ++ acc)
  }

  def ~=(chain: EqChainForm): EqChainForm = {
    val EqChainForm(t, acc) = chain
    return EqChainForm(this, List((NEQ, t)) ++ acc)
  }

  def <(chain: EqChainForm): EqChainForm = {
    val EqChainForm(t, acc) = chain
    return EqChainForm(this, List((LT, t)) ++ acc)
  }

  def >(chain: EqChainForm): EqChainForm = {
    val EqChainForm(t, acc) = chain
    return EqChainForm(this, List((GT, t)) ++ acc)
  }

  def =<(chain: EqChainForm): EqChainForm = {
    val EqChainForm(t, acc) = chain
    return EqChainForm(this, List((LEQ, t)) ++ acc)
  }

  def >=(chain: EqChainForm): EqChainForm = {
    val EqChainForm(t, acc) = chain
    return EqChainForm(this, List((GEQ, t)) ++ acc)
  }

  def prettyprint: String
}

object Term {
  import scala.language.implicitConversions

  implicit def charToCharLiteralTerm(c: Char) = CharLiteralTerm(c)

  implicit def stringToStringLiteralTerm(s: String) = StringLiteralTerm(s)

  implicit def intToIntegerTerm(i: Int) = IntegerTerm(i)

  @deprecated("Type checking should be left to IDP", "0.0.0")
  def typeCheck(args: List[Term], argTypes: List[TypeDecl]): Boolean = {
    if (args.size != argTypes.size)
      return false
    else {
      return args.zip(argTypes).forall {
        case (arg: Term, argType: TypeDecl) =>
          arg match {
            case IntegerTerm(i) =>
              argType.superType == Some(Types.int) // a TypeDecl does not always have a subType

            case CharLiteralTerm(c) =>
              argType.superType == Some(Types.char)

            case StringLiteralTerm(c) =>
              argType.superType == Some(Types.string)

            case FunctionTerm(f, _) =>
              argType == f.retType

            case _ => false
          }
      }
    }
  }

}

case object TrueTerm extends Term { override def prettyprint = "true" }
case object FalseTerm extends Term { override def prettyprint = "false" }

// AtomTerm Atom
//case class AtomTerm(a: Atom) extends Term // TODO: remove
case class FunctionTerm(f: Function, args: List[Term]) extends Term {
  override def prettyprint = {
    val argsS = if (args.size == 0) {
      ""
    } else {
      "(" + { args.map(_.prettyprint).mkString(",") } + ")"
    }

    s"${f.name.mkString("::")}$argsS"
  }
}

//			|IntegerTerm Integer
case class IntegerTerm(i: Int) extends Term {
  override def prettyprint = i + ""

  def +(other: IntegerTerm) = { val IntegerTerm(j) = other; IntegerTerm(i + j) } // ConnectedT(Plus, this, other)
  def -(other: IntegerTerm) = { val IntegerTerm(j) = other; IntegerTerm(i - j) } //ConnectedT(Minus, this, other)

  def *(other: IntegerTerm) = { val IntegerTerm(j) = other; IntegerTerm(i * j) } // ConnectedT(Mult, this, other)
  def /(other: IntegerTerm) = { val IntegerTerm(j) = other; IntegerTerm(i / j) } // ConnectedT(Div, this, other)

  def %(other: IntegerTerm) = { val IntegerTerm(j) = other; IntegerTerm(i % j) } // ConnectedT(Mod, this, other)
  def abs: Term = { IntegerTerm(i.abs) }
  def unary_- = { IntegerTerm(-i) }
}

//			|StringLiteralTerm String
case class StringLiteralTerm(s: String) extends Term {
  override def prettyprint = s
}

//			|CharLiteralTerm Char
case class CharLiteralTerm(c: Char) extends Term {
  override def prettyprint = c + ""
}

//			|ConnectedT TOperator Term Term
case class ConnectedT(tOp: TOperator, t1: Term, t2: Term) extends Term {
  override def prettyprint = s"${t1.prettyprint} ${tOp.prettyprint} ${t2.prettyprint}"
}

//			|AggTerm Aggregate
case class AggTerm(a: Aggregate) extends Term {
  override def prettyprint = a.prettyprint
}

//data Variable = Variable Name TypeRef
/**
 * Variable x in forall(x: Int)
 */
// Changed it to TypeDecl since TypeRefs are probably not going to be used
case class Variable(name: Name, tRef: TypeDecl) extends Term { // deriving (Eq,Ord,Show,Typeable,Data)
  override def prettyprint = s"'${name} -> ${tRef.prettyprint}"
}

/**
 * Variable x in P(x)
 */
case class VariableSym(name: String) extends Term {
  override def prettyprint = s"'$name"
}