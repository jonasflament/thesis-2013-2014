package be.kuleuven.IDP.representation.AST

//data FOperator =
abstract class FOperator extends IDP// deriving (Eq,Ord,Show,Typeable,Data)
// And
case object And extends FOperator{  override def prettyprint = "&"}
// | Or
case object Or extends FOperator{  override def prettyprint = "|"}
// | Impl
case object Impl extends FOperator {  override def prettyprint = "=>"}
// | Equiv
case object Equiv extends FOperator {   override def prettyprint = "<=>"}