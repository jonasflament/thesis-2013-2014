package be.kuleuven.IDP.representation.AST

import be.kuleuven.IDP.exceptions.InvalidCardinalityException
import be.kuleuven.IDP.representation.AST.ASTTypes.Name

//data VocabularyStatement =
abstract class VocabularyStatement extends IDP // deriving (Eq,Ord,Show,Typeable,Data)

// ExternVocabulary Name
case class ExternVocabulary(name: Name) extends VocabularyStatement {
  override def prettyprint = "external " + name.mkString("::")
}

// |ExternPF Name (Maybe (Integer, Bool))
case class ExternPf(name: Name, iB: Option[(Integer, Boolean)]) extends VocabularyStatement {
  override def prettyprint = "<prettyprinting ExternPf is not supported yet>"
}

// |TypeDecl Name (Maybe TypeInterpretation) (Maybe [Name]) (Maybe [Name])
case class TypeDecl(name: String, interpretation: Option[TypeInterpretation], superType: Option[TypeDecl], subType: Option[TypeDecl]) extends VocabularyStatement {
  override def prettyprint = name

  def apply(t: Term): Formula = {
    val p = Predicate(name, List(this)) // TODO: get real predicate from connectivity layer
    p(t)
  }
}

// |PFDecl Name (Maybe [Name]) (Maybe Name)
abstract class PFDecl extends VocabularyStatement {
  def arity: Int
  def name: String

  def types: List[TypeDecl]
}

case class Function(name: String, argTypes: List[TypeDecl], retType: TypeDecl) extends PFDecl {
  override def prettyprint = name + "(" + argTypes.map(_.name).mkString(",") + ") : " + retType.name

  override def arity = argTypes.size

  def apply(args: Term*): Term = {
    if (args.size != arity) {
      throw new InvalidCardinalityException("Predicate " + name, arity, args.size)
    }
    //    if (!Term.typeCheck(args.toList, argTypes)) {
    //      throw new IllegalArgumentException(s"Function $name expects arguments of type ${argTypes}, but was provided ${args}")
    //    }

    return FunctionTerm(this, args.toList)
  }

  override def types: List[TypeDecl] = argTypes :+ retType
}

case class Predicate(name: String, argTypes: List[TypeDecl]) extends PFDecl {
  override def prettyprint = name + "(" + argTypes.map(_.name).mkString(",") + ")"
  override def arity = argTypes.size

  def apply(args: Term*): PredicateForm = {
    if (args.size != arity) {
      throw new InvalidCardinalityException("Predicate " + name, arity, args.size)
    }
    //    if (!Term.typeCheck(args.toList, argTypes)) {
    //      throw new IllegalArgumentException(s"Predicate $name expects arguments ${argTypes}, but was provided ${args}")
    //    }

    return PredicateForm(this, args.toList)
  }
  override def types: List[TypeDecl] = argTypes
}