package be.kuleuven.IDP.representation.AST

import be.kuleuven.IDP.representation.AST.ASTTypes.Name
import be.kuleuven.IDP.representation.AST.ASTTypes.TypeRef

//data Atom = Atom Name TypeRef (Maybe [Term])
case class Atom(name: Name, typeRef: TypeRef, terms: Option[List[Term]]) { // deriving (Eq,Ord,Show,Typeable,Data)
}