package be.kuleuven.IDP.representation.AST

//data TOperator = 
abstract class TOperator extends IDP { // deriving (Eq,Ord,Show,Typeable,Data)
  def prettyprint: String
}
// Minus
case object Minus extends TOperator { override def prettyprint = "-" }
// | Plus
case object Plus extends TOperator { override def prettyprint = "+" }
// | Mult
case object Mult extends TOperator { override def prettyprint = "*" }
// | Div
case object Div extends TOperator { override def prettyprint = "/" }
// | Mod
case object Mod extends TOperator { override def prettyprint = "%" }
// | Pow
case object Pow extends TOperator { override def prettyprint = "^" }