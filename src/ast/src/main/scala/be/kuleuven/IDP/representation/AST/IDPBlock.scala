package be.kuleuven.IDP.representation.AST

import be.kuleuven.IDP.representation.AST.ASTTypes.Name
import be.kuleuven.IDP.representation.AST.ASTTypes.VocabularyName

//data IDPBlock = 
abstract class IDPBlock extends IDP // deriving (Eq,Ord,Show,Typeable,Data)

// Theory String VocabularyName [TheoryStatement] 
case class Theory(name: String, vocabularyName: VocabularyName, theoStmts: List[TheoryStatement]) extends IDPBlock {
  override def prettyprint = "theory " + name + " {\n" + theoStmts.map("\t" + _.prettyprint + ".").mkString("\n") + "\n}"
  
  /**
   * Joins the current and the given theories together into one theory
   */
  def +(t: Theory): Theory = {
    Theory(name + t.name, null, theoStmts ++ t.theoStmts)
  }
}

//				| Structure String VocabularyName [StructureStatement] 
case class Structure(name: String, vocabularyName: VocabularyName, stmts: List[StructureStatement]) extends IDPBlock {
override def prettyprint = "structure " + name + " {\n" + stmts.map("\t" + _.prettyprint + ".").mkString("\n") + "\n}"

  def +(other: Structure): Structure = {
    Structure(name + "-" + other.name, vocabularyName, stmts ++ other.stmts)
  }

  def breakdown: Structure = {
    copy(stmts = stmts.map(_.breakdown))
  }

  /**
   * Overrides the current interpretation of s.pf with the given statement
   */
  def update(s: StructureStatement): Structure = {
    copy(stmts = stmts.filter(x => x.pf != s.pf) :+ s)
  }

}

//				| Vocabulary String [VocabularyStatement] 
case class Vocabulary(name: String, stmts: List[VocabularyStatement]) extends IDPBlock{
  override def prettyprint = "structure " + name + " {\n" + stmts.map("\t" + _.prettyprint + ".").mkString("\n") + "\n}"
}

//				| Lua String [String] String
case class Lua(s: String, ss: List[String], s2: String) extends IDPBlock{
  override def prettyprint = "<prettyprinting luacode is not supported yet>"
}

//				| UsingNS Name
case class UsingNs(name: Name) extends IDPBlock{
  override def prettyprint = "<prettyprinting UsingNs is not supported yet>"
}

//				| IncludeLib String
case class IncludeLib(s: String) extends IDPBlock{
  override def prettyprint = "<prettyprinting IncludeLib is not supported yet>"
}

//				| IncludeFile String
case class IncludeFile(s: String) extends IDPBlock{
  override def prettyprint = "<prettyprinting IncludeFile is not supported yet>"
}

//				| TermBlock String VocabularyName Term
case class TermBlock(s: String, vocName: VocabularyName, t: Term) extends IDPBlock{
  override def prettyprint = "term " + s + " {\n" + "\t" + t.prettyprint + "\n}\n"
}

//				| QueryBlock String VocabularyName Query
case class QueryBlock(s: String, vocName: VocabularyName, q: Query) extends IDPBlock{
  override def prettyprint = "<prettyprinting query block is not supported yet>"
}

//				| FactList String VocabularyName [Atom]
case class FactList(s: String, vocName: VocabularyName, structStmts: List[StructureStatement]) extends IDPBlock {
  override def prettyprint = "<prettyprinting factlist is not supported yet>"
}

//				| Namespace String [IDPBlock]
case class Namespace(s: String, IDP: List[IDPBlock]) extends IDPBlock {
  override def prettyprint = "<prettyprinting namespace is not supported yet>"
}