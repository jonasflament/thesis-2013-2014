package be.kuleuven.IDP.representation.AST

//data Aggregate =
abstract class Aggregate extends Term // deriving (Eq,Ord,Show,Typeable,Data)

//   Card Query
case class Card(q: Query) extends Aggregate {
  override def prettyprint = s"#(${q.prettyprint})"
}

// | OtherAgg AggType AggArg
case class OtherAgg(aggType: AggType, aggArg: AggArg) extends Aggregate {
  override def prettyprint = s"${aggType.prettyprint}${aggArg.prettyprint}"
}

//data AggType =
abstract class AggType extends IDP // deriving (Eq,Ord,Show,Typeable,Data)
// ASum
case object ASum extends AggType { override def prettyprint = "sum" }
// 				|AProd
case object AProd extends AggType { override def prettyprint = "prod" }
// 				|AMin
case object AMin extends AggType { override def prettyprint = "min" }
// 				|AMax
case object AMax extends AggType { override def prettyprint = "max" }

//data AggArg =
abstract class AggArg extends IDP { // deriving (Eq,Ord,Show,Typeable,Data)
  def prettyprint: String
}
// TList [Term]
case class TList(terms: List[Term]) extends AggArg {
  override def prettyprint = "(" + terms.map(_.prettyprint).mkString(",") + ")"
}
// TSet [Variable] Formula Term
case class TSet(vars: List[Variable], f: Formula, t: Term) extends AggArg {
  override def prettyprint = "(" + vars.map(_.prettyprint).mkString(",") + " : " + f.prettyprint + " : " + t.prettyprint + ")"
}