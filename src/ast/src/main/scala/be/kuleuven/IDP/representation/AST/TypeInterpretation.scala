package be.kuleuven.IDP.representation.AST

//data TypeInterpretation =
abstract class TypeInterpretation extends IDP// deriving (Eq,Ord,Show,Typeable,Data)
// Constructed [VocabularyStatement]
case class Constructed(vocStmts: List[VocabularyStatement]) extends TypeInterpretation{
  override def prettyprint = "<prettyprinting Constructed is not supported yet>"
}
// |Assignment StructureInterpretation
case class Assignment(structI: StructureInterpretation) extends TypeInterpretation{
  override def prettyprint = "<prettyprinting Assignement is not supported yet>"
}