package be.kuleuven.IDP.representation.AST

object ASTTypes {

  //  type Name = [String]
  type Name = List[String]

  // type VocabularyName = Name
  type VocabularyName = Name

  // type TypeRef = Maybe ([Name],Maybe Name)
  type TypeRef = Option[(List[Name], Option[Name])]

  // type IDPFile = [IDPBlock]
  type IDPFile = List[IDPBlock]
}
