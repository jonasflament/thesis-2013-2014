package be.kuleuven.IDP.representation.AST

import be.kuleuven.IDP.representation.AST.ASTTypes.Name

//data StructureInterpretation =
abstract class StructureInterpretation extends IDP{// deriving (Eq,Ord,Show,Typeable,Data)
	def breakdown : StructureInterpretation
}
// ElemList [InterElem] 
case class ElemList(elems: List[InterElem]) extends StructureInterpretation {
  override def prettyprint = elems.map(_.prettyprint).mkString(",")
  override def breakdown = copy(elems = elems.map(_.breakdown).flatten)
}
// |EProcedure Name
case class EProcedure(name: Name) extends StructureInterpretation {
  override def prettyprint = s"procedure ${name.mkString("::")}"
  override def breakdown = this
}