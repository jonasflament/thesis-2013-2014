package be.kuleuven.IDP.representation.AST

//data Formula =
abstract class Formula extends TheoryStatement // deriving (Eq,Ord,Show,Typeable,Data)
{
  def &(f: Formula): Formula = {
    return ConnectedF(And, this, f)
  }

  def |(f: Formula): Formula = {
    return ConnectedF(Or, this, f)
  }

  def ==>(f: Formula): Formula = {
    return ConnectedF(Impl, this, f)
  }

  def <==(f: Formula): Formula = {
    return ConnectedF(Impl, f, this)
  }

  def <=>(f: Formula): Formula = {
    return ConnectedF(Equiv, this, f)
  }

  def unary_~ : Formula = {
    return Not(this)
  }

  def <--(f: Formula): Rule = {
    return Rule(List(), FormulaHead(this), Some(f))
  }

  /**
   * Creates a rule given a boolean
   *
   * @pre b must be true
   *
   * @exception IllegalArgumentException when b is false
   */
  def <--(b: Boolean): Rule = {
    if (b) {
      return Rule(List(), FormulaHead(this), None)
    } else {
      return Rule(List(), FormulaHead(this), Some(FalseForm))
    }
  }

}

case object TrueForm extends Formula { override def prettyprint = "true" }
case object FalseForm extends Formula { override def prettyprint = "false" }

// AtomForm Atom
//case class AtomForm(a: Atom) extends Formula // TODO: remove

// Not Formula
case class Not(formula: Formula) extends Formula {
  override def prettyprint = "~" + formula.prettyprint
}

// ConnectedF FOperator Formula Formula
case class ConnectedF(fOp: FOperator, f1: Formula, f2: Formula) extends Formula {
  override def prettyprint = f1.prettyprint + " " + fOp.prettyprint + " " + f2.prettyprint
}

// Quantified Quantification Formula
case class Quantified(quantification: Quantification, formula: Formula) extends Formula {
  override def prettyprint = quantification.prettyprint + formula.prettyprint
}

//data EqChain = EqChain Term [(CompType,Term)] 
case class EqChainForm(term: Term, terms: List[(CompType, Term)]) extends Formula { // deriving (Eq,Ord,Show,Typeable,Data)

  override def prettyprint = term.prettyprint + " " + terms.map { case (ct, t) => ct.prettyprint + " " + t.prettyprint }.mkString(" ")

  def ===(tOther: Term): EqChainForm = {
    return EqChainForm(term, terms :+ (EQ -> tOther))
  }

  def ===(otherChain: EqChainForm): EqChainForm = {
    val EqChainForm(tOther, termsOther) = otherChain
    return EqChainForm(term, (terms :+ (EQ -> tOther)) ++ termsOther)
  }
}

case class PredicateForm(predicate: Predicate, terms: List[Term]) extends Formula {
  override def prettyprint = predicate.prettyprint + "(" + terms.map(_.prettyprint).mkString(",") + ")"
}