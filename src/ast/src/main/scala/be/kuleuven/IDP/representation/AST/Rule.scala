package be.kuleuven.IDP.representation.AST

// Definition [Rule] |
case class Definition(rules: List[Rule]) extends TheoryStatement {
  override def prettyprint = "definition {\n" + rules.map("\t" + _.prettyprint + ".").mkString("\n") + "\n}"
}

//data Rule = Rule [Quantification] RuleHead (Maybe Formula)
case class Rule(
  quantifications: List[Quantification],
  head: RuleHead,
  body: Option[Formula]) extends IDP { // deriving (Eq,Ord,Show,Typeable,Data)

  override def prettyprint = {
    val bodyPrint = body match {
      case Some(f) => "<-" + f.prettyprint + "."
      case None => "."
    }
    quantifications.map(_.prettyprint).mkString(" ") + head.prettyprint + bodyPrint
  }
}

//data RuleHead =
abstract class RuleHead extends IDP // deriving (Eq,Ord,Show,Typeable,Data)
// AtomHead Atom
case class FormulaHead(a: Formula) extends RuleHead{
  override def prettyprint = a.prettyprint
}
// FuncHead Atom Term
//case class FuncHead(a: Atom, t: Term) extends RuleHead