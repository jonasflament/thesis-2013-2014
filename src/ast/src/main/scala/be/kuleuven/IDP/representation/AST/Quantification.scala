package be.kuleuven.IDP.representation.AST

//data Quantification = Quantification QuantType (Maybe CompType) (Maybe Integer) [Variable]
case class Quantification(
  t: QuantType,
  cType: Option[CompType],
  i: Option[Integer],
  v: List[Variable]) extends IDP { // deriving (Eq,Ord,Show,Typeable,Data)

  override def prettyprint = {
    val tS = t.prettyprint
    val cTypeS = cType match {
      case Some(c) => c.prettyprint
      case None => ""
    }
    val iS = i match{
      case Some(i) => i + ""
      case None => ""
    }
    val vS = v.map(_.prettyprint).mkString(" ")
    
    tS + cTypeS + iS + " :" + vS
  }
}
//data QuantType =
abstract class QuantType extends IDP // deriving (Eq,Ord,Show,Typeable,Data)
// Exists
case object Exists extends QuantType{
  override def prettyprint = "?"
}
//| ForAll
case object ForAll extends QuantType {override def prettyprint = "!"}

//data CompType =
abstract class CompType extends IDP // deriving (Eq,Ord,Show,Typeable,Data)
// NEQ
case object NEQ extends CompType{override def prettyprint = "~="}
// | EQ
case object EQ extends CompType{override def prettyprint = "="}
// | GEQ
case object GEQ extends CompType {override def prettyprint = ">="}
// | LEQ
case object LEQ extends CompType {override def prettyprint = "=<"}
// | GT
case object GT extends CompType {override def prettyprint = ">"}
// | LT
case object LT extends CompType {override def prettyprint = "<"}