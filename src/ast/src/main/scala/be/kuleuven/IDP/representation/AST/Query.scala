package be.kuleuven.IDP.representation.AST

//data Query = Query [Variable] Formula
case class Query(vars: List[Variable], f: Formula) extends IDP { // deriving (Eq,Ord,Show,Typeable,Data)
  def prettyprint : String = {
    "(" +
      vars.map(_.prettyprint).mkString(",") +
      " : " +
      f.prettyprint +
      ")"
  }
}