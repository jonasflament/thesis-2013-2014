import sbt._
import Keys._

object BuildSettings {
  val buildSettings = Defaults.defaultSettings ++ Seq(
    organization := "be.kuleuven",
    version := "0.0.1",
    scalaVersion := "2.10.2",
    scalacOptions ++= Seq("-feature", "-deprecation"),
    libraryDependencies += "org.scalatest" % "scalatest_2.10" % "2.0" % "test",
    libraryDependencies += "org.scala-lang" % "scala-swing" % "2.10.2"
  )
}

object MyBuild extends Build {
  import BuildSettings._

  lazy val root: Project = Project(
    "root",
    file("."),
    settings = buildSettings ++ Seq(
      run <<= run in Compile in core,
      testOptions in Test := Nil,
      commands ++= experimentCmds
    )
  ) aggregate(ast, core)

  lazy val ast: Project = Project(
    "ast",
    file("ast"),
    settings = buildSettings ++ Seq(testOptions in Test := Nil)
  )

  lazy val limbo: Project = Project(
    "limbo", 
    file("limbo"),
    settings = buildSettings ++ Seq(testOptions in Test := Nil)
  )

  lazy val core: Project = Project(
    "core",
    file("core"),
    settings = buildSettings
  ) dependsOn(ast)

  lazy val exp : Project = Project(
    "experiments",
    file("experiments"),
    settings = buildSettings
  ) dependsOn(core)

  val experimentCmds = List(
	("multiple-syntax", "multiplesyntax.Main"),
	("structure-generation", "structuregeneration.Main"),
	("packing", "packing.PackingExperiment"),
	("sudoku", "sudoku.SudokuExperiment"),
	("sudoku-improved", "sudoku.SudokuExperimentImproved"),
	("user-defined-theory", "userdefinedtheory.Main")
  ).map{ case (a, b) => toCommand(a, b)}

  def toCommand(name: String, mainClass : String) = {
	Command.command(name) {(state: State) => state.copy(remainingCommands = Seq(s"experiments/runMain be.kuleuven.IDP.experiment.$mainClass") ++ state.remainingCommands)}
  }

  

  val c = Command.command("hello") {(state: State) => state.copy(remainingCommands = Seq("experiments/runMain be.kuleuven.IDP.experiment.multiplesyntax.Main"))}


  val multiplesyntax = TaskKey[Unit]("multiplesyntax", "Runs the 'multiplesyntax' experiment") := {println("hi")} //:= runMain.toTask("be.kuleuven.IDP.experiment.multiplesyntax.Main").value

  //addCommandAlias("abcdefg", "experiments/runMain be.kuleuven.IDP.experiment.multiplesyntax.Main")
  
  val structuregeneration = TaskKey[Unit]("structure-generation", "Runs the 'structure generation' experiment")

  val packing = TaskKey[Unit]("packing", "Runs the 'packing' experiment")
  
  val sudoku = TaskKey[Unit]("sudoku", "Runs the 'Sudoku' experiment")
  val sudokuimproved = TaskKey[Unit]("sudoku-improved", "Runs the 'sudoku improved' experiment")

  val userdefinedtheory = TaskKey[Unit]("user-defined-theory", "Runs the 'user-defined theory' experiment")

}
