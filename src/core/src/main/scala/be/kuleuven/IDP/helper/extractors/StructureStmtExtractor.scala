package be.kuleuven.IDP.helper.extractors

import be.kuleuven.IDP.representation.AST.InterDecl
import be.kuleuven.IDP.representation.AST.PFDecl
import be.kuleuven.IDP.representation.AST.Structure
import be.kuleuven.IDP.representation.AST.StructureStatement

/**
 * Collects all structure statements for which the given function holds
 */
class StructureStmtExtractor(s: Structure, f: PFDecl => Boolean) extends Extractor[StructureStatement](
  x => x match {
    case s: InterDecl if f(s.threeV.v) => true
    case _ => false
  }
) {
  apply(s)
}