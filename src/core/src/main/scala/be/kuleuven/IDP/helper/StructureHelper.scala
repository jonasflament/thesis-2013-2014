package be.kuleuven.IDP.helper

import be.kuleuven.IDP.IDPConnect
import be.kuleuven.IDP.representation.AST.ElemList
import be.kuleuven.IDP.representation.AST.InterDecl
import be.kuleuven.IDP.representation.AST.InterElem
import be.kuleuven.IDP.representation.AST.Structure
import be.kuleuven.IDP.representation.AST.StructureStatement

/**
 * Facilitates modifying structures, to make something true, false, or unknown in them
 */
class StructureHelper {

  /**
   * StructureStatement: the needle of the operation
   * Option[StructureStatement]: the haystack of the operation
   */
  type Op = (StructureHelper, StructureStatement, Option[StructureStatement]) => List[StructureStatement]

  def merge(needle: StructureStatement, haystack: StructureStatement, f: (List[InterElem], List[InterElem]) => List[InterElem]): InterDecl = {
    // extract the elems
    val a = needle.asInstanceOf[InterDecl].structI.asInstanceOf[ElemList].breakdown
    val b = haystack.asInstanceOf[InterDecl].structI.asInstanceOf[ElemList].breakdown

    val needleElems = a.elems
    val haystackElems = b.elems

    // return a new structureStatement, based on the haystack, with the correct elements
    val h @ InterDecl(threeV, _) = haystack
    h copy (structI = ElemList(f(needleElems, haystackElems)))
  }

  def addXToY(from: StructureStatement, to: StructureStatement) = merge(from, to, (a, b) => a ++ b)
  def removeXFromY(toRemove: StructureStatement, from: StructureStatement) = merge(toRemove, from, { (needle, haystack) =>
    haystack diff needle
  })

  def makeTrue(struct: Structure, needle: StructureStatement): Structure = {
    // add to interpretation if needle.isOther
    val fOther: Op = { (h, n, os) => List(h.addXToY(n, os.get)) }

    // add it to ct, if found
    // or add a new CT interpretation if none exists
    val fCT: Op = (h, n, os) => if (os.isDefined) List(h.addXToY(n, os.get)) else List(n.asCT)

    // remove it from cf, if found
    val fCF: Op = (h, n, os) => if (os.isDefined) List(h.removeXFromY(n, os.get)) else Nil

    makeXXX(needle, struct, fOther, fCT, fCF)
  }

  def makeFalse(struct: Structure, needle: StructureStatement): Structure = {
    // remove from interpretation if needle.isOther
    val fOther: Op = { (h, n, os) => List(h.removeXFromY(n, os.get)) }

    // remove from interpretation if needle.isCt
    val fCt: Op = { (h, n, os) => if (os.isDefined) List(h.removeXFromY(n, os.get)) else Nil }

    // add from interpretation if needle.isCF
    // or add a new CF interpretation if none exists
    val fCf: Op = { (h, n, os) => if (os.isDefined) List(h.addXToY(n, os.get)) else List(n.asCF) }

    makeXXX(needle, struct, fOther, fCt, fCf)
  }

  def makeUnknown(struct: Structure, needle: StructureStatement): Structure = {
    // if a statement must be made unknown  when it is defined by an "isOther", then
    // the original statement must be split into a CT, and CF
    val fOther: Op = { (h, n, os) =>
      val original = os.get

      // with CT as the original statement, without the needle
      val newCT = h.removeXFromY(n, original).asCT

      // and with CF as the complement of the new CT
      val newCF = IDPConnect.interpretationToComplement(struct, newCT).asCF
      List(newCT, newCF)
    }

    // remove it from ct, if found
    val fCt: Op = { (h, n, os) => if (os.isDefined) List(h.removeXFromY(n, os.get)) else Nil }

    // remove it from cf, if found
    val fCf: Op = { (h, n, os) => if (os.isDefined) List(h.removeXFromY(n, os.get)) else Nil }

    makeXXX(needle, struct, fOther, fCt, fCf)
  }

  /**
   * Helper method which contains the commonality to create new structure for makeTrue, makeFalse, and makeUnknown,
   * based on the variability specified by functions given as parameter
   */
  def makeXXX(needle: StructureStatement, struct: Structure, fOther: Op,
    fCT: Op,
    fCF: Op): Structure = {

    def yyy(_pfStmts: List[StructureStatement]): List[StructureStatement] = {
      // if the there is only one statement, which is isOther
      if (_pfStmts.size == 1 && _pfStmts.head.isOther) {
        // apply fOther onto it
        fOther(this, needle, Some(_pfStmts.head))
      } else {
        // otherwise, it can only be one CT, one CF, or one CF and one CF
        // here, only CF or CT can be found
        val ctO = _pfStmts.find(_.isCT)
        val cfO = _pfStmts.find(_.isCF)

        // apply 
        fCT(this, needle, ctO) ++ fCF(this, needle, cfO)
      }
    }

    // collect all structurestatements
    val Structure(_, _, allStmts: List[StructureStatement]) = struct

    val (pfStmts, otherStmts) = allStmts.partition(_.pf == needle.pf)
    val ss = otherStmts ++ yyy(pfStmts)

    struct copy (stmts = ss)
  }
}