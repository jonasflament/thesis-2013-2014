package be.kuleuven.IDP.helper

import be.kuleuven.IDP.representation.AST.PFDecl

/**
 * Represents an IDPTable with cts, to define which are certainly true, and cfs which are certainly false
 */
case class IDPTable[T](pf: PFDecl, val cts: List[T], val cfs: List[T]){
}

