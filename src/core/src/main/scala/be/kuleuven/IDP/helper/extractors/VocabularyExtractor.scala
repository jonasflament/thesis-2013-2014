package be.kuleuven.IDP.helper.extractors

import be.kuleuven.IDP.representation.AST.IDP
import be.kuleuven.IDP.representation.AST.VocabularyStatement

/**
 * Collects all vocabulary statements
 */
class VocabularyExtractor(idp: IDP) extends Extractor[VocabularyStatement](_.isInstanceOf[VocabularyStatement]){
  apply(idp)
}