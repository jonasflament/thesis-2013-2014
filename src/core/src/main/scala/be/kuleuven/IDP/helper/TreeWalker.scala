package be.kuleuven.IDP.helper

import be.kuleuven.IDP.representation.AST.AggTerm
import be.kuleuven.IDP.representation.AST.CharE
import be.kuleuven.IDP.representation.AST.CharRange
import be.kuleuven.IDP.representation.AST.ConnectedF
import be.kuleuven.IDP.representation.AST.ConnectedT
import be.kuleuven.IDP.representation.AST.Definition
import be.kuleuven.IDP.representation.AST.EProcedure
import be.kuleuven.IDP.representation.AST.ElemList
import be.kuleuven.IDP.representation.AST.EqChainForm
import be.kuleuven.IDP.representation.AST.FormulaHead
import be.kuleuven.IDP.representation.AST.Function
import be.kuleuven.IDP.representation.AST.FunctionTerm
import be.kuleuven.IDP.representation.AST.IDP
import be.kuleuven.IDP.representation.AST.IntE
import be.kuleuven.IDP.representation.AST.IntRange
import be.kuleuven.IDP.representation.AST.InterDecl
import be.kuleuven.IDP.representation.AST.InterList
import be.kuleuven.IDP.representation.AST.Not
import be.kuleuven.IDP.representation.AST.Predicate
import be.kuleuven.IDP.representation.AST.PredicateForm
import be.kuleuven.IDP.representation.AST.Quantification
import be.kuleuven.IDP.representation.AST.Quantified
import be.kuleuven.IDP.representation.AST.Rule
import be.kuleuven.IDP.representation.AST.StringE
import be.kuleuven.IDP.representation.AST.Structure
import be.kuleuven.IDP.representation.AST.Theory
import be.kuleuven.IDP.representation.AST.ThreeValued
import be.kuleuven.IDP.representation.AST.Variable

/**
 * Class which walks the AST.
 */
abstract class TreeWalker {

  def walk(block: IDP): Unit = {

    def walkL(bs: Traversable[IDP]) {
      bs.foreach(walk(_))
    }

    block match {
      case Theory(_, _, theoStmts) => walkL(theoStmts)
      // formula's
      case Not(formula) => walk(formula)
      case ConnectedF(fOp, formula1, formula2) =>
        walk(fOp)
        walk(formula1)
        walk(formula2)
      case EqChainForm(term, otherTerms) =>
        walk(term)
        otherTerms.foreach { x => walk(x._1); walk(x._2) }

      // predicate
      case Predicate(name, argTypes) =>
        walkL(argTypes)
      case PredicateForm(predicate, terms) =>
        walk(predicate)
        walkL(terms)

      // definition
      case Definition(rules) =>
        walkL(rules)
      case Rule(qs, h, b) =>
        walkL(qs)
        walk(h)
        if (b.isDefined)
          walk(b.get)
      case FormulaHead(f) => walk(f)

      case Quantified(quantification, formula) =>
        walk(quantification)
        walk(formula)
      case Quantification(quantType, cType, i, vars) =>
        walkL(vars)

      // function
      case Function(name, argTypes, retType) =>
        walkL(argTypes)
        walk(retType)

      // Term
      case FunctionTerm(function, args) =>
        walk(function)
        walkL(args)
      case ConnectedT(tOp, t1, t2) =>
        walk(tOp)
        walk(t1)
        walk(t2)
      case AggTerm(agg) =>
        walk(agg)
      case Variable(_, tRef) =>
        walk(tRef)

      // structure
      case Structure(_, _, stmts) => walkL(stmts)
      // structure statements
      case InterDecl(threeV, structI) =>
        walk(threeV)
        walk(structI)
      case ThreeValued(v, s) => // do nothing
      // structure interpretation
      case ElemList(elems) => walkL(elems)
      case EProcedure(_) => // do nothing

      // InterElem
      case IntE(_) => // do nothing
      case IntRange(_, _) => // do nothing
      case CharE(_) => // do nothing
      case CharRange(_, _) => // do nothing
      case StringE(_) => // do nothing
      case InterList(elems) => walkL(elems)

      case _ => // Do nothing
    }
  }
}