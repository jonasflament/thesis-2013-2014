package be.kuleuven.IDP.helper.extractors

import scala.collection.mutable.ArrayBuffer

import be.kuleuven.IDP.helper.TreeWalker
import be.kuleuven.IDP.representation.AST.IDP

/**
 * Extract all elements from the AST, for which the given predicate holds, when the tree is walked
 */
class Extractor[T <: IDP](p: IDP => Boolean) extends TreeWalker with Traversable[T] {
  val acc = new ArrayBuffer[T]

  override def walk(block: IDP) {
    if (p(block)) {
      acc += block.asInstanceOf[T]
    }
    super.walk(block)
  }
  
  /**
   * Walk the AST tree, to begin extracting elements
   */
  def apply(i: IDP) : Extractor[T] = {
	  walk(i)
	  this
  }

  override def foreach[U](f: T => U) = acc.foreach(f)
}