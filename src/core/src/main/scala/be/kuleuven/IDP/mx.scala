package be.kuleuven.IDP

import be.kuleuven.IDP.helper.CharTableGenerator
import be.kuleuven.IDP.helper.IDPTable
import be.kuleuven.IDP.helper.IDPTypeWrapper
import be.kuleuven.IDP.helper.IntTableGenerator
import be.kuleuven.IDP.helper.StringTableGenerator
import be.kuleuven.IDP.helper.StructureHelper
import be.kuleuven.IDP.helper.TupleTableGenerator
import be.kuleuven.IDP.helper.extractors.VocabularyExtractor
import be.kuleuven.IDP.representation.AST.Function
import be.kuleuven.IDP.representation.AST.PFDecl
import be.kuleuven.IDP.representation.AST.Predicate
import be.kuleuven.IDP.representation.AST.Structure
import be.kuleuven.IDP.representation.AST.StructureStatement
import be.kuleuven.IDP.representation.AST.Theory
import be.kuleuven.IDP.representation.AST.TypeDecl
import be.kuleuven.IDP.representation.AST.Vocabulary

object mx {
  def onemodel(theo: Theory, struct: Structure) = {
    IDPConnect.onemodel(theo, struct, extractVoc(theo, struct))
  }

  /**
   * Returns the predicate representing the given type
   */
  def predOf(t: TypeDecl): Predicate = {
    IDPConnect.predOf(t)
  }

  /**
   * Contains all available stdoptions of IDP
   */
  object stdoptions {
    var nbmodels: Int = -1
    var cpsupport: Boolean = false
    var groundwithbounds: Boolean = true
    var symmetry: Int = 0
    var groundlazily = false
    var liftedunitpropagation = false
  }

  /**
   * Modelexpands the given theory based on the given structure, and returns a list of models
   */
  def modelexpand(theo: Theory, struct: Structure): List[Structure] = {
    IDPConnect.modelexpand(theo, struct, extractVoc(theo, struct))
  }

  /**
   * Extracts the vocabulary of at theory and a structure
   */
  private def extractVoc(theo: Theory, struct: Structure): Vocabulary = {
    val theoVocStmts = new VocabularyExtractor(theo)
    val structVocStmts = new VocabularyExtractor(struct)

    Vocabulary("Voc" + theo.name + struct.name, theoVocStmts.toList ++ structVocStmts)
  }

  /**
   * Minimizes the given theory, based on the given structure, using the given function
   */
  def minimize(t: Theory, s: Structure, f: Function): Structure = {
    IDPConnect.minimize(t, s, extractVoc(t, s), f)
  }

  /**
   * Makes the given structure statement true in the given structure.
   * 
   * @pre: the structure only contains, for the predicate or function of the needle, either
   * - one un-tagged statement,
   * - one CT and one CF statement,
   * - one CT statement, or
   * - one CF statement
   */
  def maketrue(struct: Structure, needle: StructureStatement): Structure = {
    new StructureHelper().makeTrue(struct, needle)
  }

  /**
   * Makes the given structure statement false in the given structure.
   * 
   * @pre: the structure only contains, for the predicate or function of the needle, either
   * - one un-tagged statement,
   * - one CT and one CF statement,
   * - one CT statement, or
   * - one CF statement
   */
  def makefalse(struct: Structure, needle: StructureStatement): Structure = {
    new StructureHelper().makeFalse(struct, needle)
  }

  /**
   * Makes the given structure statement unknown in the given structure.
   * 
   * @pre: the structure only contains, for the predicate or function of the needle, either
   * - one un-tagged statement,
   * - one CT and one CF statement,
   * - one CT statement, or
   * - one CF statement
   */
  def makeunknown(struct: Structure, needle: StructureStatement): Structure = {
    new StructureHelper().makeUnknown(struct, needle)
  }

  def IDPDraw(config: String) {
    IDPConnect.IDPDraw(config: String)
  }

  ////////////////////////////////////////////////////////////////////////////
  // Methods to create tables for type, predicate, and function declaration //
  ////////////////////////////////////////////////////////////////////////////

  def asIntTable(s: Structure, t: TypeDecl): IDPTable[Int] = asIntTable(s, predOf(t))
  def asIntTable(s: Structure, p: Predicate): IDPTable[Int] = new IntTableGenerator().toTable(s, p)

  def asCharTable(s: Structure, t: TypeDecl): IDPTable[Char] = asCharTable(s, predOf(t))
  def asCharTable(s: Structure, p: Predicate): IDPTable[Char] = new CharTableGenerator().toTable(s, p)

  def asStringTable(s: Structure, t: TypeDecl): IDPTable[String] = asStringTable(s, predOf(t))
  def asStringTable(s: Structure, p: Predicate): IDPTable[String] = new StringTableGenerator().toTable(s, p)

  def asTupleTable(s: Structure, pfDecl: PFDecl): IDPTable[List[IDPTypeWrapper]] = new TupleTableGenerator().toTable(s, pfDecl)

}