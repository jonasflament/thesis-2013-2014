package be.kuleuven.IDP

import scala.collection.mutable.Queue

import be.kuleuven.IDP.representation.AST.ElemList
import be.kuleuven.IDP.representation.AST.Function
import be.kuleuven.IDP.representation.AST.InterDecl
import be.kuleuven.IDP.representation.AST.Predicate
import be.kuleuven.IDP.representation.AST.Structure
import be.kuleuven.IDP.representation.AST.StructureStatement
import be.kuleuven.IDP.representation.AST.Theory
import be.kuleuven.IDP.representation.AST.TypeDecl
import be.kuleuven.IDP.representation.AST.Vocabulary

/**
 * Represents the connectivity layer of the DSL
 */
object IDPConnect extends IDPMock

/**
 * Defines the interface of the connectivity layer
 */
trait IDPConnectTrait {
  def interpretationToComplement(struct: Structure, s: StructureStatement): StructureStatement

  def onemodel(theo: Theory, s: Structure, voc: Vocabulary): Structure

  def modelexpand(theo: Theory, struct: Structure, voc: Vocabulary): List[Structure]

  def minimize(t: Theory, s: Structure, voc: Vocabulary, f: Function): Structure

  def IDPDraw(config: String)

  def predOf(t: TypeDecl): Predicate
}

class MockQueueEmptyException(s: String) extends RuntimeException(s)

/**
 * Mock object, backed up by queues, which simulates IDP behavior by returning results from the correct queue.
 */
trait IDPMock extends IDPConnectTrait {

  val oneModelQueue = new Queue[Structure]
  val modelExpandQueue = new Queue[List[Structure]]
  val minimizeQueue = new Queue[Structure]

  def reset = {
    oneModelQueue.clear
    modelExpandQueue.clear
    minimizeQueue.clear
  }

  /**
   * Simulates the complement as an empty list, as IDP is needed to compute the real complement, based on the given structure.
   */
  override def interpretationToComplement(struct: Structure, s: StructureStatement): StructureStatement = {
    val originalStruct = s.asInstanceOf[InterDecl]
    val originalStructI = originalStruct.structI.asInstanceOf[ElemList]
    originalStruct copy (structI = ElemList(Nil))
  }

  override def onemodel(theo: Theory, s: Structure, voc: Vocabulary): Structure = {
    if (oneModelQueue.isEmpty)
      throw new MockQueueEmptyException("The oneModelQueue is empty")
    oneModelQueue.dequeue
  }

  def modelexpand(theo: Theory, struct: Structure, voc: Vocabulary): List[Structure] = {
    if (modelExpandQueue.isEmpty)
      throw new MockQueueEmptyException("The modelExpandQueue is empty")
    modelExpandQueue.dequeue
  }

  override def minimize(t: Theory, s: Structure, voc: Vocabulary, f: Function): Structure = {
    if (minimizeQueue.isEmpty)
      throw new MockQueueEmptyException("The minimizeQueue is empty")
    minimizeQueue.dequeue
  }

  /**
   * Determines whether the IDPDraw config, generated in the experiments, must be shown
   * @default false
   */
  var shouldOutput = true
  override def IDPDraw(config: String) = {
    // TODO: call IDPDraw
    //    val r = Random
    //    //    math.randomseed(os.time())
    //    //    local outname = "/tmp/.idpd"..tostring(math.random(1111,9999))
    //    val outfile = File.createTempFile("Sudoku-", ".idp")
    if (shouldOutput)
      println(config)
  }

  override def predOf(t: TypeDecl) = {
    val c = new IDPContext()
    val n = t.name.toString
    c.Pred(t.name.toString, t)
  }
}