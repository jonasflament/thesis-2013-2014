package be.kuleuven.IDP

import scala.language.implicitConversions

import be.kuleuven.IDP.helper.FunctionStructurer
import be.kuleuven.IDP.helper.PredicateStructurer
import be.kuleuven.IDP.helper.UniversalQuantFormulaHelper
import be.kuleuven.IDP.helper.UniversalQuantRuleHelper
import be.kuleuven.IDP.representation.AST.AMax
import be.kuleuven.IDP.representation.AST.AMin
import be.kuleuven.IDP.representation.AST.AProd
import be.kuleuven.IDP.representation.AST.ASTTypes.Name
import be.kuleuven.IDP.representation.AST.ASum
import be.kuleuven.IDP.representation.AST.Aggregate
import be.kuleuven.IDP.representation.AST.Card
import be.kuleuven.IDP.representation.AST.CharLiteralTerm
import be.kuleuven.IDP.representation.AST.CompType
import be.kuleuven.IDP.representation.AST.Definition
import be.kuleuven.IDP.representation.AST.EQ
import be.kuleuven.IDP.representation.AST.Exists
import be.kuleuven.IDP.representation.AST.ForAll
import be.kuleuven.IDP.representation.AST.Formula
import be.kuleuven.IDP.representation.AST.FormulaHead
import be.kuleuven.IDP.representation.AST.Function
import be.kuleuven.IDP.representation.AST.GEQ
import be.kuleuven.IDP.representation.AST.GT
import be.kuleuven.IDP.representation.AST.IntegerTerm
import be.kuleuven.IDP.representation.AST.LEQ
import be.kuleuven.IDP.representation.AST.LT
import be.kuleuven.IDP.representation.AST.OtherAgg
import be.kuleuven.IDP.representation.AST.Predicate
import be.kuleuven.IDP.representation.AST.QuantType
import be.kuleuven.IDP.representation.AST.Quantification
import be.kuleuven.IDP.representation.AST.Quantified
import be.kuleuven.IDP.representation.AST.Query
import be.kuleuven.IDP.representation.AST.QueryBlock
import be.kuleuven.IDP.representation.AST.Rule
import be.kuleuven.IDP.representation.AST.StringLiteralTerm
import be.kuleuven.IDP.representation.AST.Structure
import be.kuleuven.IDP.representation.AST.StructureStatement
import be.kuleuven.IDP.representation.AST.TSet
import be.kuleuven.IDP.representation.AST.Term
import be.kuleuven.IDP.representation.AST.TermBlock
import be.kuleuven.IDP.representation.AST.Theory
import be.kuleuven.IDP.representation.AST.TheoryStatement
import be.kuleuven.IDP.representation.AST.TrueForm
import be.kuleuven.IDP.representation.AST.TypeDecl
import be.kuleuven.IDP.representation.AST.Variable
import be.kuleuven.IDP.representation.AST.VariableSym

object IDPContext {
  /**
   * Index of the last unnamed type that was created
   *
   * If none were created, the counter == -1
   */
  var typeCounter = -1

  /**
   * Index of the last unnamed predicate that was created
   *
   * If none were created, the counter == -1
   */
  var predCounter = -1

  /**
   * Index of the last unnamed function that was created
   *
   * If none were created, the counter == -1
   */
  var funcCounter = -1

  /**
   * Index of the last unnamed theory that was created
   *
   * If none were created, the counter == -1
   */
  var theoryCounter = -1

  /**
   * Index of the last unnamed structure that was created
   *
   * If none were created, the counter == -1
   */
  var structureCounter = -1

  /**
   * Index of the last unnamed structure that was created
   *
   * If none were created, the counter == -1
   */
  var termCounter = -1

  /**
   * Index of the last unnamed structure that was created
   *
   * If none were created, the counter == -1
   */
  var queryCounter = -1
}

/**
 * Contains the syntax for the DSL
 */
class IDPContext {
  import scala.language.implicitConversions

  /**
   * The prefixes used for objects which names are generated
   */
  object UPfix {
    val Type = "UnnamedType"
    val Pred = "UnnamedPred"
    val Func = "UnnamedFunc"
    val Theo = "UnnamedTheo"
    val Struct = "UnnamedStruct"
    val Term = "UnnamedTerm"
    val Query = "UnnamedQuery"
  }

  /**
   * Creates a new type with the given super type
   *
   * A unique name is automatically generated
   */
  def Type(superType: TypeDecl): TypeDecl = {
    IDPContext.typeCounter += 1
    return Type(s"${UPfix.Type}${IDPContext.typeCounter}", superType)
  }
  
  /**
   * Creates a new type wich contains the given type
   *
   * A unique name is automatically generated
   */
  def TypeContaining(containingType: TypeDecl): TypeDecl = {
    IDPContext.typeCounter += 1
    return Type(s"${UPfix.Type}${IDPContext.typeCounter}", containingType)
  }

  /**
   * Creates a new type with the given name and super type
   */
  def Type(name: String, superType: TypeDecl): TypeDecl = {
    return TypeDecl(name, None, Some(superType), None)
  }
  
  /**
   * Creates a new type with the given name and which contains the given type
   */
  def TypeContaining(name: String, containingType: TypeDecl): TypeDecl = {
    return TypeDecl(name, None, None, Some(containingType))
  }

  /**
   * Creates a new predicate with given input types
   *
   * A unique name is automatically generated
   */
  def Pred(inputTypes: TypeDecl*): Predicate = {
    IDPContext.predCounter += 1
    return Pred(s"${UPfix.Pred}${IDPContext.predCounter}", inputTypes: _*)
  }

  /**
   * Creates a new predicate with given name and input types
   */
  def Pred(name: String, inputTypes: TypeDecl*): Predicate = {
    return Predicate(name, inputTypes.toList)
  }

  /**
   * Creates a new functions which, given the input types, returns something of the given return type
   *
   * A unique name is automatically generated
   */
  def Func(argTypes: TypeDecl*)(retType: TypeDecl): Function = {
    IDPContext.funcCounter += 1
    return Func(s"${UPfix.Func}${IDPContext.funcCounter}", argTypes: _*)(retType)
  }

  /**
   * Creates a new functions which, given the input types, returns something of the given return type
   *
   * A unique name is automatically generated
   */
  def Func(name: String, argTypes: TypeDecl*)(retType: TypeDecl): Function = {
    return Function(name, argTypes.toList, retType)
  }
  
  /**
   * Transforms a sequence of Symbol to TypeDecl mappings into IDP Variables
   */
  private def mkVariables(a: Seq[(Symbol, TypeDecl)]): List[Variable] = {
    a.toList.map {
      case (s: Symbol, t: TypeDecl) =>
        Variable(s.name, t)
    }
  }

  /**
   * Creates quantifications for a rule
   */
  private def mkRuleQuant(t: QuantType, a: List[(Symbol, TypeDecl)], r: Rule): List[Quantification] = {
    return List(
      Quantification(
        ForAll,
        None,
        None,
        mkVariables(a.toList)
      )) ++ r.quantifications
  }

  /**
   * Helper method to create a ForAll or Exists quantification
   */
  private def simple_quant(t: QuantType, a: List[(Symbol, TypeDecl)], f: Formula): Formula = {
    return Quantified(
      Quantification(
        t,
        None,
        None,
        mkVariables(a.toList)
      ), f)
  }

  /**
   * Helper method to create quantifications for ?=, ?>, ?<, etc.
   */
  private def complex_quant(t: QuantType, cType: Option[CompType], n: Option[Integer], a: Seq[(Symbol, TypeDecl)], f: Formula): Formula = {
    return Quantified(
      Quantification(
        Exists,
        cType,
        n,
        mkVariables(a.toList)
      ), f)
  }

  /**
   * Creates a existential quantification
   */
  def ?(a: (Symbol, TypeDecl)*)(f: Formula): Formula = {
    return simple_quant(Exists, a.toList, f)
  }

  /**
   * Creates an existential quantification, where the number of existences is equal to n
   */
  def ?=(n: Int, a: (Symbol, TypeDecl)*)(f: Formula): Formula = {
    return complex_quant(Exists, Some(EQ), Some(n), a, f)
  }

  /**
   * Creates an existential quantification, where the number of existences is greater than n
   */
  def ?>(n: Int, a: (Symbol, TypeDecl)*)(f: Formula): Formula = {
    return complex_quant(Exists, Some(GT), Some(n), a, f)
  }

  /**
   * Creates an existential quantification, where the number of existences is less than n 
   */
  def ?<(n: Int, a: (Symbol, TypeDecl)*)(f: Formula): Formula = {
    return complex_quant(Exists, Some(LT), Some(n), a, f)
  }

  /**
   * Creates an existential quantification, where the number of existences is greater than or equals n 
   */
  def ?>=(n: Int, a: (Symbol, TypeDecl)*)(f: Formula): Formula = {
    return complex_quant(Exists, Some(GEQ), Some(n), a, f)
  }

  /**
   * Creates an existential quantification, where the number of existences is less than or equals 
   */
  def ?=<(n: Int, a: (Symbol, TypeDecl)*)(f: Formula): Formula = {
    return complex_quant(Exists, Some(LEQ), Some(n), a, f)
  }

  /**
   * Creates a definition given multiple rules
   */
  def definition(rules: Rule*): Definition = {
    Definition(rules.toList)
  }

  /**
   * Creates a theory given multiple theory statements, such as definitions and formula's
   */
  def theory(stmts: TheoryStatement*): Theory = {
    IDPContext.theoryCounter += 1
    Theory(UPfix.Theo + IDPContext.theoryCounter, "", stmts.toList)
  }

  /**
   * Creates a term block for the given term
   */
  def term(t: Term): TermBlock = {
    IDPContext.termCounter += 1
    TermBlock(UPfix.Term + IDPContext.termCounter, "", t)
  }
  
  def query(t: (Symbol, TypeDecl)*)(f: Formula): QueryBlock = {
    IDPContext.queryCounter += 1
    QueryBlock(UPfix.Query + IDPContext.queryCounter, "", Query(mkVariables(t), f))
  }

  /**
   * Interpret the given predicate
   */
  def interpret(p: Predicate): PredicateStructurer = {
    new PredicateStructurer(p)
  }

  /**
   * Interpret the given function
   */
  def interpret(f: Function): FunctionStructurer = {
    new FunctionStructurer(f)
  }

  /**
   * Create a structure given multiple structure interpretations
   */
  def structure(stmts: StructureStatement*): Structure = {
    IDPContext.structureCounter += 1
    Structure(UPfix.Struct + IDPContext.structureCounter, Nil, stmts.toList)
  }

  ////////////////
  // Aggregates //
  ////////////////
  //
  // WARNING: these aggregates aggregate over the complete set of ('x -> T, ...) declarations
  //          the other type of aggregates, where only the first symbol is aggregated over, 

  /**
   * Creates a cardinality aggregate
   */
  def card(s: (Symbol, TypeDecl)*)(f: Formula): Aggregate = {
    Card(Query(
      mkVariables(s.toList),
      f
    ))
  }

  /**
   * Creates a "max" aggregate, which represents the maximum of all elements of the given type
   */
  def max(t: TypeDecl): OtherAgg = { max('a -> t)(TrueForm, 'a) }
  
  /**
   * Creates a "max" aggregate, which represents the maximum of all elements for which the given formula holds
   */
  def max(s: (Symbol, TypeDecl)*)(f: Formula, t: Term): OtherAgg = {
    OtherAgg(AMax,
      TSet(
        mkVariables(s.toList),
        f,
        t))
  }

  /**
   * Creates a "min" aggregate, which represents the maximum of all elements of the given type
   */
  def min(t: TypeDecl) = {
    max(t) copy (aggType = AMin)
  }

  /**
   * Creates a "min" aggregate, which represents the maximum of all elements for which the given formula holds
   */
  def min(s: (Symbol, TypeDecl)*)(f: Formula, t: Term) = {
    max(s: _*)(f, t).copy(aggType = AMin)
  }
  
  /**
   * Creates a "sum" aggregate, which sums up the terms for which the given formula holds 
   */
  def sum(s: (Symbol, TypeDecl)*)(f: Formula, t: Term) = {
    OtherAgg(ASum,
      TSet(
        mkVariables(s.toList),
        f,
        t
      ))
  }

  /**
   * Creates a "product" aggregate, which multiplies the terms for which the given formula holds 
   */
  def prod(s: (Symbol, TypeDecl)*)(f: Formula, t: Term) = {
    sum(s: _*)(f, t) copy (aggType = AProd)
  }

  
  /////////////////////////////////////////////////////
  /////////////////////////////////////////////////////
  /////////////////////////////////////////////////////
  /////////////////////////////////////////////////////

  /**
   * Implicitly converts a type to a structurer, so it can be interpreted by omitting the "interpret" keyword
   */
  implicit def typeToStructurer(t: TypeDecl): PredicateStructurer = interpret(mx.predOf(t))

  /**
   * Implicitly converts a predicate to a structurer, so it can be interpreted by omitting the "interpret" keyword
   */
  implicit def predicateToStructurer(p: Predicate): PredicateStructurer = interpret(p)

  /**
   * Implicitly converts a function to a structurer, so it can be interpreted by omitting the "interpret" keyword
   */
  implicit def functonToStructurer(f: Function): FunctionStructurer = interpret(f)

  /**
   * Implicitly converts a single string to List(String) (i.e. a Name)
   */
  implicit def stringToName(a: String): Name = List(a)

  /**
   * Implicitly converts symbol to VariablesSym so it can be used in predicates
   */
  implicit def symbolToVariableSym(s: Symbol): VariableSym = {
    return VariableSym(s.name)
  }

  /**
   * Implicitly converts string to StringLiteralTerm
   */
  implicit def stringToTerm(s: String): Term = StringLiteralTerm(s)

  /**
   * Implicitly converts char to CharLiteralTerm, which is needed since another method converting ints IntegerTerm also handles chars
   */
  implicit def charToTerm(c: Char): Term = CharLiteralTerm(c)

  /**
   * Implicitly converts integer to IntegerTerm
   */
  implicit def integerToTerm(i: Int): Term = IntegerTerm(i)

  /**
   * Implicitly converts a formula to a rule in order to be able to write "P <- true" as "P"
   */
  implicit def formulaToRule(f: Formula): Rule = {
    return Rule(Nil, FormulaHead(f), None)
  }

  implicit class ProductUnivQuant(p: Product) {

    private def mkQuantificationVariables(p: Product): Quantification = {
      def throwError() = {
        throw new IllegalArgumentException("Product contains more than just (Symbol, Typedecl)s")
      }

      /**
       * Creates vars for ('x -> T)
       */
      def uniProductToVars(a: Product) = {
        a match {
          case (s: Symbol, t: TypeDecl) => List(Variable(s.name, t))
          case _ => throwError
        }
      }

      /**
       * Creates vars for ('x -> T, ...)
       */
      def multiProductToVars(a: Product): List[Variable] = {
        a.productIterator.toList.map {
          _ match {
            case (s: Symbol, t: TypeDecl) =>
              Variable(s.name, t)
            case _ => throwError
          }
        }
      }

      // !('x -> T)(...)
      val vars = if (p.productArity == 2 && p.productIterator.toList(0).isInstanceOf[Symbol]) {
        uniProductToVars(p)
      } else {
        multiProductToVars(p)
      }

      Quantification(ForAll, None, None, vars)
    }

    def apply(f: Formula): UniversalQuantFormulaHelper = {
      UniversalQuantFormulaHelper(mkQuantificationVariables(p), f)
    }

    def apply(r: Rule): UniversalQuantRuleHelper = {
      UniversalQuantRuleHelper(mkQuantificationVariables(p), r)
    }
  }

}