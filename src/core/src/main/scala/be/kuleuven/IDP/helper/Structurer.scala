package be.kuleuven.IDP.helper

import scala.collection.immutable.NumericRange
import scala.collection.immutable.Range

import be.kuleuven.IDP.representation.AST.CF
import be.kuleuven.IDP.representation.AST.CT
import be.kuleuven.IDP.representation.AST.CharE
import be.kuleuven.IDP.representation.AST.CharRange
import be.kuleuven.IDP.representation.AST.ElemList
import be.kuleuven.IDP.representation.AST.Function
import be.kuleuven.IDP.representation.AST.IntE
import be.kuleuven.IDP.representation.AST.IntRange
import be.kuleuven.IDP.representation.AST.InterDecl
import be.kuleuven.IDP.representation.AST.InterElem
import be.kuleuven.IDP.representation.AST.InterList
import be.kuleuven.IDP.representation.AST.PFDecl
import be.kuleuven.IDP.representation.AST.Predicate
import be.kuleuven.IDP.representation.AST.StringE
import be.kuleuven.IDP.representation.AST.StructureStatement
import be.kuleuven.IDP.representation.AST.ThreeValued
import be.kuleuven.IDP.representation.AST.ThreeValuedIdentifier
import be.kuleuven.IDP.representation.AST.U

/**
 * Helper object to create structure interpretations
 */
abstract class Structurer(val pf: PFDecl) {

  var tag: Option[ThreeValuedIdentifier] = None

  /**
   * Tag the structure interpretation as "certainly true"
   */
  def ct(): Structurer = {
    tag = Some(CT)
    this
  }

  /**
   * Tag the structure interpretation as "certainly false"
   */
  def cf(): Structurer = {
    tag = Some(CF)
    this
  }

  /**
   * Tag the structure interpretation as "unknown"
   */
  def u(): Structurer = {
    tag = Some(U)
    this
  }

  /**
   * The elements in the structure
   */
  var elems: List[InterElem] = Nil

  /**
   * Returns a StructureStatement based on the interpretations created using the structurer
   */
  protected def collect: StructureStatement = {
    InterDecl(
      ThreeValued(pf, tag),
      ElemList(
        elems
      )
    )
  }

  /**
   * Convert an argument to a InterElem
   */
  protected def convertToInterElem(a: Any): InterElem = {
    a match {
      case a: Int => IntE(a)
      case a: Char => CharE(a)
      case a: String => StringE(a)

      case a: Range => IntRange(a.start, a.last)

      // Added guard due to type erasure
      case a: NumericRange[Char] if a.start.isInstanceOf[Char] => CharRange(a.start, a.last)

      case a: Product =>
        InterList(
          a.productIterator.toList.map(convertToInterElem(_))
        )

      case _ => throw new IllegalArgumentException(s"Unknown method for converting element to an InterElem, received $a")
    }
  }
}

class PredicateStructurer(val p: Predicate) extends Structurer(p) {

  /**
   * Define the predicate as a list of arguments
   *
   * If the arity is one, creates a List of InterElems
   *
   * Otherwise, if the arity is larger than one, creates a List of InterLists
   */
  def as(a: Any*): StructureStatement = {
    as(a.toList: List[Any])
  }

  def as(a: List[Any]): StructureStatement = {
    elems = a.toList map (convertToInterElem _)
    collect
  }

  override def ct(): PredicateStructurer = {
    super.ct
    this
  }

  override def cf(): PredicateStructurer = {
    super.cf
    this
  }

  override def u(): PredicateStructurer = {
    super.u
    this
  }
}

class FunctionStructurer(val f: Function) extends Structurer(f) {

  /**
   * Helper method which handles constant functions
   */
  private def asConstantFunction(a: Any): StructureStatement = {
    elems = List(convertToInterElem(a))
    collect
  }

  /**
   * Interprets a constant Integer function
   */
  def as(i: Int): StructureStatement = asConstantFunction(i)

  /**
   * Interprets a constant Char function
   */
  def as(c: Char): StructureStatement = asConstantFunction(c)

  /**
   * Interprets a constant String function
   */
  def as(s: String): StructureStatement = asConstantFunction(s)

  /**
   * Interprets a function with arity 1
   *
   * @note This method is needed as (a) -> b corresponds with (Any, Any) instead of (Product, Any)
   */
  def as(ps: (Any, Any)*): StructureStatement = {
    as(ps: Traversable[(Any, Any)])
  }

  def as(ps: Traversable[(Any, Any)]): StructureStatement = {
    for (p <- ps) {
      p match {
        case (argsP: Product, ret: Any) =>
          val args = argsP.productIterator.toList
          elems = elems :+ InterList((args :+ ret) map (convertToInterElem(_)))
        case (arg: Any, ret: Any) =>
          as(Tuple2(Tuple1(arg), ret))
      }
    }
    collect
  }

  override def ct(): FunctionStructurer = {
    super.ct
    this
  }

  override def cf(): FunctionStructurer = {
    super.cf
    this
  }

  override def u(): FunctionStructurer = {
    super.u
    this
  }
}
