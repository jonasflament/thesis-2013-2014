package be.kuleuven.IDP.helper

import be.kuleuven.IDP.representation.AST.Formula
import be.kuleuven.IDP.representation.AST.Quantification
import be.kuleuven.IDP.representation.AST.Quantified
import be.kuleuven.IDP.representation.AST.Rule

/**
 * Helps creating universal quantification for the given rule
 */
case class UniversalQuantRuleHelper(q: Quantification, r: Rule) {
  def unary_! : Rule = {
    val Rule(qs, head, body) = r
    Rule(q +: qs, head, body)
  }
}

/**
 * Helps creating universal quantification for the given formula
 */
case class UniversalQuantFormulaHelper(q: Quantification, f: Formula) {
  def unary_! : Formula = {
    Quantified(q, f)
  }
}