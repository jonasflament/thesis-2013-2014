package be.kuleuven.IDP.helper

/**
 * Wrapper around an IDP type, which is either an integer, char, string, or float, and contains methods to collect the value as a certain type without casts
 */
abstract class IDPTypeWrapper {
  def asInt: Int = throw new IllegalStateException(s"Wrapper ${this.toString()} cannot be casted to int")
  def asChar: Int = throw new IllegalStateException("Wrapper cannot be casted to char")
  def asString: String = throw new IllegalStateException("Wrapper cannot be casted to string")
  def asFloat: Float = throw new IllegalStateException("Wrapper cannot be casted to float")
}

case class IntEWrapper(i: Int) extends IDPTypeWrapper {
  override def asInt = i
}

case class CharEWrapper(c: Char) extends IDPTypeWrapper {
  override def asChar = c
}

case class StringEWrapper(s: String) extends IDPTypeWrapper {
  override def asString = s
}

case class FloatEWrapper(f: Float) extends IDPTypeWrapper{
  override def asFloat = f
}