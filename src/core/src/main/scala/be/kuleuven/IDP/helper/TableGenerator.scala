package be.kuleuven.IDP.helper

import be.kuleuven.IDP.IDPConnect
import be.kuleuven.IDP.helper.extractors.Extractor
import be.kuleuven.IDP.helper.extractors.StructureStmtExtractor
import be.kuleuven.IDP.representation.AST.CharE
import be.kuleuven.IDP.representation.AST.CharRange
import be.kuleuven.IDP.representation.AST.IntE
import be.kuleuven.IDP.representation.AST.IntRange
import be.kuleuven.IDP.representation.AST.InterElem
import be.kuleuven.IDP.representation.AST.InterList
import be.kuleuven.IDP.representation.AST.PFDecl
import be.kuleuven.IDP.representation.AST.StringE
import be.kuleuven.IDP.representation.AST.Structure
import be.kuleuven.IDP.representation.AST.StructureStatement

abstract class TableGenerator[T] {

  def stmtToList(s: StructureStatement): Traversable[T]

  def toTable(s: Structure, pf: PFDecl): IDPTable[T] = {
    def sort(xs: Traversable[StructureStatement]): Traversable[StructureStatement] = {
      val mapping: StructureStatement => Int = x => x match {
        case a if a.isCT => 0
        case b if b.isCF => 1
        case _ => 2
      }

      xs.toList.sortBy(mapping)
    }

    def createTable(stmts: Traversable[StructureStatement]): IDPTable[T] = {
      // for each statement, sorted by CT < CF < Other
      // they must be sorted as such, because
      // -CT interpretations add to the ct list of the table
      // -CF interpretations add to the cf list of the table
      // -other interpretations add to ct, and add the complement to cf
      val sorted = sort(stmts)

      var table = IDPTable[T](pf, Nil, Nil)

      def throwError = throw new NotImplementedError("Can (currently) only create table of structures with max one CT and one CF, or one other interpretation")
      if (sorted.size > 2) {
        throwError
      } else if (sorted.size == 2 && (sorted.count(x => x.isOther) == 2 ||
        sorted.count(x => x.isCF) == 2 ||
        sorted.count(x => x.isCT) == 2)) {
        throwError
      }

      for (i <- sorted) {
        if (i.isCT) {
          table = IDPTable(pf, stmtToList(i).toList, table.cfs)
        } else if (i.isCF) {
          table = IDPTable(pf, table.cts, stmtToList(i).toList)
        } else {
          table = IDPTable(pf,
            stmtToList(i).toList,
            stmtToList(IDPConnect.interpretationToComplement(s, i)).toList)
        }
      }

      table
    }

    val sb = s.breakdown
    val allPFStmts = new StructureStmtExtractor(sb, (x => x == pf))
    return createTable(allPFStmts)
  }
}

class IntTableGenerator extends TableGenerator[Int] {

  override def stmtToList(s: StructureStatement): List[Int] = {
    val elems = new Extractor[InterElem](x => x.isInstanceOf[IntE] || x.isInstanceOf[IntRange])(s.breakdown)

    elems.map(x => x match {
      case IntE(i) => List(i)
      case IntRange(i, j) => (i to j).toList
      case _ => throw new IllegalStateException("Found non-integer InterElem when converting a statement to a list of integers")
    }).toList.flatten
  }
}

class CharTableGenerator extends TableGenerator[Char] {
  override def stmtToList(s: StructureStatement): List[Char] = {
    val elems = new Extractor[InterElem](x => x.isInstanceOf[CharE] || x.isInstanceOf[CharRange])(s)

    elems.map(x => x match {
      case CharE(i) => List(i)
      case CharRange(i, j) => (i to j).toList
      case _ => throw new IllegalStateException("Found non-char InterElem when converting a statement to a list of chars")
    }).toList.flatten
  }
}

class StringTableGenerator extends TableGenerator[String] {
  override def stmtToList(s: StructureStatement): Traversable[String] = {
    val elems = new Extractor[StringE](x => x.isInstanceOf[StringE])(s.breakdown)
    elems.map(x => x.s)
  }
}

class TupleTableGenerator extends TableGenerator[List[IDPTypeWrapper]] {

  override def stmtToList(s: StructureStatement): Traversable[List[IDPTypeWrapper]] = {
    val elems = new Extractor[InterList](x => x.isInstanceOf[InterList])(s.breakdown)

    elems.map {
      (x =>
        x.elems.map {
          il =>
            il match {
              case IntE(i) => IntEWrapper(i)
              case CharE(c) => CharEWrapper(c)
              case StringE(s) => StringEWrapper(s)
              case _ => throw new IllegalStateException("Found non-IDPTypeWrappable element")
            }
        }.toList: List[IDPTypeWrapper])
    }
  }
}

