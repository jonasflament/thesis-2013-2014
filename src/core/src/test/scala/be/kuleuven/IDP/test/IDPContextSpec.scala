package be.kuleuven.IDP.test

import org.scalatest.Matchers
import org.scalatest.FreeSpec
import be.kuleuven.IDP.IDPContext
import be.kuleuven.IDP.Types._

class IDPContextSpec extends FreeSpec with Matchers {

  val context = new IDPContext

  "A IDPContext should" - {
    def newType(context: IDPContext) = context.Type(int)
    def newPredicate(context: IDPContext) = context.Pred(int)
    def newFunction(context: IDPContext) = context.Func(int)(int)
    val TypeI = newType(context)
    val PredI = newPredicate(context)
    val FuncIToI = newFunction(context)

    "allow XXX to be created in a context" - {
      "XXX = Type" in { assert(TypeI != null) }
      "XXX = Predicate" in { assert(PredI != null) }
      "XXX = Function" in { assert(FuncIToI != null) }
    }

    "allow XXX to be created in another context, with a different name" - {
      "XXX = Type" in { newType(new IDPContext) should not equal (TypeI) }
      "XXX = Predicate" in { newPredicate(new IDPContext) should not equal (PredI) }
      "XXX = Function" in { newFunction(new IDPContext) should not equal (FuncIToI) }
    }
  }
}