package be.kuleuven.IDP.test

import org.scalatest.FreeSpec
import org.scalatest.Matchers

import be.kuleuven.IDP.IDPContext
import be.kuleuven.IDP.Types
import be.kuleuven.IDP.helper.extractors.VocabularyExtractor
import be.kuleuven.IDP.representation.AST.Formula
import be.kuleuven.IDP.representation.AST.PFDecl
import be.kuleuven.IDP.representation.AST.Theory
import be.kuleuven.IDP.test.mock.SimpleTheory

class TheorySpec extends FreeSpec with Matchers {

  val context = new IDPContext
  import context._

  def t(s: String) = convertToFreeSpecStringWrapper(s)

  t("Theories") - {

    val pred = Pred(Types.int)

    "must be created by sequencing formula statements" in {
      theory(
        pred(1),
        pred(2),
        pred(3)
      ).theoStmts should equal(
          List(pred(1), pred(2), pred(3))
        )
    }

    t("must be able to extract it's vocabulary") - {
      def toMultipleConjuncts(a: List[Formula]): List[Formula] = {
        return a.grouped(2).toList.map(f => if (f.size == 1) f(0) else f(0) & f(1))
      }

      def toDisjunct(a: List[Formula]): Formula = {
        return a.tail.foldLeft(a.head) {
          case (acc, f) => acc | f
        }
      }

      def test(xs: List[PFDecl], t: Theory) {
        val w = new VocabularyExtractor(t)
        w.toList.toSet should equal(xs.toSet ++ xs.map(x => x.types).flatten.toSet)
      }

      val (decls, fs) = SimpleTheory.SingleFormulaStatement.allPFsSeparate
      val conjuncts = toMultipleConjuncts(fs)

      "for predicates and functions P" - {

        SimpleTheory.SingleFormulaStatement.allPFs.take(1)
          .foreach(x =>
            s"when P is ${x._1}" in {
              test(List(x._1), theory(x._2))
            }
          )

        "when P is a combination of all single statements" in {
          test(decls, theory(fs: _*))
        }

        "when P is a conjunction of all single statements" in {
          test(decls, theory(conjuncts: _*))
        }

        "where P is a negation of a formula" in {
          test(decls, theory(toMultipleConjuncts(fs.map(x => ~x)): _*))
        }

        "when P is a disjunction of conjunctions of all single statements" in {
          val disjunct: Formula = toDisjunct(conjuncts)
          test(decls, theory(disjunct))
        }

        "when P is an implication of two disjunction of conjunctions of all single statements" in {
          val (before, after) = conjuncts.splitAt(conjuncts.size / 2)
          val f: Formula = toDisjunct(before) ==> toDisjunct(after)
          test(decls, theory(f))
        }

        "when P is an equivalence of two disjunction of conjunctions of all single statements" in {
          val (before, after) = conjuncts.splitAt(conjuncts.size / 2)
          val f: Formula = toDisjunct(before) <=> toDisjunct(after)
          test(decls, theory(f))
        }
      }

      "when P is a definition" - {

        "made with only single bodies" - {

          SimpleTheory.SingleFormulaStatement.allPFs
            .foreach { x =>
              s"when P is ${x._1}" in {
                test(List(x._1), theory(definition(x._2)))
              }
            }
        }

        "made with a combination of single bodies" in {
          test(decls, theory(definition(fs.map(formulaToRule(_)): _*)))
        }

        "made with variations" - {
          val rules = SimpleTheory.SingleFormulaStatement.declToRule
          rules.foreach(r =>
            s"such as rule ${r._2}" in {
              test(r._1, theory(definition(r._2)))
            }
          )
        }
      }
    }
  }

}