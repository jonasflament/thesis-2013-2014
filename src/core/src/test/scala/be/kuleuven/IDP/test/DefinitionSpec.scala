package be.kuleuven.IDP.test

import be.kuleuven.IDP.IDPContext
import be.kuleuven.IDP.Types._
import org.scalatest.Matchers
import org.scalatest.FreeSpec
import be.kuleuven.IDP.representation.AST.Formula
import be.kuleuven.IDP.representation.AST.Rule
import be.kuleuven.IDP.representation.AST.FormulaHead
import be.kuleuven.IDP.representation.AST.Not
import be.kuleuven.IDP.representation.AST.Rule
import be.kuleuven.IDP.representation.AST.FormulaHead
import be.kuleuven.IDP.representation.AST.Quantification
import be.kuleuven.IDP.representation.AST.Variable
import be.kuleuven.IDP.representation.AST.ForAll
import be.kuleuven.IDP.representation.AST.Definition
import be.kuleuven.IDP.representation.AST.Quantified
import be.kuleuven.IDP.representation.AST.Quantification
import be.kuleuven.IDP.representation.AST.FalseTerm
import be.kuleuven.IDP.representation.AST.FalseForm

class DefinitionSpec extends FreeSpec with Matchers {

  val context = new IDPContext
  import context._

  val T = int

  val PPred = Pred()
  val QPred = Pred()

  /**
   * Invoke the apply() function to create a formula
   */
  val P: Formula = PPred()
  val Q: Formula = QPred()

  val R = Pred(T)

  val F = Func()(T)

  def t(s: String) = convertToFreeSpecStringWrapper(s)
  t("Rules should") - {

    t("be possible to create by") - {

      "P" in {
        val r: Rule = P
        r should equal(
          Rule(
            Nil,
            FormulaHead(P),
            None))
      }

      "~P" in {
        var r: Rule = ~P
        r should equal(
          Rule(
            Nil,
            FormulaHead(
              Not(P)
            ),
            None))
      }

      "F() === 1" in {
        val r: Rule = (F() === 1)
        r should equal(
          Rule(
            Nil,
            FormulaHead(F() === 1),
            None)
        )
      }

      "P <-- Q" in {
        val r: Rule = P <-- Q
        r should equal(
          Rule(
            Nil,
            FormulaHead(P),
            Some(Q)))
      }

      "P <-- true" in {
        val r = P <-- true
        r should equal(
          Rule(
            Nil,
            FormulaHead(P),
            None)
        )
      }

      "P <-- false" in {
        val r = P <-- false
        r should equal(
          Rule(
            Nil,
            FormulaHead(P),
            Some(FalseForm))
        )
      }

      "!x: R(x)" in {
        val r = !('x -> T)(R('x): Rule)

        r should equal(
          Rule(
            List(Quantification(ForAll, None, None, List(Variable("x", T)))),
            FormulaHead(R('x)),
            None)
        )
      }

      "!x: R(x) <-- Q" in {
        val r = !('x -> T)(R('x) <-- Q)

        r should equal(
          Rule(
            List(Quantification(ForAll, None, None, List(Variable("x", T)))),
            FormulaHead(R('x)),
            Some(Q))
        )
      }

      "!x: R(x) <-- R(x)" in {
        val r = !('x -> T)(R('x) <-- R('x))

        r should equal(
          Rule(
            List(Quantification(ForAll, None, None, List(Variable("x", T)))),
            FormulaHead(R('x)),
            Some(R('x)))
        )
      }

      "P <-- ?x R(x)" in {
        val r: Rule = P <-- ?('x -> T)(R('x))
        r should equal(
          Rule(
            Nil,
            FormulaHead(P),
            Some(
              ?('x -> T)(R('x))
            )))
      }

      "!x: R(x) <-- ?y R(y)" in {
        val r = !('x -> T)(
          R('x) <-- ?('y -> T)(R('x)))

        r should equal(
          Rule(
            List(Quantification(ForAll, None, None, List(Variable("x", T)))),
            FormulaHead(R('x)),
            Some(?('y -> T)(R('x))))
        )
      }
    }
  }

  t("Definitions must") - {

    val r1 = P
    val r1AST = Rule(Nil, FormulaHead(P), None)

    val r2 = P <-- Q
    val r2AST = Rule(Nil, FormulaHead(P), Some(Q))

    val r3 = !('x -> T)(P)
    val r3AST = Rule(
      Nil,
      FormulaHead(
        Quantified(Quantification(ForAll, None, None, List(Variable("x", T))),
          P)),
      None)

    val r4 = P <-- ?('x -> T)(R('x))
    val r4AST = Rule(
      Nil,
      FormulaHead(P),
      Some(?('x -> T)(R('x))))

    "be able to hold a single rule" - {

      "where the rule has a head but no body" in {
        definition(r1) should equal(Definition(List(r1AST)))
      }

      "where the rule has both head and body" in {
        definition(r2) should equal(Definition(List(r2AST)))
      }

      "where there is only a head, but is quantified" in {
        definition(r3) should equal(Definition(List(r3AST)))
      }

      "where there is both a head and body, but the body is quantified" in {
        definition(r4) should equal(Definition(List(r4AST)))
      }
    }

    "be able to hold multiple rules" in {
      definition(
        r1,
        r2,
        r3,
        r4
      ) should equal(
          Definition(
            List(r1AST, r2AST, r3AST, r4AST)
          )
        )
    }
  }
}