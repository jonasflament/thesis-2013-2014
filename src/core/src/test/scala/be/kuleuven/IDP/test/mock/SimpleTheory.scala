package be.kuleuven.IDP.test.mock

import be.kuleuven.IDP.IDPContext
import be.kuleuven.IDP.representation.AST.Predicate
import be.kuleuven.IDP.representation.AST.IDP
import be.kuleuven.IDP.Types.{ string, char, int }
import be.kuleuven.IDP.representation.AST.Formula
import be.kuleuven.IDP.representation.AST.PFDecl
import be.kuleuven.IDP.representation.AST.Formula
import be.kuleuven.IDP.representation.AST.Theory
import be.kuleuven.IDP.representation.AST.Theory
import be.kuleuven.IDP.representation.AST.PFDecl
import be.kuleuven.IDP.representation.AST.Rule
import be.kuleuven.IDP.representation.AST.TheoryStatement
import be.kuleuven.IDP.representation.AST.Quantification

object SimpleTheory {

  private val context = new IDPContext

  import context._;

  val PI = Pred(int)
  val PC = Pred(char)
  val PS = Pred(string)

  val PII = Pred(int, int)
  val PCC = Pred(char, char)
  val PSS = Pred(string, string)

  val FI = Func()(int)
  val FC = Func()(char)
  val FS = Func()(string)

  val FII = Func(int)(int)
  val FCI = Func(char)(int)
  val FSC = Func(string)(char)

  object SingleFormulaStatement {

    val declToPF: List[(PFDecl, Formula)] = List(
      PI -> PI(1),
      PC -> PC('1'),
      PS -> PS("1"),

      PII -> PII(1, 2),
      PCC -> PCC('1', '2'),
      PSS -> PSS("1", "2"),

      FI -> (FI() === 1),
      FC -> (FC() === '1'),
      FS -> (FS() === "1"),

      FII -> (FII(1) === 2),
      FCI -> (FCI('1') === 1),
      FSC -> (FSC("1") === '1')
    )

    val declToQuant: List[(PFDecl, Formula)] = List(
      PI -> !('x -> int)(PI('x)),
      PC -> !('x -> char)(PC('x)),
      PS -> !('x -> string)(PS('x)),

      PII -> !('x -> int)(PII('x, 2)),
      PCC -> !('x -> char)(PCC('1', 'x)),
      PSS -> !('x -> string)(PSS('x, "2")),

      FI -> !('x -> int)(FI() === 'x),
      FC -> !('x -> char)(FC() === 'x),
      FS -> !('x -> string)(FS() === 'x),

      FII -> !('x -> int)(FII('x) === 2),
      FCI -> !('x -> char)(FCI('1') === 'x),
      FSC -> !('x -> string)(FSC('x) === '1')
    )

    val declToRule: List[(List[PFDecl], Rule)] = {
      // create P(1) type expressions
      declToPF.map(x => (List(x._1), formulaToRule(x._2))) ++
        // create P(1) <-- P('1') type of expressions
        declToPF.grouped(2).toList.map { l =>
          if (l.size == 1) {
            (List(l(0)._1), formulaToRule(l(0)._2))
          } else {
            (l.map(_._1).asInstanceOf[List[PFDecl]], ((l(0)._2) <-- (l(1)._2)).asInstanceOf[Rule])
          }
        } ++
        //       create ! x: P(x) type of expressions
        declToQuant.map(x => (List(x._1), formulaToRule(x._2))) ++
        // create !x: P(x) <-- ! y: P(y) type of expressions
        declToQuant.grouped(2).toList.map { l =>
          if (l.size == 1) {
            (List(l(0)._1), formulaToRule(l(0)._2))
          } else {
            (l.map(_._1).asInstanceOf[List[PFDecl]], ((l(0)._2) <-- (l(1)._2)).asInstanceOf[Rule])
          }
        }
    }

    def allPFs: List[(PFDecl, Formula)] = {
      return declToPF.map(x => (x._1, x._2))
    }

    def allPFsSeparate: (List[PFDecl], List[Formula]) = {
      return SimpleTheory.SingleFormulaStatement.allPFs.foldLeft((List[PFDecl](), List[Formula]())) {
        case (acc, (x, y)) => (acc._1 :+ x, acc._2 :+ y)
      }
    }
  }

  object MultipleStatment {

    def allSingleCombined: (List[PFDecl], List[TheoryStatement]) = {
      val l = SingleFormulaStatement.declToPF
      val allPDecl = l.map(_._1)
      val allPreds = l.map(_._2)
      return (allPDecl, allPreds)
    }
  }

}