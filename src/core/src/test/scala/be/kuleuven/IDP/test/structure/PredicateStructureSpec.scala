package be.kuleuven.IDP.test.structure

import be.kuleuven.IDP.representation.AST.IDP
import be.kuleuven.IDP.IDPContext
import org.scalatest.Matchers
import org.scalatest.FreeSpec
import be.kuleuven.IDP.Types._
import be.kuleuven.IDP.representation.AST.PFDecl
import be.kuleuven.IDP.representation.AST.StructureStatement
import be.kuleuven.IDP.representation.AST.IntRange
import be.kuleuven.IDP.representation.AST.InterElem
import be.kuleuven.IDP.representation.AST.Structure
import be.kuleuven.IDP.representation.AST.InterDecl
import be.kuleuven.IDP.representation.AST.ThreeValued
import be.kuleuven.IDP.representation.AST.InterList
import be.kuleuven.IDP.representation.AST.ElemList
import be.kuleuven.IDP.representation.AST.IntE
import be.kuleuven.IDP.representation.AST.CharRange
import be.kuleuven.IDP.representation.AST.CharE
import be.kuleuven.IDP.representation.AST.StringE
import be.kuleuven.IDP.test.SpecHelper
import be.kuleuven.IDP.representation.AST.StringE
import be.kuleuven.IDP.representation.AST.{ CT, CF, U }
import be.kuleuven.IDP.representation.AST.ThreeValuedIdentifier

class PredicateStructureSpec extends FreeSpec with Matchers {
  implicit val context = new IDPContext()
  import context._

  import SpecHelper._

  val PredI = Pred(int)
  val PredII = Pred(int, int)

  val PredS = Pred(string)
  val PredC = Pred(char)

  "In a structure for predicates" - {
    "an interpretation must be definable for" - {
      "P/1" - {

        "Pred(Int)" - {
          "as range" in {
            testSimpleStructure(
              PredI,
              List(
                IntRange(1, 5),
                IntRange(6, 9)),
              PredI as (1 to 5, 6 until 10)
            )
          }

          "as enumeration" in {
            val toTest = List(1, 3, 5, 4, 2)
            testSimpleStructure(
              PredI,
              toTest.map(IntE(_)),

              PredI as (toTest: _*)
            )
          }
        }

        "Pred(Char)" - {
          "as range" in {
            testSimpleStructure(
              PredC,
              List(
                CharRange('a', 'f'),
                CharRange('j', 'y')),

              PredC as ('a' to 'f', 'j' until 'z')
            )
          }

          "as enumeration" in {
            val toTest = List('a', 'y', 'l', 'n', 'z')
            testSimpleStructure(
              PredC,
              toTest map (CharE(_)),

              PredC as (toTest: _*)
            )
          }
        }

        "Pred(String)" in {
          val toTest = List("I", "am", "a", "little", "test")
          testSimpleStructure(
            PredS,
            toTest.map(StringE(_)),

            PredS as (toTest: _*)
          )
        }
      }

      "P/n, n > 1" - {
        "Pred(Int, Int)" in {

          def intsToEs(i: Int*) = i.toList.map(IntE(_))

          testSimpleStructure(
            PredII,
            List(
              InterList(intsToEs(1, 2)),
              InterList(intsToEs(2, 3))
            ),

            PredII as (1 -> 2, 2 -> 3))
        }
      }
    }

    "Tagged interpretations" - {

      def testTaggedInt(tag: ThreeValuedIdentifier, struct: StructureStatement) {
        testTaggedStructure(
          PredI,
          tag,
          List(
            IntRange(1, 5),
            IntRange(6, 9)
          ),
          struct
        )
      }

      "Pred(Int) ct" in {
        testTaggedInt(CT, PredI ct () as (1 to 5, 6 until 10))
      }
      "Pred(Int) cf" in {
        testTaggedInt(CF, PredI cf () as (1 to 5, 6 until 10))
      }
      "Pred(Int) u" in {
        testTaggedInt(U, PredI u () as (1 to 5, 6 until 10))
      }
    }
  }
}