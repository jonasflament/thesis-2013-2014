package be.kuleuven.IDP.test;

import org.scalatest.FlatSpec
import org.scalatest.Matchers
import be.kuleuven.IDP.representation.AST.ASTTypes.Name
import be.kuleuven.IDP.representation.AST.TypeDecl
import be.kuleuven.IDP.representation.AST.Predicate
import be.kuleuven.IDP.representation.AST.PredicateForm
import be.kuleuven.IDP.representation.AST.IntegerTerm
import be.kuleuven.IDP.representation.AST.Function
import be.kuleuven.IDP.Types
import be.kuleuven.IDP.exceptions.InvalidCardinalityException
import be.kuleuven.IDP.representation.AST.Term
import be.kuleuven.IDP.representation.AST.FunctionTerm
import be.kuleuven.IDP.representation.AST.CharLiteralTerm
import be.kuleuven.IDP.representation.AST.StringLiteralTerm
import be.kuleuven.IDP.representation.AST.FunctionTerm
import be.kuleuven.IDP.representation.AST.ConnectedT
import be.kuleuven.IDP.representation.AST.Plus
import scala.collection.mutable.ArrayBuffer
import be.kuleuven.IDP.IDPContext
import be.kuleuven.IDP.representation.AST.VariableSym
import be.kuleuven.IDP.representation.AST.Not

class PFDeclSpec extends FlatSpec with Matchers {

  behavior of "IDP context"

  val context = new IDPContext
  import context._

  def lastTypeName = s"${context.UPfix.Type}${IDPContext.typeCounter}"
  def lastPredName = s"${context.UPfix.Pred}${IDPContext.predCounter}"
  def lastFuncName = s"${context.UPfix.Func}${IDPContext.funcCounter}"

  var t1: TypeDecl = null
  it should "allow new a new type <: IntType to be created with an automatically generated name" in {
    t1 = Type(Types.int)
    t1 should equal(TypeDecl(lastTypeName, None, Some(Types.int), None))
  }

  var t2: TypeDecl = null
  it should "allow a second new type <: IntType to be created, with a different automatically generated name" in {
    t2 = Type(Types.int)
    t2 should equal(TypeDecl(lastTypeName, None, Some(Types.int), None))
    t2 should not equal(t1)
  }

  var pred: Predicate = null
  it should "allow a new predicate based on the two created types" in {
    pred = Pred(t1, t2)
    pred should equal(Predicate(lastPredName, List(t1, t2)))
  }

  it should "be possible to call the predicate given two terms of the correct type" in {
    val f = pred(1, 1)
    f should equal(PredicateForm(pred, List(IntegerTerm(1), IntegerTerm(1))))
  }

  it should "throw an exception when a predicate is used with an incorrect number of arguments" in {
    intercept[InvalidCardinalityException] {
      pred(1)
    }
  }

  ignore should "throw an exception when a predicate is used with an incorrect type" in {
    intercept[IllegalArgumentException] {
      pred("a", "a")
    }
  }

  it should "allow a predicate with char arguments to be created and used" in {
    val cT = Type(Types.char)
    cT should equal(TypeDecl(lastTypeName, None, Some(Types.char), None))

    val cPred = Pred(cT)
    cPred should equal(Predicate(lastPredName, List(cT)))

    val f = cPred('a')
    f should equal(PredicateForm(cPred, List(CharLiteralTerm('a'))))
  }

  it should "allow a predicate with string arguments to be created and used" in {
    val sT = Type(Types.string)
    sT should equal(TypeDecl(lastTypeName, None, Some(Types.string), None))

    val sPred = Pred(sT)
    sPred should equal(Predicate(lastPredName, List(sT)))

    val f = sPred("abc")
    f should equal(PredicateForm(sPred, List(StringLiteralTerm("abc"))))
  }

  var func: Function = null
  it should "allow a function to be created" in {
    func = Func(t1)(t2)
    func should equal(Function(lastFuncName, List(t1), t2))
  }

  it should "allow another function to be created with a different name" in {
    val func = Func(t1)(t2)
    func should equal(Function(lastFuncName, List(t1), t2))
  }

  it should "throw an exception when a function is invoked with an incorrect number of arguments" in {
    intercept[InvalidCardinalityException] {
      func()
    }
  }

  ignore should "throw an exception when a function is invoked with an incorrect type" in {
    intercept[IllegalArgumentException] {
      func("a")
    }
  }

  it should "be possible to invoke a function with correct arguments" in {
    val t: Term = func(1)
    t should equal(FunctionTerm(func, List(1)))
  }

  it should "be possible to use a predicate by passing it an invoked function" in {
    val f = pred(1, func(2))
    f should equal(PredicateForm(pred, List(IntegerTerm(1), FunctionTerm(func, List(IntegerTerm(2))))))
  }

  it should "allow a predicate with a connected term, created by using func(i) + j, to be created and used" in {
    val iT = Type(Types.int)
    iT should equal(TypeDecl(lastTypeName, None, Some(Types.int), None))

    val sPred = Pred(iT)
    sPred should equal(Predicate(lastPredName, List(iT)))

    val iF = Func(iT)(iT)
    iF should equal(Function(lastFuncName, List(iT), iT))

    val f = sPred(iF(2) + 1)
    f should equal(PredicateForm(sPred, List(
        ConnectedT(Plus, FunctionTerm(iF, List(IntegerTerm(2))), IntegerTerm(1))
        )))
  }

  it should "be possible to create a predicate given a formula" in {
    val pred = Pred(t1)
    val form = pred('x)
    form should equal(
      PredicateForm(
        pred,
        List(VariableSym("x")))
    )
  }

  it should "be possible to create the negation of a predicate" in {
    val pred = Pred(t1)
    val form = ~pred(1)

    form should equal(
      Not(pred(1))
    )

  }
}