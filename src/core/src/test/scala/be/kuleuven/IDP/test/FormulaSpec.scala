package be.kuleuven.IDP.test

import org.scalatest.Matchers
import be.kuleuven.IDP.IDPContext
import be.kuleuven.IDP.Types._
import be.kuleuven.IDP.representation.AST.IntegerTerm
import be.kuleuven.IDP.representation.AST.ConnectedF
import be.kuleuven.IDP.representation.AST.Impl
import be.kuleuven.IDP.representation.AST.Equiv
import org.scalatest.FreeSpec
import be.kuleuven.IDP.representation.AST.Quantification
import be.kuleuven.IDP.representation.AST.ForAll
import be.kuleuven.IDP.representation.AST.Exists
import be.kuleuven.IDP.representation.AST.EqChainForm
import be.kuleuven.IDP.representation.AST.EqChainForm
import be.kuleuven.IDP.representation.AST.TypeDecl
import be.kuleuven.IDP.representation.AST.PredicateForm
import be.kuleuven.IDP.representation.AST.And
import be.kuleuven.IDP.representation.AST.Or
import be.kuleuven.IDP.representation.AST.Formula
import be.kuleuven.IDP.representation.AST.Term
import be.kuleuven.IDP.representation.AST.EQ
import be.kuleuven.IDP.representation.AST.Quantified
import be.kuleuven.IDP.representation.AST.Variable
import be.kuleuven.IDP.representation.AST.Quantified

class FormulaSpec extends FreeSpec with Matchers {

  val T = int;

  val context = new IDPContext;
  import context._;

  val P = Pred("P", T);
  val Q = Pred("Q", T);

  def t(s: String): FormulaSpec.this.FreeSpecStringWrapper = convertToFreeSpecStringWrapper(s)

  t("Formulas") - {
    t("given P(x), with predicate P and integer x should") - {
      t("create a formula when P(x) is written") in {
        val f = P(1)
        f should equal(PredicateForm(P, List(IntegerTerm(1))))
      }
    }

    t("- given P/1 and Q/1 predicates ") - {
      t("- and c a logical connective should create P(x) c Q(x) when") - {
        val PForm = PredicateForm(P, List(IntegerTerm(1)))
        val QForm = PredicateForm(Q, List(IntegerTerm(2)))

        t("c = '&'") in {
          val f = P(1) & Q(2)
          f should equal(ConnectedF(And, PForm, QForm))
        }

        "c = '|'" in {
          val f = P(1) | Q(2)
          f should equal(ConnectedF(Or, PForm, QForm))
        }

        "c = '==>'" in {
          val f = P(1) ==> Q(2)
          f should equal(ConnectedF(Impl, PForm, QForm))
        }

        "c = '<=='" in {
          val f = P(1) <== Q(2)
          f should equal(ConnectedF(Impl, QForm, PForm))
        }

        "c = '<=>'" in {
          val f = P(1) <=> Q(2)
          f should equal(ConnectedF(Equiv, PForm, QForm))
        }
      }

      t("- using equality should") - {
        "create 1 === 2" in {
          val f: Formula = (1 === 2)
          f should equal(EqChainForm(1: Term, List((EQ, 2: Term))))
        }

        "create (1 === 2) === 3, 1 === (2 === 3), 1 === 2 === 3" in {
          def testEqual(f: Formula) = {
            f should equal(
              EqChainForm(
                1: Term,
                List((EQ, 2: Term), (EQ, 3: Term))
              )
            )
          }

          testEqual((1 === 2) === 3)
          testEqual(1 === (2 === 3))
          testEqual(1 === 2 === 3)
        }
      }

      t(" using quantification") - {
        t("forall") - {
          "should create forall(x: T) P(x)" in {
            val f = !('x -> T)(P('x))

            f should equal(Quantified(Quantification(ForAll, None, None, List(Variable("x", T))), P('x)))
          }
        }

        "exists" - {
          "should create forall(x) P(x)" in {
            val f = ?('x -> T)(P('x))
            f should equal(Quantified(Quantification(Exists, None, None, List(Variable("x", T))), P('x)))
          }
        }
      }
    }

    t("extra tests for special cases when required") - {
      val T = Type(int)
      val P = Pred(T, T, T)

      "!x[T], y[T]: ?1 z[T] : P(x, y, z)" in {
        val f = !('x -> T, 'y -> T)(
          ?=(1, 'z -> T)(P('x, 'y, 'z))
        )

        val uQuant = Quantification(ForAll, None, None, List(Variable('x.name, T), Variable('y.name, T)))
        val eQuant = Quantification(Exists, Some(EQ), Some(1), List(Variable('z.name, T)))

        f should equal(
          Quantified(
            uQuant,
            Quantified(
              eQuant, P('x, 'y, 'z))
          )
        )
      }
    }
  }
}