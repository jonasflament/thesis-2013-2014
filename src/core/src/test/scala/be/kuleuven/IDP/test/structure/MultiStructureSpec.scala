package be.kuleuven.IDP.test.structure

import org.scalatest.Matchers
import org.scalatest.FreeSpec
import be.kuleuven.IDP.Types._
import be.kuleuven.IDP.IDPContext
import be.kuleuven.IDP.representation.AST.PFDecl
import be.kuleuven.IDP.representation.AST.Structure
import be.kuleuven.IDP.representation.AST.InterDecl
import be.kuleuven.IDP.representation.AST.ThreeValued
import be.kuleuven.IDP.representation.AST.ElemList
import be.kuleuven.IDP.representation.AST.IntRange
import be.kuleuven.IDP.representation.AST.InterElem
import be.kuleuven.IDP.representation.AST.InterList
import be.kuleuven.IDP.representation.AST.StringE
import be.kuleuven.IDP.representation.AST.IntE
import be.kuleuven.IDP.representation.AST.CharE
import be.kuleuven.IDP.representation.AST.IntRange
import be.kuleuven.IDP.representation.AST.CharRange
import be.kuleuven.IDP.representation.AST.InterList
import be.kuleuven.IDP.representation.AST.Structure
import be.kuleuven.IDP.representation.AST.InterList
import be.kuleuven.IDP.representation.AST.IDP

class MultiStructureSpec extends FreeSpec with Matchers {
  val context = new IDPContext()
  import context._

  val PredI = Pred(int)
  val PredC = Pred(char)
  
  def t(s: String) = convertToFreeSpecStringWrapper(s)

  t("A structure for both predicates as functions must") - {

    t("allow defining multiple predicates at once, such as") - {

      "P(Int), P(Char)" in {
        val charsToTest = List('a', 'q', 'l', 'o')

        val struct = structure(
          interpret(PredI) as (1 to 5, 5 to 10),
          interpret(PredC) as (charsToTest: _*)
        )
        struct.stmts should equal(
          List(
            InterDecl(
              ThreeValued(PredI, None),
              ElemList(List(
                IntRange(1, 5),
                IntRange(5, 10)
              ))
            ),
            InterDecl(
              ThreeValued(PredC, None),
              ElemList(
                charsToTest.map(CharE(_))
              )
            )
          )
        )
      }
    }
  }
}