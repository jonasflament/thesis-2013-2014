package be.kuleuven.IDP.test.structure

import org.scalatest.FreeSpec
import org.scalatest.Matchers
import be.kuleuven.IDP
import be.kuleuven.IDP.IDPContext
import be.kuleuven.IDP.Types._
import be.kuleuven.IDP.representation.AST.CF
import be.kuleuven.IDP.representation.AST.CT
import be.kuleuven.IDP.representation.AST.CharE
import be.kuleuven.IDP.representation.AST.IntE
import be.kuleuven.IDP.representation.AST.InterList
import be.kuleuven.IDP.representation.AST.StringE
import be.kuleuven.IDP.representation.AST.ThreeValuedIdentifier
import be.kuleuven.IDP.representation.AST.U
import be.kuleuven.IDP.test.SpecHelper
import be.kuleuven.IDP.representation.AST.StructureStatement

class FunctionStructureSpec extends FreeSpec with Matchers {
  implicit val context = new IDPContext()
  import context._

  import IDP._
  import SpecHelper._

  val FuncIToI = Func(int)(int)
  val FuncIIToI = Func(int, int)(int)

  val FuncCToI = Func(char)(int)
  val FuncSToC = Func(string)(char)
  val FuncSToI = Func(string)(int)

  val FuncToI = Func()(int)
  val FuncToC = Func()(char)
  val FuncToS = Func()(string)

  val FuncICToS = Func(int, char)(string)
  
  def t(s: String) = convertToFreeSpecStringWrapper(s)

  t("A structure for functions") - {
    t("must allow functions to be interpreted, such as") - {

      "Func = Int" in {
        val toTest = 845
        testSimpleStructure(FuncToI,
          List(IntE(toTest)),
          FuncToI as (toTest)
        )
      }

      "Func = Char" in {
        val toTest = 'g'
        testSimpleStructure(FuncToC,
          List(CharE(toTest)),
          FuncToC as (toTest)
        )
      }

      "Func = String" in {
        val toTest = "abqsdf"
        testSimpleStructure(FuncToS,
          List(StringE(toTest)),
          FuncToS as (toTest)
        )
      }

      /**
       * Helper method to test Func(Int) = Int by writing (a) -> b, or just writing a -> b
       */
      def testIToI(toTest: List[(Int, Int)], struct: StructureStatement) = {
        testSimpleStructure(
          FuncIToI,
          toTest.map { case (a, b) => InterList(List(IntE(a), IntE(b))) },
          struct
        )
      }

      "Func(Int) = Int " - {
        "as '(a) -> b'" in {
          testIToI(List((1, 2), (2, 3)),
            FuncIToI as ((1) -> 2, (2) -> 3)
          )
        }

        "as 'a -> b'" in {
          testIToI(List((1, 2), (2, 3)),
            FuncIToI as (1 -> 2, 2 -> 3)
          )
        }
      }

      /**
       * Tested with a function that, given two integers, maps them on their sum
       */
      "Func(Int, Int) = Int" in {
        val toTest = List((1, 2), (3, 4))
        val fRes = toTest.map { case (a, b) => (a, b) -> (a + b) }

        testSimpleStructure(FuncIIToI,
          fRes.map { case ((a, b), c) => InterList(List(IntE(a), IntE(b), IntE(c))) },

          FuncIIToI as (fRes: _*)
        )
      }

      /**
       * Tested with a function that, given a string, maps that string onto its size
       */
      "Func(String) = Int" in {
        val toTest = List("Three", "Four", "Five")
        val fRes: List[(String, Int)] = toTest.map(string => (string, string.length))

        testSimpleStructure(FuncSToI,
          fRes.map { case (string, size) => InterList(List(StringE(string), IntE(size))) },

          FuncSToI as (fRes.map { case (string: String, size: Int) => string -> size }: _*)
        )
      }

      /**
       * Tested with a function that, given a string, maps that string to its first character
       */
      "Funct(String) = Char" in {
        val toTest = List("ABC", "Def", "gHi")
        val fRes = toTest.map(string => (string, string.charAt(0)))

        testSimpleStructure(FuncSToC,
          fRes.map { case (string, char) => InterList(List(StringE(string), CharE(char))) },

          FuncSToC as (fRes.map { case (string, char) => string -> char }: _*)
        )
      }

      /**
       * Tested with a function that, given a character, maps the character to its Integer value
       */
      "Funct(Char) = Int" in {
        val toTest = List('a', 'r', 'z')
        val fRes = toTest.map(x => (x, x.toInt))

        testSimpleStructure(FuncCToI,
          fRes.map { case (char, int) => InterList(List(CharE(char), IntE(int))) },

          FuncCToI as (fRes: _*)
        )
      }

      "Func(Int, Char) = String" in {
        val toTest = List((2, 'b'), (7, 'o'), (4, 'z'))
        val fRes = toTest.map { case (i, c) => (i, c) -> (s"$c" * i) }

        testSimpleStructure(FuncICToS,
          fRes.map {
            case ((i, c), string) =>
              InterList(List(
                IntE(i),
                CharE(c),
                StringE(string)
              ))
          },
          FuncICToS as (fRes: _*)
        )
      }

      "Tagged interpretations" - {

        def testTaggedInt(tag: ThreeValuedIdentifier, struct: StructureStatement) {
          testTaggedStructure(
            FuncIToI,
            tag,
            List(
              InterList(List(IntE(1), IntE(2))),
              InterList(List(IntE(2), IntE(3)))
            ),
            struct
          )
        }

        "Pred(Int) ct" in {
          testTaggedInt(CT, FuncIToI ct() as (1 -> 2, (2) -> 3))
        }
        "Pred(Int) cf" in {
          testTaggedInt(CF, FuncIToI cf() as (1 -> 2, (2) -> 3))
        }
        "Pred(Int) u" in {
          testTaggedInt(U, FuncIToI u() as (1 -> 2, (2) -> 3))
        }
      }

    }
  }
}