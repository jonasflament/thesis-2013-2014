package be.kuleuven.IDP.test.structure

import org.scalatest.FreeSpec
import org.scalatest.Matchers
import be.kuleuven.IDP.IDPContext
import be.kuleuven.IDP.Types._
import be.kuleuven.IDP.helper.StructureHelper
import be.kuleuven.IDP.mx

class StructureHelperSpec extends FreeSpec with Matchers {

  val c = new IDPContext
  import c._

  val P = Pred(int)
  val h = new StructureHelper()

  def t(s: String) = convertToFreeSpecStringWrapper(s)

  t("The mx package, with the structure helper") - {

    t("should allow statments to be made true") - {

      t("in a simple structure") - {

        t("isOther") in {
          val s = structure(
            P as (1, 2)
          )

          val r = h.makeTrue(s, P as (3))
          val table = mx.asIntTable(r, P)
          val cts = table.cts
          assert((1 to 2).toList.forall(cts.contains(_)), "The original elemens were removed")
          assert(cts.contains(3), "The new element was not added")
        }

        t("isCT") in {
          val s = structure(
            P ct () as (1, 2)
          )

          val r = h.makeTrue(s, P as (3))
          val table = mx.asIntTable(r, P)
          val cts = table.cts
          assert((1 to 2).toList.forall(cts.contains(_)), "The original elemens were removed")
          assert(cts.contains(3), "The new element was not added")
        }

        t("isCF") in {
          val s = structure(
            P cf () as (1, 2)
          )
          
          val r = h.makeTrue(s, P as (3))
          val table = mx.asIntTable(r, P)
          
          val cts = table.cts
          val cfs = table.cfs
          
          assert((1 to 2).toList.forall(!cts.contains(_)), "The original elemens were NOT removed")
          assert((1 to 2).toList.forall(cfs.contains(_)), "The original elemens were removed")
          
          assert(cts.contains(3), "The new element was not added")
        }
      }
    }
    
    t("should allow statments to be made false") - {

      t("in a simple structure") - {

        t("isOther") in {
          val s = structure(
            P as (1, 2)
          )

          val r = h.makeFalse(s, P as (3))
          val table = mx.asIntTable(r, P)
          val cts = table.cts
          val cfs = table.cfs
          assert((1 to 2).toList.forall(cts.contains(_)), "The original elemens were removed")
          assert(!cts.contains(3), "The new element was not added")
        }

        t("isCT") in {
          val s = structure(
            P ct () as (1, 2)
          )

          val r = h.makeFalse(s, P as (3))
          val table = mx.asIntTable(r, P)
          val cts = table.cts
          val cfs = table.cfs
          assert((1 to 2).toList.forall(cts.contains(_)), "The original elemens were removed")
          assert(!cts.contains(3), "The new element was added as true")
          assert(cfs.contains(3), "The new element was not added")
        }

        t("isCF") in {
          val s = structure(
            P cf () as (1, 2)
          )
          
          val r = h.makeFalse(s, P as (3))
          val table = mx.asIntTable(r, P)
          
          val cts = table.cts
          val cfs = table.cfs
          
          assert((1 to 2).toList.forall(cfs.contains(_)), "The original elemens were removed")
          assert(!cts.contains(3), "The new element was added as true")
          assert(cfs.contains(3), "The new element was not added")
        }
      }
    }
    
    t("should allow statments to be made unknown") - {

      t("in a simple structure") - {

        t("isOther") in {
          val s = structure(
            P as (1, 2, 3)
          )

          val r = h.makeUnknown(s, P as (3))
          val table = mx.asIntTable(r, P)
          val cts = table.cts
          val cfs = table.cfs
          
          assert((1 to 2).toList.forall(cts.contains(_)), "The original elemens were removed")
          assert(!cts.contains(3), "The new element was not removed from ct")
          assert(!cfs.contains(3), "The new element was not removed from cf")
        }

        t("isCT") in {
          val s = structure(
            P ct () as (1, 2, 3)
          )

          val r = h.makeUnknown(s, P as (3))
          val table = mx.asIntTable(r, P)
          val cts = table.cts
          val cfs = table.cfs
          
          assert((1 to 2).toList.forall(cts.contains(_)), "The original elemens were removed")
          assert(!cts.contains(3), "The new element was not removed from true")
          assert(!cfs.contains(3), "The new element was not removed from false")
        }

        t("isCF") in {
          val s = structure(
            P cf () as (1, 2, 3)
          )
          
          val r = h.makeUnknown(s, P as (3))
          val table = mx.asIntTable(r, P)
          
          val cts = table.cts
          val cfs = table.cfs
          
          assert((1 to 2).toList.forall(cfs.contains(_)), "The original elemens were removed")
          assert(!cts.contains(3), "The new element was not removed from ct")
          assert(!cfs.contains(3), "The new element was not removed from cf")
        }
      }
    }

  }
}