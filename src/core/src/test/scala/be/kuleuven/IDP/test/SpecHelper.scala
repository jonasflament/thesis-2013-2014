package be.kuleuven.IDP.test

import be.kuleuven.IDP.representation.AST.PFDecl
import be.kuleuven.IDP.representation.AST.InterElem
import be.kuleuven.IDP.representation.AST.Structure
import be.kuleuven.IDP.representation.AST.InterDecl
import be.kuleuven.IDP.helper.Structurer
import be.kuleuven.IDP.representation.AST.ThreeValued
import be.kuleuven.IDP.representation.AST.ElemList
import be.kuleuven.IDP.IDPContext
import org.scalatest.exceptions.TestFailedException
import be.kuleuven.IDP.representation.AST.ElemList
import be.kuleuven.IDP.representation.AST.ThreeValuedIdentifier
import be.kuleuven.IDP.representation.AST.StructureStatement

object SpecHelper {
  
  def testTaggedStructure(pf: PFDecl, tag: ThreeValuedIdentifier, elems: List[InterElem], struct: StructureStatement)(implicit context: IDPContext) = {
    import context._

    val tagO = if (tag == null) None else Some(tag)
    
    val structureTest = structure(struct)
    structureTest match {
      case Structure(_, tagI, List(
        InterDecl(
          ThreeValued(pfStructure, _),
          ElemList(
            elemsStructure
            )
          )
        )
        ) if (pfStructure == pf
            && elemsStructure == elems
            && tagI == tagO) =>
      case _ =>
        val stmts = structureTest.stmts
        assert(!stmts.isEmpty, "Structure statements in structure is empty")

        val InterDecl(threeV, structI : ElemList) = stmts(0).asInstanceOf[InterDecl]
        val structurePF = threeV.v
        
        val structElems = structI.elems
        
        assert(structurePF == pf, s"Wrong predicate/function provided: $structurePF did not equal $pf")
        assert(structElems == elems, s"$elems did not equal $structElems")
        
        val tagI = threeV.s
        assert(tagI == tagO, s"Invalid tag: $tagO did not equal $tagI")
    }
  }

  def testSimpleStructure(pf: PFDecl, elems: List[InterElem], struct: StructureStatement)(implicit context: IDPContext) = {
    testTaggedStructure(pf, null, elems, struct)
  }

}