package be.kuleuven.IDP.test.structurelimbo

import org.scalatest.FreeSpec
import org.scalatest.Matchers
import be.kuleuven.IDP.representation.AST.Predicate
import be.kuleuven.IDP.representation.AST.Structure
import be.kuleuven.IDP.representation.AST.StructureInterpretation
import be.kuleuven.IDP.representation.AST.InterElem
import scala.collection.mutable.ArrayBuffer
import be.kuleuven.IDP.representation.AST.ElemList
import be.kuleuven.IDP.representation.AST.IntRange
import be.kuleuven.IDP.IDPContext
import be.kuleuven.IDP.representation.AST.IDP
import be.kuleuven.IDP.representation.AST.InterDecl
import be.kuleuven.IDP.representation.AST.ThreeValued
import be.kuleuven.IDP.representation.AST.StructureStatement
import be.kuleuven.IDP.representation.AST.Structure
import be.kuleuven.IDP.representation.AST.Structure
import be.kuleuven.IDP.representation.AST.ThreeValued
import be.kuleuven.IDP.representation.AST.IntRange
import be.kuleuven.IDP.Types._

class ParameterListStructure extends FreeSpec with Matchers {

  import Helper._

  val context = new IDPContext
  val Pred = context.Pred(int)
  val Pred2 = context.Pred(int)

  "A structure with a parameter list" - {
    "must be able to" - {
      "define a single interpretation" in {
        val struct = structure(
          interpret(Pred) as (1 to 5)
        )
        struct should equal(
          Structure("", Nil, List(
            InterDecl(
              ThreeValued(Pred, None),
              ElemList(List(
                IntRange(1, 5)
              ))
            ))
          ))
      }

      "define multiple interpretation" in {
        val struct = structure(
          interpret(Pred) as (1 to 5),
          interpret(Pred2) as (2 to 4)
        )
        struct should equal(
          Structure("", Nil, List(
            InterDecl(
              ThreeValued(Pred, None),
              ElemList(List(
                IntRange(1, 5)
              ))
            ),
            InterDecl(
              ThreeValued(Pred2, None),
              ElemList(List(
                IntRange(2, 4)
              ))
            )
          )
          ))
      }
    }
  }

  class Structurer(val p: Predicate) {

    var range: Option[Range] = None
    def as(a: Range): Structurer = {
      range = Some(a)
      this
    }

    def collect: StructureStatement = {
      range match {
        case Some(r) =>
          InterDecl(
            ThreeValued(this.p, None),
            ElemList(
              List(
                IntRange(r.start, r.end)
              )
            )
          )
        case None => null
      }
    }
  }

  object Helper {
    def interpret(p: Predicate): Structurer = {
      new Structurer(p)
    }

    val structures = new ArrayBuffer[Structure]

    def structure(structurers: Structurer*): Structure = {
      val structureElems = structurers.toList.map(_.collect)
      val res = Structure("", Nil, structureElems)
      structures += res
      return res
    }
  }
}