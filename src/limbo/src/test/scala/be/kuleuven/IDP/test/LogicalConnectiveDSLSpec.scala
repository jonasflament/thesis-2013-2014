package be.kuleuven.IDP.test

import org.scalatest.FlatSpec
import org.scalatest.Matchers
import be.kuleuven.IDP.representation.AST._
import be.kuleuven.IDP.representation.DSL.NamedLogicalConnectivesDSL

class LogicalConnectiveDSLSpec extends FlatSpec with Matchers with LogicalConnectivesBehavior {
  type FMap = (Formula, Formula) => Formula

  behavior of "NamedLogicalConnectives"
  var fAnd: FMap = null
  var fOr: FMap = null
  var fImpl: FMap = null
  var fEquiv: FMap = null

  new NamedLogicalConnectivesDSL {
    fAnd = ((a: Formula, b: Formula) => a and b)
    fOr = ((a: Formula, b: Formula) => a or b)
    fImpl = ((a: Formula, b: Formula) => a implies b)
    fEquiv = ((a: Formula, b: Formula) => a equiv b)
  }

  it should behave like andConnectives(fAnd)
  it should behave like orConnectives(fOr)
  it should behave like implConnectives(fImpl)
  it should behave like equivConnectives(fEquiv)

  behavior of "SymbolLogicalConnectives"

  fAnd = ((a: Formula, b: Formula) => a & b)
  fOr = ((a: Formula, b: Formula) => a | b)
  fImpl = ((a: Formula, b: Formula) => a ==> b)
  fEquiv = ((a: Formula, b: Formula) => a <=> b)

  it should behave like andConnectives(fAnd)
  it should behave like orConnectives(fOr)
  it should behave like implConnectives(fImpl)
  it should behave like equivConnectives(fEquiv)
}

trait LogicalConnectivesBehavior { this: FlatSpec with Matchers =>
  val aPred = Predicate("A", Nil)
  val bPred = Predicate("b", Nil)
  
  val A = aPred()
  val B = bPred()

  def _test(f: (Formula, Formula) => Formula, o: FOperator) = {
    // TODO: replace ignore with it to run the tests
    it should s"create $o correctly" in {
      f(A, B) should equal(ConnectedF(o, A, B))
    }
  }

  def andConnectives(f: (Formula, Formula) => Formula) = {
    _test(f, And)
  }

  def orConnectives(f: (Formula, Formula) => Formula) = {
    _test(f, Or)
  }

  def implConnectives(f: (Formula, Formula) => Formula) = {
    _test(f, Impl)
  }

  def equivConnectives(f: (Formula, Formula) => Formula) = {
    _test(f, Equiv)
  }

}