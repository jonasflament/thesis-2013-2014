package be.kuleuven.IDP.test.structurelimbo

import org.scalatest.Matchers
import org.scalatest.FreeSpec
import be.kuleuven.IDP.IDPContext
import scala.collection.mutable.ArrayBuffer
import be.kuleuven.IDP.representation.AST.IDP
import be.kuleuven.IDP.representation.AST.Structure
import be.kuleuven.IDP.representation.AST.InterDecl
import be.kuleuven.IDP.representation.AST.StructureStatement
import be.kuleuven.IDP.representation.AST.Predicate
import be.kuleuven.IDP.representation.AST.ThreeValued
import be.kuleuven.IDP.representation.AST.ElemList
import be.kuleuven.IDP.representation.AST.IntRange
import be.kuleuven.IDP.representation.AST.Predicate
import be.kuleuven.IDP.Types._

class ChainStructureSpec extends FreeSpec with Matchers {
  import Helper._

  val context = new IDPContext()
  val Pred1 = context.Pred(int)
  val Pred2 = context.Pred(int)

  "A structure with a parameter list" - {
    "must be able to" - {
      "define a single interpretation" in {
        val struct = structure(
          interpret(Pred1) as (1 to 5)
        )
        struct should equal(
          Structure("", Nil, List(
            InterDecl(
              ThreeValued(Pred1, None),
              ElemList(List(
                IntRange(1, 5)
              ))
            ))
          ))
      }

      "define multiple interpretation" in {
        val struct = structure(
          interpret(Pred1) as (1 to 5)
            interpret (Pred2) as (2 to 4)
        )
        struct should equal(
          Structure("", Nil, List(
            InterDecl(
              ThreeValued(Pred1, None),
              ElemList(List(
                IntRange(1, 5)
              ))
            ),
            InterDecl(
              ThreeValued(Pred2, None),
              ElemList(List(
                IntRange(2, 4)
              ))
            )
          )
          ))
      }
    }
  }

  class Structurer(val p: Predicate) {

    val family = new ArrayBuffer[Structurer]
    def init() {
      family += this
    }
    init()

    var range: Option[Range] = None
    def as(a: Range): Structurer = {
      range = Some(a)
      this
    }

    def collect: List[StructureStatement] = {
      val res = new ArrayBuffer[StructureStatement]

      for (s <- family) {
        s.range match {
          case Some(r) =>
            res += InterDecl(
              ThreeValued(s.p, None),
              ElemList(
                List(
                  IntRange(r.start, r.end)
                )
              )
            )
          case None => null
        }
      }

      res.toList
    }

    def interpret(p: Predicate) = {
      val s = new SubStructurer(this, p)
      family += s
      s
    }
  }

  class SubStructurer(parent: Structurer, p: Predicate) extends Structurer(p) {

    override def init() {}

    override def interpret(p: Predicate) = {
      val s = new SubStructurer(parent, p)
      parent.family += s
      s
    }

    override def collect = parent.collect
  }

  object Helper {
    def interpret(p: Predicate): Structurer = {
      new Structurer(p)
    }

    val structures = new ArrayBuffer[Structure]

    def structure(structurer: Structurer): Structure = {
      val structureElems = structurer.collect
      val res = Structure("", Nil, structureElems)
      structures += res
      return res
    }
  }
}