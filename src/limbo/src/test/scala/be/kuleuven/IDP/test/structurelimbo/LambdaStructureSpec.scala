package be.kuleuven.IDP.test.structurelimbo

import org.scalatest.FreeSpec
import org.scalatest.Matchers
import be.kuleuven.IDP.IDPContext
import be.kuleuven.IDP.Types._
import be.kuleuven.IDP.representation.AST.Structure
import scala.collection.mutable.ArrayBuffer
import be.kuleuven.IDP.helper.Structurer
import be.kuleuven.IDP.representation.AST.Structure
import be.kuleuven.IDP.representation.AST.Predicate
import be.kuleuven.IDP.representation.AST.StructureStatement
import be.kuleuven.IDP.representation.AST.InterDecl
import be.kuleuven.IDP.representation.AST.ThreeValued
import be.kuleuven.IDP.representation.AST.ElemList
import be.kuleuven.IDP.representation.AST.IntRange

class LambdaStructureSpec extends FreeSpec with Matchers {

  import Helper._

  val context = new IDPContext
  val Pred = context.Pred(int)
  val Pred2 = context.Pred(int)

  "A structure with a parameter list must be able to" - {
    "define a single interpretation" in {
      val struct = structure { implicit a =>
        Pred as (1 to 5)
      }
      struct should equal(
        Structure("", Nil, List(
          InterDecl(
            ThreeValued(Pred, None),
            ElemList(List(
              IntRange(1, 5)
            ))
          ))
        ))
    }

    "define multiple interpretation" in {
      val struct = structure { implicit a =>
        Pred as (1 to 5)
        Pred2 as (2 to 4)
      }

      struct should equal(
        Structure("", Nil, List(
          InterDecl(
            ThreeValued(Pred, None),
            ElemList(List(
              IntRange(1, 5)
            ))
          ),
          InterDecl(
            ThreeValued(Pred2, None),
            ElemList(List(
              IntRange(2, 4)
            ))
          )
        )
        ))
    }
  }

  object Helper {

    class StructureContext {
      val structurers = new ArrayBuffer[Structurer]
    }

    class Structurer(p: Predicate) {
      var range: Range = null

      def as(r: Range) {
        range = r
      }

      def collect: StructureStatement = {
        InterDecl(
          ThreeValued(p, None),
          ElemList(
            List(
              IntRange(range.start, range.end)
            )
          )
        )
      }
    }

    implicit def interpret(p: Predicate)(implicit a: StructureContext): Structurer = {
      val s = new Structurer(p)
      a.structurers += s
      s
    }

    def structure(b: StructureContext => Any): Structure = {
      val a = new StructureContext
      b(a)
      return Structure("", Nil, a.structurers.map(x => x.collect).toList)
    }
  }
}