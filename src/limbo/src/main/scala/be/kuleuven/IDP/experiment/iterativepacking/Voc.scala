package be.kuleuven.IDP.experiment.iterativepacking

import be.kuleuven.IDP.IDPContext
import be.kuleuven.IDP.Types._

object Voc {
  val c = new IDPContext
  import c._

  object Draw {
    val ColorValue = Type(nat)
    val area = Pred(nat, nat)
    val square = Pred(string, nat)
    val xpos = Pred(string, nat)
    val ypos = Pred(string, nat)
    val color = Pred(string, ColorValue, ColorValue, ColorValue)
    val text = Pred(string, string)
  }

  object Packing {
    val Square = Type(nat)
    val SizeT = Type(nat)
    val X = Type(int)
    val Y = Type(int)

    /* Properties of the area */
    val Width = Func()(X)
    val Height = Func()(Y)

    /* Properties of squares */
    val Size = Func(Square)(SizeT)
    val XPos = Func(Square)(X)
    val YPos = Func(Square)(Y)
    val ValidPos = Pred(Square)
    val LeftOf = Pred(Square, Square)
    val Above = Pred(Square, Square)
    val NoOverlap = Pred(Square, Square)
    val Largest = Func()(Square)
  }

}