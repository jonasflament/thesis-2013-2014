//package be.kuleuven.IDP.experiment.iterativepacking
//
//import be.kuleuven.IDP.IDPContext
//
//class Improve{
//  val c = new IDPContext
//  import c._
//  
//  import Voc.Packing._
//  
//    val VerticalOverlap = Pred(Square,Square)
//    val LeftAndVOverlap = Pred(Square,Square)
//    val NewXPos = Func(Square)(X)
//    
//  val movingLeftTheory = theory(
//      definition(
//          LeftOf('id1,'id2) <-- (XPos('id1)+Size('id1) =< XPos('id2)),
//		  Above('id1,'id2)  <-- (YPos('id1)+Size('id1) =< YPos('id2)),
//      VerticalOverlap('s1,'s2) <-- (~Above('s1,'s2) & ~Above('s2,'s1)),
//      LeftAndVOverlap('s1,'s2) <-- (LeftOf('s1,'s2) & VerticalOverlap('s1,'s2))
//      ),
//    definition(
//        (NewXPos('s2) === 'x) <-- (?('s1 -> null)(LeftAndVOverlap('s1,'s2)
//         & 'x === max('s3 -> null)(LeftAndVOverlap('s3,'s2) , XPos('s3) + Size('s3)))),
//      (NewXPos('s2) === 0) <-- ~(?('s1 -> null)(LeftAndVOverlap('s1,'s2))),	
//      // TODO: add neq operator
//      ?('s -> null)(NewXPos('s) ~= XPos('s)) 
//	)
//	
//		val HorizontalOverlap = Pred(Square,Square)
//		val AboveAndHOverlap = Pred(Square,Square)
//		val NewYPos = Func(Square)(Y)
//		
//	val movingUpTheory = theory(
//    { LeftOf(id1,id2) <- XPos(id1)+Size(id1) =< XPos(id2).
//      Above(id1,id2)  <- YPos(id1)+Size(id1) =< YPos(id2).
//      HorizontalOverlap(s1,s2) <- ~LeftOf(s1,s2) & ~LeftOf(s2,s1).
//      AboveAndHOverlap(s1,s2) <- Above(s1,s2) & HorizontalOverlap(s1,s2). }
//    { NewYPos(s2) = y <- ? s1 : AboveAndHOverlap(s1,s2) 
//         & y = max{s3 : AboveAndHOverlap(s3,s2) : YPos(s3) + Size(s3)}.
//       //& y = max{s1 : AboveAndHOverlap(s1,s2) : NewYPos(s1) + Size(s1)}.
//      NewYPos(s2) = 0 <- ~ ? s1: AboveAndHOverlap(s1,s2). }	
//
//    ? s : NewYPos(s) ~= YPos(s). 
//	)
//
//    val NewXPos = Func(Square)(X)
//	val	NewYPos = Func(Square)(Y)
//  }
//
//  val moveOne = theory(
//    /* All squares should have a valid position */
//  	! sq : ValidPos(sq).
//    { ValidPos(sq) <- (NewXPos(sq) + Size(sq) =< Width) 
//                    & (NewYPos(sq) + Size(sq) =< Height). }
//    /* Two squares should not overlap */
//  	! sq1 sq2 : NoOverlap(sq1,sq2).
//	  { LeftOf(sq1,sq2) <- NewXPos(sq1)+Size(sq1) =< NewXPos(sq2).
//		  Above(sq1,sq2)  <- NewYPos(sq1)+Size(sq1) =< NewYPos(sq2).
//		  NoOverlap(sq, sq).
//		  NoOverlap(sq1,sq2) <- LeftOf(sq1,sq2) | LeftOf(sq2,sq1) 
//                          | Above(sq1,sq2) | Above(sq2,sq1). }
//    /* Move one block closer to top-left corner, leave others */
//    ? sq1 : NewXPos(sq1) + NewYPos(sq1) < XPos(sq1) + YPos(sq1)
//        & ! sq2 : sq2 ~= sq1 => NewXPos(sq2) = XPos(sq2) 
//                             & NewYPos(sq2) = YPos(sq2). 
//  )
//
//  val resize = theory (
//    Width = max{ s x[X] : Square(s) & x = XPos(s) + Size(s) : x }.
//    Height = max{ s y[Y] : Square(s) & y = YPos(s) + Size(s) : y }.
//    (? x[X] : x > Width) | (? y[Y] : y > Height).
//  )
//
//	/**
//	 * Transforms a structure over the movingLeftVoc to a structure over 
//   * voc: sends NewXPos to XPos and leaves the rest alone.
//	 */
//	def projectLeft(solution) {
//    solution[voc::XPos] = solution[movingLeftVoc::NewXPos]
//    setvocabulary(solution,voc)
//		return solution
//	}
//	
//	/**
//	 * Transforms a structure over the movingUpVoc to a structure over voc:
//	 * sends NewYPos to YPos and leaves the rest alone.
//	 */
//	def projectUp(solution) {
//    solution[voc::YPos] = solution[movingUpVoc::NewYPos]
//    setvocabulary(solution,voc)
//		return solution
//	}
//
//  /** Combination of projectLeft and projectUp */
//	def projectBoth(solution) {
//    solution[voc::XPos] = solution[moveOneVoc::NewXPos]
//    solution[voc::YPos] = solution[moveOneVoc::NewYPos]
//    setvocabulary(solution,voc)
//		return solution
//	}
//
//  def clearLeftAndAbove(struct) {
//    local empty = newstructure(voc,"empty")
//    struct[voc::LeftOf] = empty[voc::LeftOf]
//    struct[voc::Above] = empty[voc::Above]
//  }
//
//  def clearWidthAndHeight(struct) {
//    local empty = newstructure(voc,"empty")
//    struct[voc::Width] = empty[voc::Width]
//    struct[voc::Height] = empty[voc::Height]
//  }
//
//  vocabulary inputvoc {
//    extern type voc::Square
//    extern type voc::SizeT
//    extern type voc::X
//    extern type voc::Y
//    extern voc::Width/0:1
//    extern voc::Height/0:1
//    extern voc::Size/1:1
//  }
//
//  def reset(struct) {
//    setvocabulary(struct,inputvoc)
//    setvocabulary(struct,voc)
//    struct[voc::X.type] = range(0,struct[voc::Width]())
//    struct[voc::Y.type] = range(0,struct[voc::Height]())
//  }
//
//  def showResult(solution,visfile,timepoint) {
//    printtuples("XPos",solution[voc::XPos].graph.ct)
//    printtuples("YPos",solution[voc::YPos].graph.ct)
//    addVisualization(visfile,timepoint,solution)
//  }
//
//	/**
//	 * Improves a solution to the packing problem by recursively moving 
//   * all blocks as much as possible to the left, and then moving them 
//   * up as much as possible.
//	 */
//	def improve(solution) {
//    stdoptions.cpsupport = true
//    local visfile = draw::initVisualization()
//    local timepoint = 0
//    print("First solution")
//    showResult(solution,visfile,timepoint)
//    timepoint = timepoint + 1
//
//    while true do
//      while true do
//
//        local changed = true
//	  	  while changed do
//          changed = false
//
//          //-- Move squares left
//          print("Moving left")
//          setvocabulary(solution,movingLeftVoc)
//          clearLeftAndAbove(solution)
//	  	  	local solution2 = onemodel(movingLeftTheory,solution)
//          if solution2 ~= nil then
//            changed = true
//            solution = projectLeft(solution2)
//            showResult(solution,visfile,timepoint)
//            timepoint = timepoint + 1
//          end
//
//          //-- Move squares up
//          print("Moving up")
//          setvocabulary(solution,movingUpVoc)
//          clearLeftAndAbove(solution)
//	  	  	local solution2 = onemodel(movingUpTheory,solution)
//          if solution2 ~= nil then
//            changed = true
//            solution = projectUp(solution2)
//            showResult(solution,visfile,timepoint)
//            timepoint = timepoint + 1
//          end
//	  	  end
//
//        //-- Move one square
//        print("Moving one")
//        setvocabulary(solution,moveOneVoc)
//        clearLeftAndAbove(solution)
//        local solution2 = onemodel(moveOne,solution)
//        if solution2 == nil then break 
//        else 
//          solution = projectBoth(solution2)
//          showResult(solution,visfile,timepoint)
//          timepoint = timepoint + 1
//        end
//
//      end
//
//      draw::finishVisualization(visfile)
//
//      //-- Resize
//      print("Resizing")
//      clearWidthAndHeight(solution)
//      setvocabulary(solution,voc)
//      stdoptions.cpsupport = false //-- sometimes slow otherwise?
//      local solution2 = onemodel(resize,solution)
//      stdoptions.cpsupport = true
//      if solution2 == nil then break 
//      else 
//        solution = solution2
//        print("Resetting positions")
//        reset(solution)
//        print("Searching for new positions")
//        solution = solve(solution)
//        showResult(solution,visfile,timepoint)
//        timepoint = timepoint + 1
//      end
//
//    end
//    print("Finished improving.")
//
////--    draw::finishVisualization(visfile)
//	}
//
//}