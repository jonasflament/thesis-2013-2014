package be.kuleuven.IDP.experiment.iterativepacking

import be.kuleuven.IDP.representation.AST.Structure
import be.kuleuven.IDP.IDPContext
import be.kuleuven.IDP.Types._
import be.kuleuven.IDP.mx._
import be.kuleuven.IDP.representation.AST.Structure

object Packing {
  val c = new IDPContext
  import c._

  import Voc.Packing._

  val theo = theory(
    /*** Constraints ***/
    /* All squares should have a valid position */
    !('id -> Square)(ValidPos('id)),

    definition(
      ValidPos('id) <-- ((XPos('id) + Size('id) =< Width()) & (YPos('id) + Size('id) =< Height()))
    ),
    /* Two squares should not overlap */
    !('id1 -> Square, 'id2 -> Square)(NoOverlap('id1, 'id2)),
    definition(
      LeftOf('id1, 'id2) <-- (XPos('id1) + Size('id1) =< XPos('id2)),
      Above('id1, 'id2) <-- (YPos('id1) + Size('id1) =< YPos('id2)),
      NoOverlap('id, 'id),
      NoOverlap('id1, 'id2) <-- (LeftOf('id1, 'id2) | LeftOf('id2, 'id1) | Above('id1, 'id2) | Above('id2, 'id1))
    ),

    /*** Symmetry breaking ***/
    /* Put largest square in a corner */
    !('id -> X)('id === Largest() ==> (XPos('id) === 0) & (YPos('id) === 0)),

    /* Optimization for unsat (no solution at all...) */
    sum('id -> nat)(Square('id), Size('id) * Size('id)) =< (Width() * Height())
  )

  /**
   * Return the first solution for a given dataset.
   */
  def solve(data: Structure) : Structure = {
    stdoptions.cpsupport = true
    stdoptions.groundlazily = true
    stdoptions.groundwithbounds = false
    stdoptions.liftedunitpropagation = false
    return onemodel(theo, data)
  }
}
