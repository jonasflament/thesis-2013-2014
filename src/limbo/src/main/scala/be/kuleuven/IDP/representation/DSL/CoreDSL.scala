package be.kuleuven.IDP.representation.DSL
import be.kuleuven.IDP.representation.AST.TypeDecl
import be.kuleuven.IDP.representation.AST.Predicate
import be.kuleuven.IDP.representation.AST.Function
import be.kuleuven.IDP.representation.AST.ASTTypes.Name
import be.kuleuven.IDP.IDPContext

object CoreDSL extends DSL with CoreDSLTrait

trait CoreDSLTrait extends DSL {

  /**
   * Creates a new type with the given super type, and adds the type to the IDP context
   */
  def newType(superType: TypeDecl)(implicit context :IDPContext): TypeDecl = {
    IDPContext.typeCounter += 1
    val r = TypeDecl(s"${context.UPfix.Type}${IDPContext.typeCounter}", None, Some(superType), None)
    return r
  }

  /**
   * Creates a new predicate which accepts parameters of certain types, and adds it to the predicates in this IDP context
   */
  def newPredicate(types: TypeDecl*)(implicit context :IDPContext): Predicate = {
    IDPContext.predCounter += 1
    val r = Predicate(s"${context.UPfix.Pred}${IDPContext.predCounter}", types.toList)
    return r
  }

  /**
   * Creates a new functions which can be applied to given types and returns something of the given type, and adds it to this IDP context
   */
  def newFunction(argTypes: TypeDecl*)(retType: TypeDecl)(implicit context :IDPContext): Function = {
    IDPContext.funcCounter += 1
    val r = Function(s"${context.UPfix.Func}${IDPContext.funcCounter}", argTypes.toList, retType)
    return r
  }

  /**
   * Implicitly converts a single string to List(String) (i.e. a Name)
   */
  implicit def stringToName(a: String): Name = List(a)
}