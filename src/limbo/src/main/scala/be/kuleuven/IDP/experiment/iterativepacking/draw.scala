package be.kuleuven.IDP.experiment.iterativepacking

import be.kuleuven.IDP.representation.AST.Structure
import be.kuleuven.IDP.IDPContext
import be.kuleuven.IDP.mx._
import be.kuleuven.IDP.representation.AST.Vocabulary
import be.kuleuven.IDP.representation.AST.Structure
import be.kuleuven.IDP.representation.AST.Vocabulary

object Draw {
  val c = new IDPContext
  import c._

  import Voc.Draw._

  val data = structure(
    ColorValue as (0 to 255)
  )

  def visualize(struct: Structure) {
    //    local outname = initVisualization()
    //    local out = io.open(outname,"w")
    //    for t in tuples(struct[vocab::area].ct) do
    //      local w = t[1]
    //      local h = t[2]
    //      out:write("idpd_polygon(4,area,0,0,0,"..h..','..w..','..h..','..w..",0).\n")
    //    end
    //    for t in tuples(struct[vocab::square].ct) do
    //      local t2 = t[2]
    //      out:write("idpd_polygon(4,"..t[1]..",0,0,0,"..t2..','..t2..','..t2..','..t2..",0).\n")
    //    end
    //    for t in tuples(struct[vocab::xpos].ct) do
    //      out:write("idpd_xpos("..t[1]..','..t[2]..").\n")
    //    end
    //    for t in tuples(struct[vocab::ypos].ct) do
    //      out:write("idpd_ypos("..t[1]..','..t[2]..").\n")
    //    end
    //    for t in tuples(struct[vocab::color].ct) do
    //      out:write("idpd_color("..t[1]..','..t[2]..','..t[3]..','..t[4]..").\n")
    //    end
    //    for t in tuples(struct[vocab::text].ct) do
    //      out:write("idpd_text("..t[1]..','..t[2]..").\n")
    //    end
    //    out:close()
    //    finishVisualization(outname)
  }

  def initVisualization(): String = {
    ""
    //    math.randomseed(os.time())
    //    if (not vocab) then vocab = voc end
    //    local outfile = "/tmp/.idpd"..tostring(os.time())..tostring(math.random(1111,9999))
    //    return outfile
  }

  def finishVisualization(config: String) {
    IDPDraw(config)
    //    local idpdraw = "binaries/IDPDraw"
    //    os.execute("cat "..outfile.." | "..idpdraw.." -I &")
  }

  def addTimepoint(outfile: String, timepoint: Int, struct: Structure, vocab: Vocabulary) {
    //    local out = io.open(outfile,"a")
    //    for t in tuples(struct[vocab::area].ct) do
    //      local w = t[1]
    //      local h = t[2]
    //      out:write("idpd_polygon_t("..timepoint..",4,area,0,0,0,"..h..','..w..','..h..','..w..",0).\n")
    //    end
    //    for t in tuples(struct[vocab::square].ct) do
    //      local t2 = t[2]
    //      out:write("idpd_polygon_t("..timepoint..",4,"..t[1]..",0,0,0,"..t2..','..t2..','..t2..','..t2..",0).\n")
    //    end
    //    for t in tuples(struct[vocab::xpos].ct) do
    //      out:write("idpd_xpos_t("..timepoint..","..t[1]..','..t[2]..").\n")
    //    end
    //    for t in tuples(struct[vocab::ypos].ct) do
    //      out:write("idpd_ypos_t("..timepoint..","..t[1]..','..t[2]..").\n")
    //    end
    //    for t in tuples(struct[vocab::color].ct) do
    //      out:write("idpd_color_t("..timepoint..","..t[1]..','..t[2]..','..t[3]..','..t[4]..").\n")
    //    end
    //    for t in tuples(struct[vocab::text].ct) do
    //      out:write("idpd_text_t("..timepoint..","..t[1]..','..t[2]..").\n")
    //    end
    //    out:close()
  }
}
