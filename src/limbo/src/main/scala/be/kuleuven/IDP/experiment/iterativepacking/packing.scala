package be.kuleuven.IDP.experiment.iterativepacking

import be.kuleuven.IDP.mx
import ASPInstances._
import be.kuleuven.IDP.representation.AST.Structure

object PackingExperiment {

  def main() {
    println("Improved packing demo on ASP instance 30...")
    val result = solve(aci30)
  }

  def solve(inst: Structure) : Structure= { ???
    // TODO: change to real solve invocation (probably in improve)
//    val result = solve(inst)
//    println("HHHHHMMMMM")
//    if (result != null) {
//      showSolution(result)
//      improve(result)
//    } else {
//      print("No solution found.")
//    }
  }

  def solveACIs() {
    val acis = List(
      aci1, aci2, aci3, aci4, aci5,
      aci6, aci7, aci8, aci9, aci10,
      aci11, aci12, aci13, aci14, aci15,
      aci16, aci17, aci18, aci19, aci20,
      aci21, aci22, aci23, aci24, aci25,
      aci26, aci27, aci28, aci29, aci30,
      aci31, aci32, aci33, aci34, aci35,
      aci36, aci37, aci38, aci39, aci40,
      aci41, aci42, aci43, aci44, aci45,
      aci46, aci47, aci48, aci49, aci50
    )

    var config = Draw.initVisualization()
    for ((e, i) <- acis.zipWithIndex) {
      val sol = solve(e)
      config = Visualize.addVisualization(config, i, sol)
    }
    Draw.finishVisualization(config)
  }

}