package be.kuleuven.IDP.experiment.iterativepacking

import be.kuleuven.IDP.IDPContext
import be.kuleuven.IDP.representation.AST.Structure
import be.kuleuven.IDP.mx._

object Visualize {

  import Voc.Packing._
  import Voc.Draw._

  val c = new IDPContext
  import c._

  val vistheory = theory(
    definition(
      square('id, 's) <-- ('s === Size('id)),
      xpos('id, 'x) <-- ('x === XPos('id)),
      ypos('id, 'y) <-- ('y === YPos('id)),
      color('id, 154, 205, 50),
      text('id, 'id)
    )
  )

  /**
   * Draw the first solution for a given dataset.
   */
  def showSolution(model: Structure) {
    val str = model copy ()

    if (model != null) {
      maketrue(str, ColorValue as (154, 205, 50))
      
      // TODO: what is done here?
      //      maketrue(str[visvoc::area],{str[voc::Width](),str[voc::Height]()})

      val vis = onemodel(vistheory, str)
      Draw.visualize(vis)
    }
  }

  def addVisualization(config: String, timepoint: Int, model: Structure) : String = {
    ""
    //    local str = clone(model)
    //    if model ~= nil then
    //      setvocabulary(str,visvoc)
    //      str[visvoc::ColorValue.type] = { 154;205;50 }
    //      maketrue(str[visvoc::area],{str[voc::Width](),str[voc::Height]()})
    //      local vis = onemodel(vistheory,str)
    //      draw::addTimepoint(visfile,timepoint,vis,visvoc)
    //    end
  }

}
