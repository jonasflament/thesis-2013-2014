package be.kuleuven.IDP.experiment.iterativepacking

import be.kuleuven.IDP.mx._
import be.kuleuven.IDP.representation.AST.Structure
import be.kuleuven.IDP.representation.AST.Theory
import be.kuleuven.IDP.IDPContext
import be.kuleuven.IDP.Types._

object Data {
  val c = new IDPContext
  import c._

  import Voc.Packing._

  val data9 = structure(
    Square as (1 to 9),
    X as (0 to 13),
    Y as (0 to 7),
    Width as 13,
    Height as 7,
    Size as (1 -> 5, 2 -> 5, 3 -> 2, 4 -> 2, 5 -> 2, 6 -> 2, 7 -> 2, 8 -> 2, 9 -> 2)
  )

  val data3 = structure(
    Square as (1 to 3),
    X as (0 to 6),
    Y as (0 to 4),
    Width as 6,
    Height as 4,
    //SizeT as (0 to 6)
    Size as (1 -> 4, 2 -> 2, 3 -> 2)
  )

  val data4 = structure(
    // UNSAT
    Square as (1 to 4),
    X as (0 to 13),
    Y as (0 to 7),
    Width as 13,
    Height as 7,
    //SizeT as (0 to 5)
    Size as (1 -> 4, 2 -> 4, 3 -> 4, 4 -> 4)
  )

  val data15 = structure(
    Square as (1 to 15),
    X as (0 to 200),
    Y as (0 to 100),
    Width as 200,
    Height as 100,
    //SizeT as (0 to 100)
    Size as (1 -> 43, 2 -> 87, 3 -> 42, 4 -> 58, 5 -> 10, 6 -> 15, 7 -> 17, 8 -> 29, 9 -> 17, 10 -> 12, 11 -> 10, 12 -> 15, 13 -> 4, 14 -> 8, 15 -> 7)
  )

  val data20 = structure(
    Square as (1 to 20),
    X as (0 to 200),
    Y as (0 to 200),
    Width as 200,
    Height as 200,
    //SizeT as (0 to 100)
    Size as (1 -> 43, 2 -> 87, 3 -> 42, 4 -> 58, 5 -> 10, 6 -> 15, 7 -> 17, 8 -> 29, 9 -> 17, 10 -> 12, 11 -> 10, 12 -> 15, 13 -> 4, 14 -> 8, 15 -> 7, 16 -> 40, 17 -> 30, 18 -> 25, 19 -> 20, 20 -> 15)
  )

  val data30 = structure(
    Square as (1 to 30),
    X as (0 to 200),
    Y as (0 to 100),
    Width as 200,
    Height as 100,
    //SizeT as (0 to 100)
    Size as (1 -> 43, 2 -> 45, 3 -> 42, 4 -> 68, 5 -> 10, 6 -> 15, 7 -> 17, 8 -> 29, 9 -> 17, 10 -> 12,
      11 -> 10, 12 -> 15, 13 -> 4, 14 -> 8, 15 -> 7, 16 -> 35, 17 -> 20, 18 -> 20, 19 -> 20, 20 -> 15,
      21 -> 13, 22 -> 18, 23 -> 17, 24 -> 16, 25 -> 23, 26 -> 32, 27 -> 33, 28 -> 4, 29 -> 7, 30 -> 15)
  )

}
