package be.kuleuven.IDP.representation.DSL

import be.kuleuven.IDP.representation.AST._

class NamedLogicalConnectivesDSL {
  implicit def formulaToFWrapperWithLogicalConnective(f: Formula): NamedLogicalConnectives = {
    new FWrapper(f) with NamedLogicalConnectives
  }
}

class FWrapper(_wrappedF: Formula) {
  val wrappedF = _wrappedF
}

trait NamedLogicalConnectives extends FWrapper {
  def and(f: Formula): Formula = {
    return ConnectedF(And, wrappedF, f)
  }

  def or(f: Formula): Formula = {
    return ConnectedF(Or, wrappedF, f)
  }

  def equiv(f: Formula): Formula = {
    return ConnectedF(Equiv, wrappedF, f)
  }

  def implies(f: Formula): Formula = {
    return ConnectedF(Impl, wrappedF, f)
  }
}